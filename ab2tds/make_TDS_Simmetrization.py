
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/



from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


""" The script  make_TDS_Simmetrization reconstruct a whole Brillouin zone
from CASTEP output files using symmetry operations that are automatically deduced from the crystal structure.
The output from CASTEP contains a subset of a Monkhorst-Pack grid  which covers a part of the
Brillouin zone.  Knowing the symmetry operations, the whole Brillouin zone 
can be recovered. This is the task of  make_TDS_Simmetrization.

The usage is ::

  make_TDS_Simmetrization castep_filename input_filename optional_extra_file


* The optional_extra_file is not necessary. If given the reading procedure will
    try to find therein the Polarizability and Born effective charges. These informations
    will then be written to the hdf5 outputfile for further use 
    when calculating nonanalytical corrections. The reading is done by

    .. automodule:: TDS_Reading
    .. autofunction:: ReadCastep

* The input_file must set the variables 
    
    * APPLYTIMEREVERSAL
    * CALCULATECOMPLEMENT
    * energy_scaling
    * CODE   ( optional, defaults to Castep) 

using a python syntax
These variables are documented with docstrings below
  
""" 


from . import startmessage

from . import TDS_Simmetry      
from . import TDS_Reading
from . import TDS_Interpolation

import numpy

import sys
import time
import copy
# import octree




#### INPUT 

APPLYTIMEREVERSAL=1
""" * write APPLYTIMEREVERSAL=1 in input file to duplicate eigenvectors at K to get those at -K by complex conjugation 
    * write APPLYTIMEREVERSAL=0  otherwise """
CALCULATECOMPLEMENT=0
""" docstring CALCULATECOMPLEMENT """

energy_scaling = 1.0
""" docstring  energy_scaling """


CODE = "Castep"
"""  other option is phonopy which interfaces vasp, wien2k, abinit, siesta
"""


####

if(sys.argv[0][-12:]!="sphinx-build"):
  s=open(sys.argv[2], "r").read()
  exec(s)


def main() :
  #: Doc comment  docstring A 
  """ essay """
  
  CALCULATEFOURIER = 0
  """ essay 2 """ 
  Nfour_interp = 0


  if(len(sys.argv)==4):
    file_extra=sys.argv[3]
  else:
    file_extra=None


  print( " file_extra = " , file_extra)

  calculatedDatas = TDS_Reading.CalcDatas.get_CalcDatas_Object_From_File(sys.argv[1] , file_extra,
                                             energy_scaling=energy_scaling)

  """ docstring B """


  md5postfix= calculatedDatas.Get_md5postfix()
  filename = calculatedDatas.Get_filename()


  cella=TDS_Simmetry.OP_cella(cellvectors=calculatedDatas.cellVects ,
                 AtomNames_long=calculatedDatas.atomNames,
                 PositionsList_flat=calculatedDatas.atomAbsolutePositions )

  simmetries_dict = TDS_Simmetry.FindSimmetries(cella,   filename= filename,
                                                md5postfix=md5postfix, overwrite= False,
                                                key = "simmetries_dict" )

  print( """
      *** Now printing all the simmetry operation of the crystal. *** 
  """ )

  TDS_Simmetry.printSimmetries(simmetries_dict["RotList"][:] , simmetries_dict["ShiftList"][:])



  print( """
    *** Now printing all the different rotations that can be applied to Qs vectors. *** 
  """)
  TDS_Simmetry.printRotations(  simmetries_dict["DiffRotList"][:]  )


  print( "\n\n  NOW REPLICATING THE INPUT Q VECTORS ")
  print( "     * first we retranslate them inside Brillouin zone ..." )
  print( "__________________ ")


  Replica = TDS_Simmetry.GetAllReplicated (  calculatedDatas, simmetries_dict,   filename= filename,
                                             md5postfix=md5postfix,
                                             overwrite= False ,
                                             key = "Codes_"+(str(APPLYTIMEREVERSAL)) ,
                                             APPLYTIMEREVERSAL=APPLYTIMEREVERSAL,
                                             CALCULATEFOURIER =CALCULATEFOURIER ,
                                             CALCULATECOMPLEMENT=CALCULATECOMPLEMENT,
                                             Nfour_interp=Nfour_interp,
                                             MAKING=1
                                       )

  totQs, totIrot, totItime, totBVifacts , steps, totDMs = [ Replica[key] for key in ["totQs", "totIrot", "totItime", "totBVifacts", "steps" , "totDMs"] ]
  if CALCULATECOMPLEMENT:
    AtotQs, AtotIrot, AtotItime, AtotBVifacts, AtotDMs = [ Replica["complemento"][key]  for key in ["totQs", "totIrot", "totItime", "totBVifacts", "totDMs"] ]

  else:  
    AtotQs, AtotIrot, AtotItime, AtotBVifacts, AtotDMs = [],[],[],[],[]

  print( "NOW PRINTING ALL REPLICAS FOR q POINTS TO FILE")

  for p_totQs, p_totIrot, p_totItime, p_totBVifacts , nomefile  in (
    (totQs, totIrot, totItime, totBVifacts ,"replicated.dat"),
    (AtotQs, AtotIrot, AtotItime, AtotBVifacts, "complement_replicated.dat")
    ):
    print( " WRITING ", len(p_totQs)," REPLICATED POINTS TO FILE  " , nomefile)
    f=open(nomefile,'w')
    for i in range(len(p_totQs)):
        Qs  = p_totQs[i]
        codesymm =  (p_totIrot[i] + 1) *p_totItime[i]
        a,b,c=Qs 
        # if abs( c-1.453042e-02) <1.0e-5 :
        f.write("%e %e %e %d\n"%(a,b,c,codesymm))
    print( " WRITING   OK " )
  f=None
