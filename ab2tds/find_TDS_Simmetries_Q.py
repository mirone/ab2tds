
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


from . import startmessage

from . import TDS_Simmetry      
from . import TDS_Reading
from . import TDS_Interpolation

import numpy

import sys
import time
import copy


#### INPUT 

APPLYTIMEREVERSAL=1
CALCULATECOMPLEMENT= 1

s=open(sys.argv[2], "r").read()
exec(s)

assert("QQreduced" in dir())

####

if(len(sys.argv)==4):
  file_extra=sys.argv[3]
else:
  file_extra=None
  

calculatedDatas = TDS_Reading.CalcDatas.get_CalcDatas_Object_From_File(sys.argv[1] , file_extra)


md5postfix= calculatedDatas.Get_md5postfix()
filename = calculatedDatas.Get_filename()


cella=TDS_Simmetry.OP_cella(cellvectors=calculatedDatas.cellVects ,
               AtomNames_long=calculatedDatas.atomNames,
               PositionsList_flat=calculatedDatas.atomAbsolutePositions )

simmetries_dict = TDS_Simmetry.FindSimmetries(cella,   filename= filename,
                                              md5postfix=md5postfix, overwrite= False,
                                              key = "simmetries_dict")

print ("""
    *** Now printing all the simmetry operation of the crystal plus QQ *** 
""")
Brills  =  calculatedDatas.GetBrillVects()
Bravais =  calculatedDatas.cellVects
TDS_Simmetry.printSimmetries(simmetries_dict["RotList"][:] ,
                             simmetries_dict["ShiftList"][:], QQreduced=QQreduced, Brills =Brills , Bravais=Bravais)

