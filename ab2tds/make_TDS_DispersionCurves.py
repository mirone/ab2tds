
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


from . import startmessage

from . import TDS_Simmetry      
from . import TDS_Reading
from . import TDS_Interpolation
import math

import numpy

import sys
import time
import copy
# import octree


if(sys.argv[0][-12:]!="sphinx-build"):
  s=open(sys.argv[2], "r").read()
  exec(s)


def main():

  assert("Nfour_interp"  in dir())
  assert("Nqlines"  in dir())
  assert("redEnds"  in dir())
  assert("redStarts"  in dir())





  redLine=[]

  for redStart, redEnd, Nqline in zip(     redStarts, redEnds, Nqlines     ):
    if Nqline<2:
      raise Exception( "Nqline<2")
    addLine = numpy.array(redStart)+(((numpy.array(redEnd)-numpy.array(redStart) )))*(( numpy.arange(Nqline)*1.0/(Nqline-1))[:,None])
    redLine.extend( addLine )
  redLine=numpy.array(redLine)


  calculatedDatas = TDS_Reading.CalcDatas.get_CalcDatas_Object_From_File(sys.argv[1])




  md5postfix= calculatedDatas.Get_md5postfix()
  filename = calculatedDatas.Get_filename()

  cella=TDS_Simmetry.OP_cella(cellvectors=calculatedDatas.cellVects ,
                 AtomNames_long=calculatedDatas.atomNames,
                 PositionsList_flat=calculatedDatas.atomAbsolutePositions )

  simmetries_dict = TDS_Simmetry.FindSimmetries(cella,   filename= filename,
                                                md5postfix=md5postfix, overwrite= False,
                                                key = "simmetries_dict" )

  # print( """)
  #     *** Now printing all the simmetry operation of the crystal. *** 
  # """

  # TDS_Simmetry.printSimmetries(simmetries_dict["RotList"][:] , simmetries_dict["ShiftList"][:])

  # print """
  #   *** Now printing all the different rotations that can be applied to Qs vectors. *** 
  # """
  #TDS_Simmetry.printRotations(  simmetries_dict["DiffRotList"][:]  )

  print( "GOING TO RETRIEVE FOURIER TRASFORM  Nfour_interp = ", Nfour_interp)

  Replica = TDS_Simmetry.GetAllReplicated (  calculatedDatas, simmetries_dict,   filename= filename,
                                             md5postfix=md5postfix,
                                             overwrite= False ,
                                             key = "Codes_"+(str(APPLYTIMEREVERSAL)) ,
                                             APPLYTIMEREVERSAL=APPLYTIMEREVERSAL,
                                             CALCULATEFOURIER =1 ,
                                             CALCULATECOMPLEMENT=0,
                                             Nfour_interp=Nfour_interp,
                                             MAKING=0,
                                             MAKINGFOURIER=0
                                       )

  #################################################################################3
  ## 
  ##         A DISPERSION CURVE
  ##

  BV=calculatedDatas.GetBrillVects()
  qsLine =  calculatedDatas.GetClippedQs(QsReduced=redLine)
  qsLine_notShifted =   numpy.dot( redLine ,  BV  )

  file=open("alongthelineF.dat", "w")
  for redQ, q in zip(redLine, qsLine):
    dm = TDS_Simmetry.GetDM_fromF(q,  Replica["Four_Rvects"],   Replica["Four_DMs"] , calculatedDatas,0.0 )
    evals, evects = numpy.linalg.eigh(dm)
    file.write("%e %e %e " % tuple(redQ.tolist())  )
    file.write("%e %e %e " % tuple(q.tolist())  )
    file.write((("%e "*len(evals) )+"\n") % tuple(evals.tolist())  )
    # file.write((("%e "*len(dm) )+"\n") % tuple(dm[0].real.tolist())  )
  file=None


