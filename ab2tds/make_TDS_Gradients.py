
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/
"""The script  make_TDS_Gradients calculates the inverse gradients of the phonon dispersion relation in a specified energy window and allows for the search of critical points. The following steps must have been performed beforehand:

  * Symmetrysation
  * Fourier Interpolation
  * Debye-Waller factors for the temperature of choice

* The usage is ::

        make_TDS_Gradients castep_filename input_filename  

  the file *castep_filename* can be either the name of the original castep output
  our the associated hdf5 file. In any case the associated hdf5 file must exist already.

* The input_file must set the variables:

  * Mandatories

    * subN1
    * subN2
    * subN3
    * N1
    * N2
    * N3
    * Nfour_interp
    * Temperature
    * Lambda

  * Optional

    * branchWeight
    * energyWindow
    * outputname

* The inverse gradients of each phonon branch are written to individual .h5 and .ccp4 files. The auxilary script find_maxima.py can be used to filter the results and search for maximas,
"""


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from . import startmessage

from . import TDS_Simmetry      
from . import TDS_Reading
from . import TDS_Interpolation
import h5py
from . import ccp4_writer


import numpy

import sys
import time
import copy
# import octree
import string
from . import  dabax
import math

if(0):
  APPLYTIMEREVERSAL=1
  subN1=20
  subN2=20
  subN3=20
  N1 = 4
  N2 =  4
  N3 =  4
  Nfour_interp=5
  Temperature=100
  Lambda=0.2
  outputname=""

APPLYTIMEREVERSAL=1
"""
  this is one by default. Must be coherent with previous steps
"""
subN1=20
""" Subdivision, in the elementary brillouin zone, along the Z dimension of the reconstructed volume, 
    which is parallel to the first axis of the brilloin zone.
"""
subN2=20
""" Subdivision, in the elementary brillouin zone, along the Y dimension of the reconstructed volume, 
    which is parallel to the second axis of the brilloin zone.
"""
subN3=20
""" Subdivision, in the elementary brillouin zone, along the X dimension of the reconstructed volume, 
    which is parallel to the third axis of the brilloin zone.
"""
N1 =  4
""" Besides being subdivised, the Brillouin zone, is replicated alonx the first axis  for -N1 up to N1-1 included. In reciprocal cell vector units. N1 can be either an integer or a pair of integer. In the latter case the first element is the starting shift, the second is the ending shift+1.
"""
N2 =  4
"""
  same thing as N1 but along the second reciprocal cell vector
"""
N3 =  4
"""
  same thing as N1 but along the third reciprocal  cell vector
"""

Nfour_interp=4
"""  The number of points in each direction of the 3D reciprocal grid.
     The hdf5 file must contain a previous pretreatement done with the same parameter.
"""
Temperature=100
""" 
The hdf5 file must include DW factors calculated ALSO at this temperature.
 The temperature at which DW factors have been calculated.
   Units are Kelvin
"""
Lambda=0.2
""" For X-ray scattering : the wavelenght in Angstroems.
"""
outputname= None
""" a name to design the output file with data from calculation considering branchWeight or energyWindow
"""
branchWeight=None
""" a list of weights : one per branch.
"""
energyWindow=None
"""  a list of two numbers : minimum and maximum in meV
"""

try:
    from mpi4py import MPI
    myrank = MPI.COMM_WORLD.Get_rank()
    nprocs = MPI.COMM_WORLD.Get_size()
    procnm = MPI.Get_processor_name()
    comm = MPI.COMM_WORLD
    print( "MPI LOADED , nprocs = ", nprocs)
except:
    nprocs=1
    myrank = 0
    print( "MPI NOT LOADED ")


if(sys.argv[0][-12:]!="sphinx-build"):
  s=open(sys.argv[2], "r").read()
  exec(s)

    
def main():
  

  assert("Temperature"  in dir())
  assert("Nfour_interp"  in dir())
  assert("subN1"  in dir())
  assert("subN2"  in dir())
  assert("subN3"  in dir())
  assert("N1"  in dir())
  assert("N2"  in dir())
  assert("N3"  in dir())
  assert("Lambda"  in dir())
  assert("APPLYTIMEREVERSAL"  in dir())

  if "branchWeight" in dir() or "energyWindow" in dir():
    assert("outputname" in dir())
  if "outputname" in dir():
    assert("branchWeight" in dir()  or  "energyWindow" in dir()  )








  ########################################################
  # 
  # Q=2*sin(theta)*2*Pi/lambda
  def Theta(Q, Lambda  ):
    return numpy.arcsin(  numpy.sqrt( numpy.sum(Q*Q, axis=-1)) * Lambda/2.0/math.pi/2.0 )
  #########################################################################

  calculatedDatas = TDS_Reading.CalcDatas.get_CalcDatas_Object_From_File(sys.argv[1])

  tf0 = dabax.Dabax_f0_Table("f0_WaasKirf.dat")

  scatterers = [  tf0.Element(aname  ) for aname in  calculatedDatas.atomNames ] 


  md5postfix= calculatedDatas.Get_md5postfix()
  filename = calculatedDatas.Get_filename()

  cella=TDS_Simmetry.OP_cella(cellvectors=calculatedDatas.cellVects ,
                 AtomNames_long=calculatedDatas.atomNames,
                 PositionsList_flat=calculatedDatas.atomAbsolutePositions )

  simmetries_dict = TDS_Simmetry.FindSimmetries(cella,   filename= filename,
                                                md5postfix=md5postfix, overwrite= False,
                                                key = "simmetries_dict" )


  if myrank==0: print( "GOING TO RETRIEVE FOURIER TRASFORM  Nfour_interp = ", Nfour_interp)

  Replica = TDS_Simmetry.GetAllReplicated (  calculatedDatas, simmetries_dict,   filename= filename,
                                             md5postfix=md5postfix,
                                             overwrite= False ,
                                             key = "Codes_"+(str(APPLYTIMEREVERSAL)) ,
                                             APPLYTIMEREVERSAL=APPLYTIMEREVERSAL,
                                             CALCULATEFOURIER =1 ,
                                             CALCULATECOMPLEMENT=0,
                                             Nfour_interp=Nfour_interp,
                                             MAKING=0,
                                             MAKINGFOURIER=0
                                       )
  #################################################################################3
  ## 
  ##        
  ##

  BV=calculatedDatas.GetBrillVects()


  if myrank==0: print( " GOING TO IFFT out DMs ........ ")

  stime=time.time()
  DMs_nnn =  TDS_Simmetry.GetDM_fromFFT(  Replica["Four_Rvects"],   Replica["Four_DMs"], calculatedDatas, subN1, subN2, subN3  ) 


  Qs_n1 =  numpy.array( numpy.arange(subN1)[:, None]*  BV[0]/subN1, "f")
  Qs_n2 =  numpy.array( numpy.arange(subN2)[:, None]*  BV[1]/subN2, "f")
  Qs_n3 =  numpy.array( numpy.arange(subN3)[:, None]*  BV[2]/subN3, "f")
  Qs_nnn = Qs_n1[:,None,None,: ] + Qs_n2[None,:,None,: ]  + Qs_n3[None,None,:,: ] 



  # print( " SPECIALE " , Qs_nnn[7,3,2])
  # qspeciale =  numpy.array(Qs_nnn[7,3,2])
  # DMspeciale = numpy.array(DMs_nnn[7,3,2])
  Nspeciale = 2+3*subN3 + 7*subN3*subN2


  Nbranches=3*len(calculatedDatas.atomNames)

  # kinematicalFactors      =  numpy.zeros( list(Qs_nnn.shape[:3])+[  Nbranches/3  ] , numpy.complex64)
  # kinematicalFactors.imag = numpy.tensordot( Qs_nnn  ,
  #                                           -calculatedDatas.atomAbsolutePositions  ,  # the minus sign is here
  #                                           [[3], [ 1 ]] ) 
  # kinematicalFactors = numpy.exp( kinematicalFactors )


  # numpy.multiply(  numpy.reshape( DMs_nnn, [subN1, subN2, subN3,  Nbranches , -1, 3 ] )  ,    
  #                  kinematicalFactors.conjugate()[ :,:,:, None, :,None],
  #                  numpy.reshape( DMs_nnn, [ subN1, subN2, subN3 , Nbranches  , -1, 3 ] )
  #                  ) 
  # numpy.multiply(  numpy.reshape( numpy.swapaxes( DMs_nnn , -2,-1 ), [subN1, subN2, subN3,Nbranches   , -1, 3 ] )  ,    
  #                  kinematicalFactors[ :,:,:, None, :,None],
  #                  numpy.reshape( numpy.swapaxes( DMs_nnn , -2,-1 ) , [subN1, subN2, subN3, Nbranches  , -1, 3 ] )
  #                  ) 

  if myrank==0:  print( subN1*subN2*subN3,  " DMs FFT-RETRIEVED in ", time.time()-stime, "  seconds ")

  eigenvalues_nnn  = numpy.zeros(DMs_nnn.shape[:-1] , numpy.float32)



  stime=time.time()
  if myrank==0:  print( " EIGEN-SOLVING ...... ")
  count=0
  for eigenvalues,  dm in zip (
    numpy.reshape (eigenvalues_nnn, [subN1*subN2*subN3,Nbranches ]),
    numpy.reshape (DMs_nnn , [subN1*subN2*subN3,Nbranches,Nbranches ]),
    ):
    count+=1
    if myrank!= count%nprocs:
      continue
    eigenvalues[:] , eigenvectors  = numpy.linalg.eigh(dm)


  if( nprocs>1):

    target = numpy.zeros(  eigenvalues_nnn.shape          , "f")

    comm.Allreduce([numpy.array(eigenvalues_nnn.real), MPI.FLOAT], [ target,MPI.FLOAT]  , MPI.SUM )
    eigenvalues_nnn.real=target
    target=None

  # eigenvalues_speciale , eigenvectors_speciale  = numpy.linalg.eigh(DMspeciale)
  # eigenvalues_speciale =numpy.sqrt(numpy.maximum([eigenvalues_speciale] , numpy.array([1.0e-6], numpy.float32) ))
  # print( " SPECIALE eigvals " , eigenvalues_speciale[-3:])
  # print( " SPECIALE eigvects[-3:,2] " , eigenvectors_speciale[-3:,2])

  if myrank==0:
    print( subN1*subN2*subN3, "   ",  Nbranches , "X",Nbranches , "   DMs eigensolved in    ", time.time()-stime, " seconds ")
  else:
    MPI.Finalize()


  eigenvalues_nnn[:]=numpy.sqrt(numpy.maximum(eigenvalues_nnn , numpy.array([1.0e-10], numpy.float32) ))

  if "branchWeight" in dir():
      print( " CONSIDERING WEIGTHED BRANCHES AS GIVEN BY  branchWeight = ", branchWeight)
      totalstats= numpy.ones([subN1, subN2, subN3 , 1  ] ,  "f" )* numpy.array(branchWeight) 
  else:
      print( "no branchWeight provided, considering all branches \n"*10)

  if "energyWindow" in dir():
      print( " CONSIDERING energyWindow(meV) = ", energyWindow)
      totalstats= numpy.less(eigenvalues_nnn  * 12398.5253/(10.0**8) *1000.0   , energyWindow[1])
      totalstats= totalstats *numpy.less(energyWindow[0]        ,eigenvalues_nnn * 12398.5253/(10.0**8) *1000.0) 
  else:
      print( "no energyWindow provided, considering all remaining branches \n"*10)



  totalw=0

  for ibranch in range(Nbranches):
    # res=1
    res=0
    for asse in [0,1,2]:
      diff = numpy.roll( eigenvalues_nnn[:,:,:,ibranch] , 1, axis=asse) - numpy.roll( eigenvalues_nnn[:,:,:,ibranch], -1, axis=asse)
      res=res+diff*diff
      res=res+(1-totalstats[:,:,:,ibranch])*1.0e30

    res=1.0/numpy.sqrt(res+1.0e-12)

    totalw=totalw+       res*(totalstats.T[ibranch]).T


  ##     diff = eigenvalues_nnn -numpy.roll( eigenvalues_nnn, 1, axis=asse) 
  ##     res=res*numpy.less( diff , 0)
  ##     diff = eigenvalues_nnn -numpy.roll( eigenvalues_nnn, -1, axis=asse) 
  ##     res=res*numpy.less( diff , 0)

    bigresult = numpy.zeros( [2*N1 ,subN1,2*N2 ,subN2,2*N3 ,subN3  ]  , numpy.float32)

    count=0
    for ishift1 in range(-N1,N1):
      for ishift2 in range(-N2,N2):
        for ishift3 in range(-N3,N3):
          print(  ishift1, ishift2, ishift3)
          bigresult[ishift1+N1, :, ishift2+N2, :, ishift3+N3  , :  ] =  res




    h5=h5py.File("InverseGradient_%d_.h5" %(ibranch ,) , "w")
    h5["bigresult"] = numpy.reshape(bigresult, [   2*N1*subN1,2*N2*subN2,2*N3*subN3           ]    ) 
    h5["subN1"    ]=subN1
    h5["subN2"    ]=subN2    
    h5["subN3"    ]=subN3    
    h5["N1"       ]=N1     
    h5["N2"       ]=N2       
    h5["N3"       ]=N3
    h5["BV"]       =BV 
    h5=None
    fname = "InverseGradient_%d_.ccp4" %(ibranch ,)
    print( " NOW WRITING VOLUME ALSO TO A CCP4 FILE ", fname)
    ccp4_writer.write_ccp4_grid_data(numpy.reshape(bigresult,[2*N1*subN1,2*N2*subN2,2*N3*subN3]),fname)




  for ishift1 in range(-N1,N1):
    for ishift2 in range(-N2,N2):
      for ishift3 in range(-N3,N3):
        print(  ishift1, ishift2, ishift3)
        bigresult[ishift1+N1, :, ishift2+N2, :, ishift3+N3  , :  ] =  totalw




  h5=h5py.File("totalw_.h5"  , "w")
  h5["bigresult"] = numpy.reshape(bigresult, [   2*N1*subN1,2*N2*subN2,2*N3*subN3           ]    ) 
  h5["subN1"    ]=subN1
  h5["subN2"    ]=subN2    
  h5["subN3"    ]=subN3    
  h5["N1"       ]=N1     
  h5["N2"       ]=N2       
  h5["N3"       ]=N3
  h5["BV"]       =BV 
  h5=None



  print( " NOW WRITING VOLUME ALSO TO A CCP4 FILE ")
  fname ="totalw_%s_.ccp4" %Temperature 
  ccp4_writer.write_ccp4_grid_data(numpy.reshape(bigresult,[2*N1*subN1,2*N2*subN2,2*N3*subN3]) , fname)
