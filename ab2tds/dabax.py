
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import string
import numpy.fft as FFT

import numpy
import numpy as np

import sys 
import copy
# try:
# from ab2tds.PyMca import specfile
# except:
  
from PyMca5.PyMcaIO import specfile
import math
import os


dirname = os.path.dirname(__file__)
dabaxpath = os.path.join( dirname, "data" )


LAMBDAEV = 12398.425

class Dabax_f0_Table:

  """ Object to retrieve elements f0 tables.
      Creator is Dabax_f0_Table(Name)

      Name can be either  "f0_WaasKirf.dat" ( mind the quotes)
      or "f0_CromerMann.dat" : the two kind of tables implemented


      The function Dabax_f0_Table.Element(self, "name of element") returns
      an object having the methods 
                 f0Lambda(self, Lambda, theta=None)
                 f0Energy(self, Energy, theta=None):              

  """


  def __init__(self, Name=None):
#    self.path="/home/alessandro/data/f0"
    self.path=dabaxpath

    self.dataprocessors   ={   "f0_WaasKirf.dat": self.process_WaasKirf,
                             "f0_CromerMann.dat": self.process_WaasKirf
                           }
    self.elementretrievers={  "f0_WaasKirf.dat":   self.Element_WaasKirf ,
                            "f0_CromerMann.dat":   self.Element_WaasKirf  }
    if(Name!=None):
      self.load(Name)    


  def getDataBaseNames(self):
    return self.elementretrievers.keys()

  def load(self, location):
     self.Location=self.path+"/"+ location
     self.fileType=location
     print( self.Location)
     self.data=specfile.Specfile(self.Location)

     print( "Opened file %s. It is of the  %s type"%(
                          self.Location,self.fileType) )


     if( (self.fileType in self.dataprocessors.keys()) and
         (self.fileType in self.elementretrievers.keys()) ):

        self.Element= self.elementretrievers[self.fileType]
        self.dataprocessors[self.fileType]()

     else:
        print( " I can't process a file of the type %s", self.fileType)
        print( " Possible types are ", self.fileType in dataprocessors.keys)
        exit(1)

  def process_WaasKirf(self):

     self.elementlist=[]
     for scan in self.data:
       self.elementlist.append( scan.command().split() [-1])

  def Element_WaasKirf(self,name):
      index=self.elementlist.index(name)
      if(index==-1):
         print( "%s not found in %s" % ( name, self.Location))
         exit(1)
      lista=self.data[index].dataline(1)
      res= f0_WaasKirf( lista[5],lista[0:5], lista[6:11]   )
      res.element=name
      return res

###############################################
#
#  class to get the f0 from WaasKirf (internal use only)

class f0_WaasKirf:
  def __init__(self, c,a,b):
    self.a_array=numpy.array(a)
    self.b_array=(numpy.array(b))
    self.c=c
    self.classe="class to get the f0 from WaasKirf (internal use only)"

  def __repr__(self):
     for key in dir(self):
       if(key not in ['self','a_array','b_array','c' ]): print( "            ",  key, "=", getattr(self,key))
     return ''

  def f0Lambda(self, Lambda, theta=None):
    

      if( isinstance(Lambda,numpy.ndarray)  ):
        if(theta is None):
           theta=numpy.ones( len(Lambda)     )*0*math.pi/180.0
        k=  numpy.sin(theta) / Lambda
        k=-k*k

        return (self.c+numpy.sum( numpy.swapaxes(self.a_array* numpy.exp(  numpy.reshape(k,[len(k),1])* self.b_array),0,1) , axis=0  ) )
      else:
        print( " THETA SCALARE")
        if(theta is None):
           theta=0*math.pi/180.0
        k=  numpy.sin(theta) / Lambda
        k=-k*k
        # k=-k*k
        return (self.c+ numpy.sum(self.a_array*numpy.exp( self.b_array*k), axis=0 ) )


  def f0Energy(self,  Energy, theta=None):
      return self.f0Lambda(LAMBDAEV /Energy, theta)

#########################################################################################
#
#  f12  tables

class Dabax_f1f2_Table:

  """ Object to retrieve elements f0 tables.
      Creator is Dabax_f1f2_Table(Name)

      Name can be either  "f1f2_Sasaki.dat" ( mind the quotes)
      or "f1f2_Windt.dat" : the two kind of tables implemented


      The function Dabax_f1f2_Table.Element(self, "name of element") returns
      an object having the methods 
                 f1f2Lambda(self, Lambda, theta=None)
                 f1f2Energy(self, Energy, theta=None):              
      
  """

  def __init__(self, Name=None):
    
    self.path=dabaxpath
    
    self.dataprocessors   ={"f1f2_Windt.dat": self.process_Windt,"f1f2_Sasaki.dat":self.process_Sasaki, 
			    "f1f2_Henke.dat": self.process_Windt,"f1f2_asf_Kissel.dat":self.process_Windt}
    
    self.elementretrievers={"f1f2_Windt.dat": self.Element_Windt,"f1f2_Sasaki.dat":self.Element_Windt ,
                            "f1f2_Henke.dat": self.Element_Windt, "f1f2_asf_Kissel.dat": self.Element_Kissel}
    
    if(Name!=None):
      self.load(Name)    

  def getDataBaseNames(self):
    return list(self.elementretrievers.keys())

  def load(self, location):

     self.Location=self.path+"/"+ location
     self.fileType=location
     self.data=specfile.Specfile(self.Location)

     print( "Opened file %s. It is of the  %s type" %(self.Location,self.fileType) )

     if( (self.fileType in list(self.dataprocessors.keys())) and
         (self.fileType in list(self.elementretrievers.keys())) ):

        self.Element= self.elementretrievers[self.fileType]
        self.dataprocessors[self.fileType]()

     else:
        msg="***********************************************************\n"
        msg = msg+ " I can't process a file of the type %s\n" % self.fileType
        msg=msg+" Possible types are %s\n STOP" % self.dataprocessors.keys()
        raise Exception(msg)


  def  process_Windt(self): 
     self.elementlist=[]
     for scan in self.data: 
       self.elementlist.append( scan.command().split() [-1])

  def  process_Sasaki(self): 
     self.elementlist=[]
     for scan in self.data: 
       self.elementlist.append( scan.command().split() [-1])

  def Element_Windt(self,name):
     self.labelE = 'PhotonEnergy[eV]'
     self.factorE = 1.0
     self.labelf1 = 'f1'
     self.labelf2 = 'f2'
     return self.Element_generic(name)

  def Element_Kissel(self,name):
     self.labelE = 'PhotonEnergy[KeV]'
     self.factorE = 1000.0
     self.labelf1 = 'f1'
     self.labelf2 = 'f2'
     return self.Element_generic(name)

  def Element_generic(self,name):
      index=self.elementlist.index(name)
      if(index==-1):
         print ("%s not found in %s" %( name, self.Location) )
         exit(1)

      ene=self.data[index].datacol(self.labelE)*self.factorE
      f1 =self.data[index].datacol(self.labelf1)
      f2 =self.data[index].datacol(self.labelf2)

      hea=self.data[index].header("")
      head=""
      for tok in hea: head=head+tok

      if( head.find( "1ADD 0.0")>0):
         # retrieving Z
         Z = AtomicProperties(name, 'Z')
         f1=f1-Z
#                -----
      res= Ef1f2_Interpolated(ene, f1,  f2  )
      res.element=name
      return res

#
#########################################################################################




###############################################
#
#  class to get f1f2 (internal use only)

class Ef1f2_Interpolated:
  def __init__(self, ene,f1,f2):
    self.ene=ene
    self.f1=f1
    self.f2=f2
    self.classe="Ef1f2_Interpolated class to get f1f2 (internal use only) "

  def __repr__(self):
     for key in dir(self):
       if(key not in ['self','ene','f1','f2' ]): print( "            ",  key, "=", getattr(self,key))
     return ''

  def f1f2Lambda(self, Lambda):

    energy= LAMBDAEV/Lambda
    return self.f1f2Energy(energy)

  def f1f2Energy(self, energy):
    if(isinstance(energy,numpy.ndarray)):
      return numpy.interp(energy,self.ene, self.f1 )+complex(0,1)*numpy.interp(energy  ,self.ene, self.f2 ) 
    else:
      return complex(numpy.interp((energy,)  ,self.ene,  self.f1)[0],numpy.interp((energy,) ,self.ene,   self.f2)[0])




class _AtomicProperties:
  
  databaseA=specfile.Specfile("%s/AtomicConstants.dat" % dabaxpath  )
  databaseB=specfile.Specfile("%s/AtomicDensities.dat" % dabaxpath  )

  elementlistA=[]
  
  for scan in databaseA: 
     elementlistA.append( scan.command().split() [-1])
     
  elementlistB=[]
  for scan in databaseB: 
     elementlistB.append( scan.command().split() [-1])
     
  propA=databaseA[0].header('L')[0].split(  )[1:]
  propB=databaseA[0].header('L')[0].split(  )[1:]
  
    
def AtomicPropertiesList():
	return _AtomicProperties.propA+_AtomicProperties.propB
  
def AtomicProperties(atom, property):
     if(property in _AtomicProperties.propA and atom in _AtomicProperties.elementlistA):
          return _AtomicProperties.databaseA[_AtomicProperties.elementlistA.index(atom)].datacol(property)   [0]
     elif(property in _AtomicProperties.propB and atom in _AtomicProperties.elementlistB):
          return _AtomicProperties.databaseB[_AtomicProperties.elementlistB.index(atom)].datacol(property)   [0]
     elif(property == "Z"):
          return  _AtomicProperties.elementlistA.index(atom)+1
     else:
       print( _AtomicProperties.elementlistA         )
       print( _AtomicProperties.elementlistB         )
       print( " property %s not found " % property   )
       exit(0)
  

#######################################################
# ESEMPI DI UTILIZZAZIONE
#
if (__name__=="__main__"):
  
  Table_f0= Dabax_f0_Table("f0_WaasKirf.dat")

  Table_f1f2=Dabax_f1f2_Table("f1f2_Windt.dat")

  Sif0  = Table_f0.Element("Si")
  Sif1f2= Table_f1f2.Element("Si")

  print( Bef0.f0Energy(1000.0))
  print( Bef1f2.f1f2Energy(1000.0))

  d_Si = 5.431

  E0 = 8000.0

  lamd =  12398.425 / E0


  np = numpy

  Ghkl = np.array([1.0,1.0,1.0])

  G = 1.0/d_Si *np.linalg.norm(Ghkl)

  sint = lamd*G *0.5

  trad = np.arcsin(sint)
  tdeg = np.arcsin(sint)*180/np.pi

  print(" ANGOLO t", tdeg)
  
#
#
#######################################################






