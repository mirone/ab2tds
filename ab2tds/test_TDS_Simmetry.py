
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/


# raise Exception, " this file should not be used in principle"


# from . import startmessage

# from . import TDS_Simmetry      
# from . import TDS_Reading
# from . import TDS_Interpolation

# import numpy
# # from PyMca.Object3D import Object3DQhull

# import sys
# import time
# import copy
# import octree


# APPLYTIMEREVERSAL=1
# CALCULATEFOURIER = 1
# CALCULATECOMPLEMENT= 0
# redStart=[-2.0 , -2.0, 0.  ]
# redEnd  =[2.0 , 2.0 , 0.]
# Nqline=200
# Nfour_interp=5

# s=open(sys.argv[2], "r").read()
# exec(s)

# if Nqline<2:
#   raise Exception, "Nqline<2"
# redLine = numpy.array(redStart)+(((numpy.array(redEnd)-numpy.array(redStart) )))*(( numpy.arange(Nqline)*1.0/(Nqline-1))[:,None])



# calculatedDatas = TDS_Reading.CalcDatas.get_CalcDatas_Object_From_File(sys.argv[1])


# md5postfix= calculatedDatas.Get_md5postfix()
# filename = calculatedDatas.Get_filename()


# cella=TDS_Simmetry.OP_cella(cellvectors=calculatedDatas.cellVects ,
#                AtomNames_long=calculatedDatas.atomNames,
#                PositionsList_flat=calculatedDatas.atomAbsolutePositions )

# simmetries_dict = TDS_Simmetry.FindSimmetries(cella,   filename= filename,
#                                               md5postfix=md5postfix, overwrite= False,
#                                               key = "simmetries_dict" )

# print """
#     *** Now printing all the simmetry operation of the crystal. *** 
# """


# TDS_Simmetry.printSimmetries(simmetries_dict["RotList"][:] , simmetries_dict["ShiftList"][:])



# print """
#   *** Now printing all the different rotations that can be applied to Qs vectors. *** 
# """
# TDS_Simmetry.printRotations(  simmetries_dict["DiffRotList"][:]  )


# print "\n\n  NOW REPLICATING THE INPUT Q VECTORS "
# print "     * first we retranslate them inside Brillouin zone ..." 
# print "__________________ "


# Replica = TDS_Simmetry.GetAllReplicated (  calculatedDatas, simmetries_dict,   filename= filename,
#                                            md5postfix=md5postfix,
#                                            overwrite= False ,
#                                            key = "Codes_"+(str(APPLYTIMEREVERSAL)) ,
#                                            APPLYTIMEREVERSAL=APPLYTIMEREVERSAL,
#                                            CALCULATEFOURIER =CALCULATEFOURIER ,
#                                            CALCULATECOMPLEMENT=CALCULATECOMPLEMENT,
#                                            Nfour_interp=Nfour_interp,
#                                            MAKING=1
#                                      )

# totQs, totIrot, totItime, totBVifacts , steps, totDMs = [ Replica[key] for key in ["totQs", "totIrot", "totItime", "totBVifacts", "steps" , "totDMs"] ]
# if CALCULATECOMPLEMENT:
#   AtotQs, AtotIrot, AtotItime, AtotBVifacts, AtotDMs = [ Replica["complemento"][key]  for key in ["totQs", "totIrot", "totItime", "totBVifacts", "totDMs"] ]
  
# else:  
#   AtotQs, AtotIrot, AtotItime, AtotBVifacts, AtotDMs = [],[],[],[],[]


# for p_totQs, p_totIrot, p_totItime, p_totBVifacts , nomefile  in (
#   (totQs, totIrot, totItime, totBVifacts ,"replicated.dat"),
#   (AtotQs, AtotIrot, AtotItime, AtotBVifacts, "complement_replicated.dat")
#   ):
#   print " WRITING ", len(p_totQs)," REPLICATED POINTS TO FILE  " , nomefile
#   f=open(nomefile,'w')
#   for i in range(len(p_totQs)):
#       Qs  = p_totQs[i]
#       codesymm =  (p_totIrot[i] + 1) *p_totItime[i]
#       a,b,c=Qs 
#       # if abs( c-1.453042e-02) <1.0e-5 :
#       f.write("%e %e %e %d\n"%(a,b,c,codesymm))
#   print " WRITING   OK " 
# f=None

    
# #################################################################################3
# ## 
# ##         A DISPERSION CURVE
# ##

# BV=calculatedDatas.GetBrillVects()
# qsLine =  calculatedDatas.GetClippedQs(QsReduced=redLine)
# qsLine_notShifted =   numpy.dot( redLine ,  BV  )

# if CALCULATEFOURIER:
#   file=open("alongthelineF.dat", "w")
#   for redQ, q in zip(redLine, qsLine):
#     dm = TDS_Simmetry.GetDM_fromF(q,  Replica["Four_Rvects"],   Replica["Four_DMs"]  )
#     evals, evects = numpy.linalg.eigh(dm)
#     file.write("%e %e %e " % tuple(redQ.tolist())  )
#     file.write("%e %e %e " % tuple(q.tolist())  )
#     file.write((("%e "*len(evals) )+"\n") % tuple(evals.tolist())  )
#     # file.write((("%e "*len(dm) )+"\n") % tuple(dm[0].real.tolist())  )
#   file=None

# else:
#   ls=numpy.sqrt(numpy.sum(numpy.sum(BV*BV)))*2


#   print " Concatenating together data from BZ and its neighbour "

#   totQs= numpy.concatenate( (totQs , AtotQs) , axis=0                    )
#   totIrot= numpy.concatenate( (totIrot  , AtotIrot) , axis=0             )
#   totItime= numpy.concatenate( (totItime , AtotItime  ) , axis=0         )
#   totBVifacts= numpy.concatenate( ( totBVifacts ,AtotBVifacts ) , axis=0 )

#   totDMs= numpy.concatenate( ( totDMs ,AtotDMs ) , axis=0 )

#   myTree = octree.OctTree(0.0,0.0,0.0,ls)

#   data2insert= numpy.zeros([  totQs.shape[0],totQs.shape[1]+1 ]   , numpy.float64 ) 
#   data2insert[:, 0:3 ]  = totQs
#   data2insert[:,3 ]  = numpy.arange( totQs.shape[0] ) 

#   myTree.insertNodeS(data2insert  )

#   delta = numpy.max(numpy.abs( numpy.dot( steps, BV ) )) *2.0


#   print "get contact elements "
#   Nline = len (qsLine )

#   # print "la linea est questa ", qsLine
#   # file=open("alongtheline3.dat", "w")
#   # for alpha in numpy.arange(0.0,1.1,0.1):
#   #   dm = totDMs[0]*(1-alpha)+ totDMs[1]*alpha
#   #   evals, evects = numpy.linalg.eigh(dm)
#   #   file.write("%e " % alpha)
#   #   file.write((("%e "*len(evals) )+"\n") % tuple(  (evals.real) .tolist())  )
#   # file=None

#   file=open("alongtheline4.dat", "w")

#   for q, qr in zip(qsLine, redLine):

#     neighbours = myTree.getcontactelements( [q] , delta*1.6)
#     neighboursPos =  totQs[neighbours]
#     Nneig = len(neighboursPos)
#     diff = neighboursPos-q
#     diff = numpy.sum(diff*diff, axis=-1)
#     ipos = diff.tolist().index(  min(diff)  )
#     qnear = neighboursPos[  ipos ]


#     dm = totDMs[neighbours[ipos]]
#     evals, evects = numpy.linalg.eigh(dm)


#     file.write("%e %e %e " % tuple(qr.tolist())  )
#     file.write("%e %e %e " % tuple(qnear.tolist())  )
#     file.write((("%e "*len(evals) )+"\n") % tuple(  (evals.real) .tolist())  )
#   file=None

#   neighbours = myTree.getcontactelements( qsLine , delta*1.6)
#   neighboursPos =  totQs[neighbours]

#   res=Object3DQhull.delaunay( neighboursPos  , "qhull  d Qbb QJ Qc",qsLine  )

#   tetraList = res[1]

#   assert(len(tetraList.shape) ==2 )
#   assert(tetraList.shape[1]   ==4 )

#   determinants  = numpy.zeros( [len( tetraList ), 4, 3])
#   determinants[:,:,:] = neighboursPos[ tetraList  ]

#   volumesV = TDS_Interpolation.multiDets4X3( determinants )
#   interFactor =  numpy.zeros( [len( tetraList ), 4])
#   for itetra in range(0,4):
#     if(itetra):
#       determinants[:, itetra-1 ,:]=tmp
#     tmp = numpy.array(determinants[:, itetra ,:])
#     determinants[:, itetra ,:] =   qsLine
#     interFactor[:,itetra] = TDS_Interpolation.multiDets4X3( determinants )/volumesV
#     lt0 = numpy.sum(  numpy.less( interFactor[:,itetra], -6.0e-2   ) ) 
#     gt1 = numpy.sum(   numpy.less( 1+6.0e-2,interFactor[:,itetra]  ) )

#     try:
#       assert(lt0==0)
#       assert(gt1==0)
#     except:  
#       print " per i fattori del punto " , itetra , " del tetraedro  ci sono ", lt0, " punti piu piccoli di zero "
#       print numpy.arange( len(interFactor) )[ numpy.less( interFactor[:,itetra],  -6.0e-2  )]
#       print interFactor[ :, itetra] [ numpy.less( interFactor[:,itetra], -6.0e-2  )]
#       print " per i fattori del punto " , itetra , " del tetraedro  ci sono ", gt1, " punti piu grandi di uno  " 
#       print numpy.arange( len(interFactor) )[  numpy.less( 1+6.0e-2,interFactor[:,itetra]  ) ]
#       print interFactor[ :, itetra] [ numpy.less( 1+6.0e-2,interFactor[:,itetra]  ) ]
#       raise

#   shifts = qsLine_notShifted - qsLine
#   globalIndexForInterpolation =  neighbours[tetraList ]

#   DMs = numpy.sum( (totDMs[ globalIndexForInterpolation  ])*interFactor[:,:,None,None] , axis= 1 )
#   pp = numpy.sum( (totQs[ globalIndexForInterpolation  ])*interFactor[:,:,None] , axis= 1 )
#   file=open("alongtheline6.dat", "w")
#   for redQ, dm, p in zip(redLine, DMs, pp):
#     evals, evects = numpy.linalg.eigh(dm)
#     file.write("%e %e %e " % tuple(redQ.tolist())  )
#     file.write((("%e "*len(p) )+" ") % tuple(  (p.real) .tolist())  )
#     file.write((("%e "*len(evals) )+"\n") % tuple(evals.tolist())  )
#     # file.write((("%e "*len(dm) )+"\n") % tuple(  (dm[0].real) .tolist())  )
#   file=None


