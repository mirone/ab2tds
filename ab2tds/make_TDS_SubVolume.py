
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/
""" The script make_TDS_SubVolunme cuts a subvolumes out of 3D TDS intensity distribution calculation. It reqires the results of a make_TDS_intensityVolume calculation.
    
    * The usage is  ::

           make_TDS_IntensityVolume intensityVolume_file.h5  

      where the intensityVolume_file.h5 is the result of the make_TDS_intensityVolume calculation in .h5 format

    * The input_file must set the variables :

      * Mandatories

          * redCenter
          * DQ
          * Ngrid1
          * Ngrid2
          * Ngrid3
          * CuttingPlanes
          * outputname

      * Output

        The calculated intensities are written to a .ccp and .h5 file for visualization with chimera and pymca respectively.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from . import startmessage

import h5py
import numpy

import matplotlib
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure, show, axes, sci
from matplotlib import cm, colors
from matplotlib.font_manager import FontProperties
import pylab

import sys
import time
import string
import math
from . import ccp4_writer




redCenter=numpy.array([0.0,0.0,0.0])
""" Center of sub volume, example redCenter=numpy.array([0.0,0.0,0.0])
""" 
DQ=0.08
""" q-resolution in 1/Ang
"""
Ngrid1 = 100
""" number of grid points along one axis
"""
Ngrid2 = 100
""" number of grid points along one axis
"""
Ngrid3 = 100
""" number of grid points along one axis
"""
outputname="subvolume_"
""" prefix of output file, example outputname="subvolume\_"
"""
CuttingPlanes= [
  [ [0,0,1.0 ], [1,0,1.0 ],    [0, 1, 1.0 ] ],
  [ [0,0,-1.0 ], [-1,0,-1.0 ], [0,-1,-1.0 ] ],

  [ [ 1,0,0 ], [ 1,0, 1 ], [ 0, 1,0 ] ],
  [ [-1,0,0 ], [-1,0,-1 ], [ 0,-1,0 ] ],

  [ [ 1,0,0 ], [  1,-1,  1 ], [ 1,-1,0 ] ],
  [ [-1,0,0 ], [ -1,+1, -1 ], [-1,+1,0 ] ],

  [ [0, 1,0 ], [ -1 ,+1 , 1 ], [ -1 , + 1 , 0 ] ],
  [ [0,-1,0 ], [  1 ,-1 , 1 ], [  1 , - 1 , 0 ] ],
]
""" 
Cutting Planes, example Cutting ::

  Planes= [
   [ [0,0,1.0 ], [1,0,1.0 ],    [0, 1, 1.0 ] ],
   [ [0,0,-1.0 ], [-1,0,-1.0 ], [0,-1,-1.0 ] ],

   [ [ 1,0,0 ], [ 1,0, 1 ], [ 0, 1,0 ] ],
   [ [-1,0,0 ], [-1,0,-1 ], [ 0,-1,0 ] ],

   [ [ 1,0,0 ], [  1,-1,  1 ], [ 1,-1,0 ] ],
   [ [-1,0,0 ], [ -1,+1, -1 ], [-1,+1,0 ] ],

   [ [0, 1,0 ], [ -1 ,+1 , 1 ], [ -1 , + 1 , 0 ] ],
   [ [0,-1,0 ], [  1 ,-1 , 1 ], [  1 , - 1 , 0 ] ],
   ]


"""


if(sys.argv[0][-12:]!="sphinx-build"):
  s=open(sys.argv[2], "r").read()
  exec(s)

def main():

  assert("redCenter"  in dir())
  assert("DQ"  in dir())
  assert("Ngrid1"  in dir())
  assert("Ngrid2"  in dir())
  assert("Ngrid3"  in dir())
  assert("CuttingPlanes"  in dir())
  assert("outputname"  in dir())
  print( "READING .....", sys.argv[1])


  h5=h5py.File(sys.argv[1] , "r")
  bigresult = h5["bigresult"][:] 
  subN1     = h5["subN1"    ] .value
  subN2     = h5["subN2"    ] .value
  subN3     = h5["subN3"    ] .value

  try:
    N1start        = h5["N1start"       ] .value
    N2start        = h5["N2start"       ] .value
    N3start        = h5["N3start"       ] .value
    N1end        = h5["N1end"       ] .value
    N2end        = h5["N2end"       ] .value
    N3end        = h5["N3end"       ] .value
  except:
    N1start        = -h5["N1"       ] .value
    N2start        = -h5["N2"       ] .value
    N3start        = -h5["N3"       ] .value
    N1end=-N1start
    N2end=-N2start
    N3end=-N3start

  print( "    " , N1start, N1end)


  BV        = h5["BV"][:]              
  BVinv=numpy.linalg.inv(BV)


  print( "FINISHED READING ", sys.argv[1])
  print( "CREATING POINT  .... " )

  Center =  numpy.dot( redCenter , BV )

  Zpoints = DQ*(  ( numpy.arange(Ngrid1)[:,None] -Ngrid1/2.0 )* numpy.array([1,0,0])  )
  Ypoints = DQ*(  ( numpy.arange(Ngrid2)[:,None] -Ngrid2/2.0 )* numpy.array([0,1,0])  )
  Xpoints = DQ*(  ( numpy.arange(Ngrid3)[:,None] -Ngrid3/2.0 )* numpy.array([0,0,1])  )

  VolumePs = Zpoints[:,None,None,:] + Ypoints[None,:,None,:] + Xpoints[None,None,:,:]+Center

  VolumePs_flat = numpy.reshape( VolumePs,  [ Ngrid1*Ngrid2*Ngrid3, 3 ] )

  determinants  = numpy.zeros( [len( VolumePs_flat ), 4, 3])
  determinants[:, 3, : ] = VolumePs_flat

  det_one = numpy.zeros( [1,  4, 3])
  det_one[0, 3,:] = Center
  print( "FINISHED CREATING POINT  " )

  from . import TDS_Interpolation



  print( "CALCULATING INSIDE/OUSIDE.........")
  inside = numpy.ones( len( VolumePs_flat ) )

  for redPlane in CuttingPlanes:
    Plane = numpy.array([ numpy.dot(p,BV) for p in redPlane] )

    det_one[0, :3,:] = Plane
    centerValue   =  TDS_Interpolation.multiDets4X3( det_one )[0]

    determinants[:, :3, : ] =Plane

    Values = TDS_Interpolation.multiDets4X3( determinants  )

    if( centerValue > 0) :
      Values = numpy.less(0, Values)
    else:
      Values = numpy.less( Values, 0)

  #   print( " printing in or out for ", len(Values), "  points ")
  #   print( " FOR PLANE ", Plane)
  #   print( " CENTER ", Center)
  #   for i in range(len(Values)):
  #     print( "--------------------------------------------")
  #     print( " point " ,  VolumePs_flat[i])
  #     print( " 1=in 0=out ",  Values[i])


    inside = inside*Values
    print( " restent ", inside.sum(), " points " )

  print( "FINISHED CALCULATING INSIDE/OUTSIDE.........")

  intensity = numpy.zeros([ Ngrid1, Ngrid2, Ngrid3 ] , "f"   )
  intensity_line    =  numpy.reshape( intensity,  [ Ngrid1*Ngrid2*Ngrid3  ])
  redQs = numpy.tensordot(  VolumePs_flat  , BVinv, [[ 1  ],[0  ]] )
  float_pos = numpy.array([-N1start*subN1,-N2start*subN2,-N3start*subN3]) + numpy.array([subN1,subN2,subN3])*redQs

  flor_pos = numpy.floor(float_pos  )
  print( flor_pos)

  if(0):
    poss = numpy.minimum(numpy.maximum( numpy.floor(float_pos + numpy.array([0,0,0])), numpy.array([0,0,0])),
                         numpy.array([(N1end-N1start)*subN1-1,
                                      (N2end-N2start)*subN2-1,
                                      (N3end-N3start)*subN3-1])  ).astype(numpy.int32)

    intensity_line[:] = bigresult[  poss[:,0], poss[:,1], poss[:, 2] ]
    print( intensity_line)
    print( intensity_line.sum())
    # raise

  else:
    print( " INTERPOLATING ..... ")
    res=0
    for i in [0,1]:
      for j in [0,1]:
        for k in [0,1]:
          poss = numpy.minimum(numpy.maximum( numpy.floor(float_pos + numpy.array([i,j,k])), numpy.array([0,0,0])),
                               numpy.array([(N1end-N1start)*subN1-1,
                                            (N2end-N2start)*subN2-1,
                                            (N3end-N3start)*subN3-1])  ).astype(numpy.int32)

          add = bigresult[  poss[:,0], poss[:,1], poss[:, 2] ]
          for ipos, ival in zip( (0,1,2), (i,j,k) ) :
            if(ival==1):
              add =add * (float_pos[:,ipos]-flor_pos[:,ipos])
            else:
              add =add * (flor_pos[:,ipos]+1 -float_pos[:,ipos])
          intensity_line[:]+=add

  intensity_line[:]= intensity_line*inside

  print( "FINISHED  INTERPOLATING ")

  print( "WRITING TO .... ",  "%s_.h5" %( outputname))
  h5=h5py.File("%s_.h5" %( outputname) , "w")
  h5["image/data"] = intensity
  h5=None
  print( "FINISHED WRITING TO .... ",  "%s_.h5" %( outputname))


  print( " NOW WRITING VOLUME ALSO TO A CCP4 FILE ")
  fname =outputname+".ccp4" 
  ccp4_writer.write_ccp4_grid_data(intensity , fname)
