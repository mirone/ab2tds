
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/
""" The script  make_TDS_DispersionIntensityCurvesIntegrated calculates IXS and INS scattering intensities integrated over a given axis or sphere. For a given Q, it considers a whole set of points obtained from this q by rotation operations.
    It requires that you have previously done the following operations :

       * symmetrisation
       * Fourier transform
       * Debye waller calculation for the temperature of choice
    
    * The usage is  ::

        make_TDS_DispersionIntensityCurvesIntegrated castep_filename input_filename  

      the file *castep_filename*  can be either the name of the original castep output
      our the associated hdf5 file.  In any case the associated hdf5 file must exist already ( make_TDS_Simmetrization
      must be runned beforehand )

    * The input_file must set the variables :

      * Mandatories

        *  APPLYTIMEREVERSAL
        *  redStarts
        *  redEnds
        *  Nqlines
        *  Nfour_interp
        *  Temperature
        *  resolutionfile
        *  Saturation
        *  lowerLimit
        *  bottom_meV
        *  axis   

             this is the axis around which rotations are done when sphereintegral==0

        *  Nangular

             this is the number of azimuthal steps 

        *  sphereintegral
        
      * if sphereintegral==1

        * Nangular2

            this is the number of polar steps

      * If NEUTRONCALC==0 ( default )

        * Lambda

      * If NEUTRONCALC==1

        * CohB
        * NeutronE

      * Optional

        * branchWeight
        * Eigscal
        * UniqueIon

    EXTRA OUTPUT

      * File summedoverQs.dat

        contains two columns : energy , intensity.   Obtained after summing over Qs

      * File  alongtheline_TDS.dat : 
     
        energy integrated intensity (TDS) for each q-value 



The input variables are documented with docstrings below
  
""" 


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from . import startmessage

from . import TDS_Simmetry      
from . import TDS_Reading
from . import TDS_Interpolation
import h5py
import matplotlib.cm as cm


try:
    from mpi4py import MPI
    myrank = MPI.COMM_WORLD.Get_rank()
    nprocs = MPI.COMM_WORLD.Get_size()
    procnm = MPI.Get_processor_name()
    comm = MPI.COMM_WORLD
    print( "MPI LOADED , nprocs = ", nprocs)
except:
    nprocs=1
    myrank = 0
    print( "MPI NOT LOADED ")


import numpy

import sys
import time
import copy
import string
from . import dabax
import math
from . import EdfFile
import os
import tempfile
import subprocess



if(sys.argv[0][-12:]!="sphinx-build"):
  s=open(sys.argv[2], "r").read()
  exec(s)


def check_input():

  
  assert("Temperature"  in dir())
  assert("Nfour_interp"  in dir())
  assert("Nqlines"  in dir())
  assert("redEnds"  in dir())
  assert("redStarts"  in dir())
  assert("resolutionfile"  in dir())
  assert("APPLYTIMEREVERSAL"  in dir())
  assert("Saturation"  in dir())
  assert("lowerLimit"  in dir())
  assert("bottom_meV"  in dir())

  if "NEUTRONCALC" in dir() and NEUTRONCALC:
    assert( "CohB" in dir())
    assert( "NeutronE" in dir())
  else:
    assert("Lambda"  in dir())

  assert("axis" in dir())
  assert("Nangular" in dir())
  assert("sphereintegral" in dir())
  if sphereintegral==1:
      assert("Nangular2" in dir())


APPLYTIMEREVERSAL=1
"""
  this is one by default. Must be coherent with previous steps
"""

redStarts=[[-2.0 , -2.0, 0.], [2.0 , 2.0 ,  0.]]
""" The list of Q points at which calculation is made is done of N segments. The redStarts
variable contains a list of N starting points in  reciprocal space reduced units.
Each segment i starts at  redStarts[i] and ends at redEnds[i].  Within each segment
the spacing between points is the segment lenght divided by Nqlines[i]
"""
redEnds  =[[2.0 , 2.0 , 0.],  [2.0 , 2.0 , 1.0]] 
""" see redStarts
"""
Nqlines=[200, 100]
""" see redStarts
"""

Nfour_interp=5
"""  The number of points in each direction of the 3D reciprocal grid.
     The hdf5 file must contain a previous pretreatement done with the same parameter.
"""
Temperature=100
""" 
The hdf5 file must include DW factors calculated ALSO at this temperature.
 The temperature at which DW factors have been calculated.
   Units are Kelvin
"""
resolutionfile=""
""" a two column file : first the energy in cm-1, second the value of resolution function
"""

Lambda=1.0
""" For X-ray scattering : the wavelenght in Angstroems.
"""

Saturation=0
""" To limit intensity at peaks : intensity is saturated at this value
"""

lowerLimit=0
""" To correct the dynamical range, when displaying, the intensity is clipped to prevent it going below this value
"""

bottom_meV=0
""" Calculated eigenvalues are clipped to this value before use.
"""

NEUTRONCALC=0
""" This activate neutron scattering calculations.
"""
CohB=None
""" When neutroncalc is on, this must be a dictionary : for each atom name the Coherent lenght.
"""
NeutronE=0
""" When neutroncalc is on, the neutron energy in meV
"""
branchWeight=None
""" a list of weights : one per branch.
"""

Eigscal=0
"""
EigScal==1   Intensity are calculated WITH only eigenvector scalar products ; ==2 Intensity are calculated WITH only eigenvector scalar products PLUS Mass factor
"""


axis = [0.0,0.0,1.0]
""" Rotation axis
"""
Nangular = 4
""" No of angular steps over 2pi
"""
sphereintegral = 0
""" if set to 1 : the whole sphere is integrated
"""
Nangular2 = 1
""" if sphereintegral == 1 this variable has to be set :  Nangular for No of azimuth angle steps, Nangular2 for No of polar angle steps
"""

UniqueIon=-1
""" If >=0. Selects one ion. All the other will be silent.
"""

def main():
    check_input()

    if "EigScal" in dir():
        print( "EigScal  option has been set , in input" )
    else:
        EigScal=0

    if EigScal==1:
      print( " Intensity are calculated WITH only eigenvector scalar products ")
    elif   EigScal==2:
        print( " Intensity are calculated WITH only eigenvector scalar products PLUS Mass factor")
    else:
      print( " Intensity are calculated WITH  more than just   eigenvector scalar products")
    if branchWeight is not None:
       print( " CONSIDERING WEIGTHED BRANCHES AS GIVEN BY  branchWeight = ", branchWeight)





    if sphereintegral==1:
        assert("Nangular2" in dir())
    else:
        Nangular2=1
    ## ==================


    CMGRAY=cm.gray
    if "COLOR" in globals():
      if COLOR:
        CMGRAY=None



    redLine=[]
    tickpos=[]
    ticklabel=[]
    directions=[]

    count=0
    for redStart, redEnd, Nqline in zip(     redStarts, redEnds, Nqlines     ):
      count=count+1

      if Nqline<1:
        raise Exception( "Nqline<2")

      tickpos.append(len(redLine))
      ticklabel.append(str(redStart ) )

      if count==len(Nqlines):
          npts=Nqline+1
      else:
          npts =Nqline

      addLine = numpy.array(redStart)+(((numpy.array(redEnd)-numpy.array(redStart) )))*(( numpy.arange(npts)*1.0/(Nqline))[:,None])

      addDirections = [  (numpy.array(redEnd)-numpy.array(redStart))/100.0   ]*len(addLine) 
      redLine.extend( addLine )
      directions.extend(addDirections)

    tickpos.append(len(redLine))
    ticklabel.append(str(redEnd) )
    # tickpos=numpy.array(tickpos, numpy.float32)/(len( redLine)-1)
    tickpos=numpy.array(tickpos, numpy.float32)/(len( redLine))


    redLine=numpy.array(redLine)
    directions=numpy.array(directions)

    resolution=[]
    file=open(resolutionfile, "r")
    for line in file:
      toks = map(string.atof, string.split(line))
      if len(toks)==2:
        resolution.append(toks)

    resolution=numpy.array(resolution)

    ########################################################
    # 
    # Q=2*sin(theta)*2*Pi/lambda
    def Theta(Q, Lambda  ):
      return numpy.arcsin(  numpy.sqrt( numpy.sum(Q*Q, axis=1)) * Lambda/2.0/math.pi/2.0 )
    #########################################################################

    calculatedDatas = TDS_Reading.CalcDatas.get_CalcDatas_Object_From_File(sys.argv[1])

    if "UniqueIon" in dir():
        N_ions=len(calculatedDatas.atomNames)   
        UniqueFilter = numpy.zeros( N_ions  , dtype= numpy.float32 )
        UniqueFilter[UniqueIon]=1.0
    else:
        UniqueIon=-1



    md5postfix= calculatedDatas.Get_md5postfix()
    filename = calculatedDatas.Get_filename()

    cella=TDS_Simmetry.OP_cella(cellvectors=calculatedDatas.cellVects ,
                   AtomNames_long=calculatedDatas.atomNames,
                   PositionsList_flat=calculatedDatas.atomAbsolutePositions )

    simmetries_dict = TDS_Simmetry.FindSimmetries(cella,   filename= filename,
                                                  md5postfix=md5postfix, overwrite= False,
                                                  key = "simmetries_dict" )


    print( "GOING TO RETRIEVE FOURIER TRASFORM  Nfour_interp = ", Nfour_interp)

    Replica = TDS_Simmetry.GetAllReplicated (  calculatedDatas, simmetries_dict,   filename= filename,
                                               md5postfix=md5postfix,
                                               overwrite= False ,
                                               key = "Codes_"+(str(APPLYTIMEREVERSAL)) ,
                                               APPLYTIMEREVERSAL=APPLYTIMEREVERSAL,
                                               CALCULATEFOURIER =1 ,
                                               CALCULATECOMPLEMENT=0,
                                               Nfour_interp=Nfour_interp,
                                               MAKING=0,
                                               MAKINGFOURIER=0
                                         )

    #################################################################################3
    ## 
    ##         A DISPERSION CURVE
    ##

    BV=calculatedDatas.GetBrillVects()
    # qsLine =  calculatedDatas.GetClippedQs(QsReduced=redLine)
    qsLine_notShifted =   numpy.dot( redLine ,  BV  )
    directions_original = calculatedDatas.GetClippedQs(QsReduced=directions)


    Nprefold = len(qsLine_notShifted)


    qsLine_notShifted_original  = qsLine_notShifted
    qsLine_notShifted = numpy.zeros([Nprefold*Nangular*Nangular2 ,  3], "f"   )
    directions  = numpy.zeros([Nprefold*Nangular*Nangular2 ,  3], "f"   )
    axis = numpy.array(axis)
    axis=axis/numpy.sqrt(numpy.sum(axis*axis))
    u,v,w= axis

    dtheta= math.pi*2/Nangular





    if sphereintegral!=1:

        for i in range(Nangular):
          theta=dtheta*i
          cos=math.cos(theta)
          sin=math.sin(theta)
          rotation_matrix = [

            [ u*u +(v*v+w*w)*cos        , u*v*(1-cos) -w *sin   ,        u*w*(1-cos) + v *sin                         ] , 

            [ v*u*(1-cos) + w *sin      ,  v*v +(w*w+u*u)*cos ,     v*w*(1-cos) -u *sin          ] , 

            [ w*u*(1-cos) -v *sin       ,  w*v*(1-cos) + u *sin   ,    w*w +(u*u+v*v)*cos  ,                    ] , 

            ]
          rotation_matrix = numpy.array( rotation_matrix )
          qsLine_notShifted[ i*Nprefold : (i+1)*Nprefold  ] = numpy.tensordot(  qsLine_notShifted_original, rotation_matrix , [[1],[1]]              )  
          directions[ i*Nprefold : (i+1)*Nprefold  ] = numpy.tensordot(  directions_original, rotation_matrix , [[1],[1]]              )  
    else:
        modulus = numpy.sqrt(numpy.sum(  qsLine_notShifted_original*qsLine_notShifted_original  , axis=1 ))
        count=-1
        for i1 in range(Nangular):
            omega = dtheta*i1
            for i2 in range(Nangular2):
                count=count+1
                theta = math.acos( (0.5-(0.5+i2  )/Nangular2)*2)
                qsLine_notShifted[ count*Nprefold : (count+1)*Nprefold , 2 ] = modulus*math.cos(theta)
                qsLine_notShifted[ count*Nprefold : (count+1)*Nprefold , 1 ] = modulus*math.sin(theta)*math.sin(omega)
                qsLine_notShifted[ count*Nprefold : (count+1)*Nprefold , 0 ] = modulus*math.sin(theta)*math.cos(omega)
                directions[ count*Nprefold : (count+1)*Nprefold , 2 ] = math.cos(theta)
                directions[ count*Nprefold : (count+1)*Nprefold , 1 ] = math.sin(theta)*math.sin(omega)
                directions[ count*Nprefold : (count+1)*Nprefold , 0 ] = math.sin(theta)*math.cos(omega)



    if NEUTRONCALC:
        scatterers = numpy.array([  CohB[aname] for aname in  calculatedDatas.atomNames ] , "F" )   
    else:
        tf0 = dabax.Dabax_f0_Table("f0_WaasKirf.dat")
        lambdas= [Lambda]*len(qsLine_notShifted)
        scatterers = [  tf0.Element(aname  ) for aname in  calculatedDatas.atomNames ] 
        scattFactors_site_q =numpy.array( [ scatterer.f0Lambda(numpy.array(lambdas), Theta(qsLine_notShifted, Lambda  )   )
                                        for scatterer in scatterers ]  ).astype(numpy.float32)

        scattFactors_q_site= scattFactors_site_q.T

    lambdas= [Lambda]*len(qsLine_notShifted)


    file=open("alongthelineF.dat", "w")
    Nbranches=3*len(calculatedDatas.atomNames)

    kinematicalFactors      =  numpy.zeros( [  len(qsLine_notShifted),  Nbranches//3  ] , numpy.complex64)
    kinematicalFactors.imag = numpy.tensordot(   qsLine_notShifted,
                                              -calculatedDatas.atomAbsolutePositions  ,  # the minus sign is here
                                              [[1], [ 1 ]] ) 
    kinematicalFactors = numpy.exp( kinematicalFactors )

    massfactors  = numpy.sqrt(1.0/ calculatedDatas.atomMasses )


    h5=h5py.File(filename+"."+md5postfix, "r")
    DWs_3X3 = h5["Codes_"+(str(APPLYTIMEREVERSAL))]["DWs"][str(Temperature)]["DWf_33"][:]


    dwfacts_perq = numpy.tensordot(  qsLine_notShifted,DWs_3X3    ,  axes=[[1], [ 1]])

    dwfacts_perq  = dwfacts_perq  *qsLine_notShifted[:,None,:]
    dwfacts_perq  = numpy.exp(-numpy.sum(dwfacts_perq , axis=2 ))


    if EigScal==2 :
        dwfacts_mass_kin_scatt_perq  = massfactors*kinematicalFactors
    elif EigScal==1:
        dwfacts_mass_kin_scatt_perq  = kinematicalFactors
    else:
        if NEUTRONCALC:
            dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*  scatterers  
        else:
            dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*scattFactors_q_site

    if UniqueIon !=-1:
        dwfacts_mass_kin_scatt_perq  = dwfacts_mass_kin_scatt_perq  *UniqueFilter




    # cambia temperatura in Hartree
    Temperature_Hartree=Temperature /11604.50520/27.211396132

    # amu = 1822.8897312974866 electrons
    # cm-1 ==> Hartree  cm-1 =  4.5563723521675276e-06 Hartree
    # Bohr = 0.529177249 Angstroems
    factor_forcoth = 4.5563723521675276e-06*1.0/(2.0*Temperature_Hartree)

    resevals=[]
    resstokes=[]

    stime=time.time()
    DMs = [   ]

    resevals=numpy.zeros([len(qsLine_notShifted), 2*Nbranches], "f")
    resstokes=numpy.zeros([len(qsLine_notShifted), 2*Nbranches], "f")
    count=-1
    for ( qnotshifted, 
         dwfacts_mass_kin_scatt,
            dire
         ) in zip(
                    qsLine_notShifted,
                    dwfacts_mass_kin_scatt_perq,
                        directions
                    ):

      count+=1
      if( count%nprocs != myrank) : 
          continue
      dm = TDS_Simmetry.GetDM_fromF( qnotshifted,  Replica["Four_Rvects"],   Replica["Four_DMs"]  ,  calculatedDatas , direction=dire ) 
      evals, evects = numpy.linalg.eigh(dm)

      evals=numpy.sqrt(numpy.maximum(evals, numpy.array([1.0e-16], numpy.float32) ))
      evals=(numpy.maximum(evals, numpy.array([ bottom_meV/(0.0001239852 *1000) ], numpy.float32) ))


      qvect_s = numpy.tensordot(  qnotshifted ,      evects.reshape([-1,3, Nbranches]) , [[0], [1]   ]  )

      Fjs = numpy.sum(dwfacts_mass_kin_scatt* (qvect_s.T), axis=-1)  # sommato sugli ioni


      exp_plus =  numpy.exp( factor_forcoth * evals)
      exp_minus = numpy.exp(-factor_forcoth * evals)

      intensity_array = ( Fjs*(Fjs.conjugate())).real

      deno = (exp_plus-exp_minus)*evals


      if NEUTRONCALC:
          kinstokes = numpy.sqrt(numpy.maximum( (NeutronE-evals*(0.0001239852 *1000))/NeutronE, 0.0 ))
          kinantistk= numpy.sqrt((NeutronE+evals*(0.0001239852 *1000))/NeutronE)
      else:
          kinstokes = 1.0
          kinantistk= 1.0


      if EigScal==0:
        intensity_stokes     =       intensity_array*      exp_plus/deno    *kinstokes
        intensity_antistokes =       intensity_array*      exp_minus/deno   *kinantistk
      else:
        intensity_stokes     =       intensity_array   *kinstokes
        intensity_antistokes =       intensity_array   *kinantistk

      if branchWeight is not None:
        intensity_stokes     =   intensity_stokes     *numpy.array(branchWeight) *kinstokes
        intensity_antistokes =    intensity_stokes    *numpy.array(branchWeight) *kinantistk

      freqmev = evals*0.0001239852 *1000
      mask = numpy.less(0.0, freqmev)

      towrite=  numpy.array([freqmev,  intensity_stokes*mask ,  intensity_antistokes*mask  ])


      resevals[count, :]=numpy.concatenate( [-evals*0.0001239852 *1000,  evals*0.0001239852 *1000] ) 
      resstokes[count,:]= numpy.concatenate( [   intensity_antistokes*mask , intensity_stokes*mask  ] )   



    if nprocs>1:
      if myrank==0:
        comm.Reduce( MPI.IN_PLACE   , [ resevals , MPI.FLOAT  ], op=MPI.SUM, root=0)
      else:
        comm.Reduce([ resevals  , MPI.FLOAT  ], None, op=MPI.SUM, root=0)

      if myrank==0:
        comm.Reduce( MPI.IN_PLACE   , [ resstokes , MPI.FLOAT  ], op=MPI.SUM, root=0)
      else:
        comm.Reduce([ resstokes  , MPI.FLOAT  ], None, op=MPI.SUM, root=0)


    if myrank==0 : print( " Calculated intensities  in " , time.time()-stime, " seconds ")


    if myrank==0:

      file=None

      evalsWmin=numpy.amin(resevals)
      evalsWmax=numpy.amax(resevals)
      step = (resolution[-1,0]-resolution[0,0])/len(resolution)
      Wmin = evalsWmin+resolution[0,0]
      Wmax = evalsWmax+resolution[-1,0]
      Ws=numpy.arange(Wmin,Wmax+step*0.9, step )
      Wmax = Ws[-1]

      # res = numpy.zeros( [len(redLine), len(Ws)], numpy.float32  )
      res = numpy.zeros( [Nprefold, len(Ws)], numpy.float32  )
      minres = numpy.min( resolution[:,1] )
      reso=numpy.interp( Ws ,resolution[:,0] ,resolution[:,1] , left=minres, right= minres)

      reso[Ws<resolution[0,0]] = minres*(resolution[0,0]/Ws[ Ws<resolution[0,0]])**2
      reso[Ws> resolution[-1,0]] = minres*(resolution[-1,0]/Ws[ Ws>resolution[-1,0]])**2



      print( "Writing on reso.dat ")
      print( "minres est ", minres)
      file=open("reso.dat", "w")
      for om,da in zip(Ws, reso):
        file.write("%e %e \n"%(om,da ))
      print( "NOW CONVOLUTING ")

      resofft=numpy.fft.fft(reso)
      freqs = numpy.fft.fftfreq (len(Ws),step)*2*math.pi

      for count in range(Nprefold):
        facts=0
        for countA in range(Nangular*Nangular2):
            ws , intens = resevals[ count + countA*Nprefold ], resstokes[  count + countA*Nprefold  ]
            facts =facts+ numpy.sum( numpy.exp ( -1.0j* freqs[:,None]*  ws    ) * intens , axis = 1  )

        res[count,:]+=numpy.fft.ifft(  facts*resofft  ).real


      print( "CONVOLUTION DONE ")
    #   count=-1
    #   for ( ws , intens  ) in zip(resevals, resstokes ):
    #     facts = numpy.sum( numpy.exp ( -1.0j* freqs[:,None]*  ws    ) * intens , axis = 1  )
    #     count+=1
    #     res[count%Nprefold,:]+=numpy.fft.ifft(  facts*resofft  ).real

      #
      res=res.T/(Nangular*Nangular2)

      # la res, plottee ca commence par le haut  
      res=res[::-1]

      import numpy as np
      import matplotlib
      import matplotlib.cm as cm
      import matplotlib.mlab as mlab
      import matplotlib.pyplot as plt
      from matplotlib.pyplot import figure, show, axes, sci
      from matplotlib import cm, colors
      from matplotlib.font_manager import FontProperties


      edfname = "dispimage.edf" 
      if os.path.exists(edfname):
          os.remove(edfname)

      edf= EdfFile.EdfFile(edfname)
      edf.WriteImage({}, res)
      edf=None

      fig=figure()
      axes=fig.add_subplot(111)
      ima=axes.imshow(numpy.log(numpy.maximum(res,1.0e-14)),cmap=CMGRAY)

      n_pix_y, n_pix_x=  res.shape

      step_x = 1.0/n_pix_x
      step_y = (Wmax-Wmin)/(n_pix_y-1)

      ima.set_extent([0-0.5*step_x,1.+0.5*step_x, Wmin -0.5*step_y,  Wmax + 0.5*step_y] )

      fig.colorbar(ima)

      axes.set_aspect('auto')


      axes.xaxis.set_ticks(numpy.array(tickpos) )


      axes.xaxis.set_ticklabels(ticklabel )

      class event_manager:
        def __init__(self, fig, log_lin):
          self.fig=fig
          self.last_line=[None,None, None,None]
          self.log_lin=log_lin

        def onclick(self, event ):
          x0 = event.xdata
          y0 = event.ydata
          x = event.x
          y = event.y

          if event.button in [1,2] and self.last_line[2] is not None:

            data=self.last_line[2]
            if event.button == 1:
              data[:]=data/1.5
            else:
              data[:]=data*1.5

            self.last_line[1].remove( )

            self.last_line[1] = matplotlib.lines.Line2D( data  ,Ws[::-1] , color="r")

            if self.last_line[3] is None :
              self.last_line[3]=ax=self.fig.add_subplot(111)
            else:
              ax = self.last_line[3]

            ax.add_line( self.last_line[1] )
            self.fig.show()

          if event.button == 3: #right click
            x0 = event.xdata
            y0 = event.ydata
            x = event.x
            y = event.y

            pinredLine = int( 0.5  +  (len(redLine)-1)*x0)
            if pinredLine>=0 and pinredLine<len(redLine):

                if self.last_line[0] is not None:
                  self.last_line[0].remove( )
                  self.last_line[1].remove( )

                if self.last_line[3] is None :
                  self.last_line[3]=ax=self.fig.add_subplot(111)
                else:
                  ax = self.last_line[3]

                self.last_line[0] = matplotlib.lines.Line2D([0.0+x0,0.0+x0], [Wmin  ,Wmax   ], color="r")

                if(self.log_lin=="lin"):
                  self.last_line[2] = res[:,pinredLine]/numpy.max(res[:,pinredLine])

                  f=open("click.dat", "w")
                  for x,y in zip(Ws[::-1],res[:,pinredLine] ):
                    f.write("%e %e \n"%(x,y   ))
                  f=None
                else:
                  fmin= numpy.log(numpy.min(res[:,pinredLine]) )
                  fmax= numpy.log(numpy.max(res[:,pinredLine]))

                  self.last_line[2] = (numpy.log( res[:,pinredLine]) - fmin ) /(fmax-fmin)

                self.last_line[1] = matplotlib.lines.Line2D(  self.last_line[2]  ,Ws[::-1] , color="r")
                #self.last_line[1] = matplotlib.lines.Line2D( [1.0,0.0] ,[ 0 ,Wmax   ], color="r")


                ax.add_line( self.last_line[0] )
                ax.add_line( self.last_line[1] )
                self.fig.show()



      dum=event_manager(fig,"log")

      cid = fig.canvas.mpl_connect('button_press_event', dum.onclick)


      fig2=figure()
      axes=fig2.add_subplot(111)

      fig2.colorbar(ima)

 
      cumulated = res.sum(axis=1)/res.shape[1]
      np.savetxt('summedoverQs.dat', np.column_stack([-Ws, cumulated])[::-1,:] , fmt='%20.10e', delimiter=' ')

      ima=axes.imshow(numpy.maximum(numpy.minimum(res,Saturation ),lowerLimit),cmap=CMGRAY)
      

      ima.set_extent([0-0.5*step_x,1.+0.5*step_x, Wmin -0.5*step_y,  Wmax + 0.5*step_y] )
      axes.xaxis.set_ticks(tickpos )
      axes.xaxis.set_ticklabels(ticklabel )

      axes.set_aspect('auto')

      dum=event_manager(fig2,"lin")
      cid2 = fig2.canvas.mpl_connect('button_press_event', dum.onclick)


      # -----------------------------------------------------------------------------------------
      data4plot = numpy.sum(numpy.maximum( numpy.minimum(res,Saturation ), lowerLimit),axis=0)
      fig3=figure()
      axes=fig3.add_subplot(111)

      plt.plot(numpy.arange(len(data4plot)), data4plot )

      axes.xaxis.set_ticks(numpy.array(tickpos)*(len(data4plot)-1 ) )
      axes.xaxis.set_ticklabels(ticklabel )

      show()
      fig.savefig("pippo.png")
      # ------------------------------------------------------------------------------------------



    if( nprocs>1):
      MPI.Finalize()




