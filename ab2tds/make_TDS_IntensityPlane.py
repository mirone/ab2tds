
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


""" The script  make_TDS_intensityPlane calculates x-ray and neutron TDS intensities on a given plane in reciprocal space.
    The following steps must have been performed beforehand:

      * Symmetrysation
      * Fourier Interpolation
      * Debye-Waller factors for the temperature of choice
    
    * The usage is ::

            make_TDS_IntensityPlane castep_filename input_filename  

      the file *castep_filename* can be either the name of the original castep output
      our the associated hdf5 file. In any case the associated hdf5 file must exist already.

    * The input_file must set the variables:

      * Mandatories

        *  APPLYTIMEREVERSAL
        *  Nfour_interp
        *  Temperature
        *  Saturation
        *  lowerLimit
        *  bottom_meV
        *  Nqs
        *  DQ

      * Definition of Plane through 3 points
        * redA, redB, redC
        * redCenter (optional)
        
      * Definition of plane via normal and point
        * redNormal 
        * redX_idea 
        * redCenter 
        
      * If NEUTRONCALC==0 ( default )

        * Lambda

      * If NEUTRONCALC==1

        * CohB
        * NeutronE

      * Optional

        * branchWeight
        * energyWindow 
        * Eigscal
        * UniqueIon
        * LINEAR
        * COLOR 
        * red4castep

    * Interactions

        * right mouse click on graph plots intensity profile and displays position in reciprocal space

The input variables are documented with docstrings below
  
""" 

from . import startmessage

from . import TDS_Simmetry      
from . import TDS_Reading
from . import TDS_Interpolation
import h5py
import os
import matplotlib.cm as cm

import numpy

import sys
import time
import copy
# import octree
import string
from . import  dabax
import math
from . import EdfFile


try:
    from mpi4py import MPI
    myrank = MPI.COMM_WORLD.Get_rank()
    nprocs = MPI.COMM_WORLD.Get_size()
    procnm = MPI.Get_processor_name()
    comm = MPI.COMM_WORLD
    print( "MPI LOADED , nprocs = ", nprocs)
except:
    nprocs=1
    myrank = 0
    print( "MPI NOT LOADED ")



APPLYTIMEREVERSAL=1
"""
  this is one by default. Must be coherent with previous steps
"""
Nfour_interp=4
"""  The number of points in each direction of the 3D reciprocal grid.
     The hdf5 file must contain a previous pretreatement done with the same parameter.
"""
Temperature=100
""" 
The hdf5 file must include DW factors calculated ALSO at this temperature.
 The temperature at which DW factors have been calculated.
   Units are Kelvin
"""
Lambda=0.2
""" For X-ray scattering: the wavelenght in Angstroems.
"""
Nqs=100
""" Number of q-points along one axis
"""
DQ=0.08
""" q-resolution in 1/Ang
"""

if(sys.argv[0][-12:]=="sphinx-build"):

    redA = [ 1.0 , 1.0  ,0.0  ]
    """ 1. q-point defining plane
    """
    redB = [ 0.0  ,0.0  ,0.0  ]
    """ 2. q-point defining plane
    """
    redC = [ 0.0 , 0.0 , 1.0 ] 
    """ 3. q-point defining plane
    """
    
    redCenter = [ 0.0  ,0.0  ,0.0  ]
    """ center of plane
    """
    redNormal = [ 0.0  ,0.0  ,1.0  ]
    """ Normal to plane
    """
    redX_idea = [1.0,-1.0,0.0]
    """ Orientation of plane
    """
    
NEUTRONCALC=0
""" This activate neutron scattering calculations.
"""
CohB=None
""" When neutroncalc is on, this must be a dictionary: for each atom name the Coherent lenght.
"""
NeutronE=0
""" When neutroncalc is on, the neutron energy in meV
"""
branchWeight=None
""" a list of weights : one per branch.
"""
energyWindow=None
"""  a list of two numbers : minimum and maximum in meV
"""
RemoveBose=0
""" RemoveBose=1 desactivates the Bose statistics.
"""
Saturation=10.0
""" To limit intensity at peaks : intensity is saturated at this value 
"""
lowerLimit=0.000001
""" To correct the dynamical range, when displaying, the intensity is clipped to prevent it going below this value
"""
bottom_meV = 0
""" Calculated eigenvalues are clipped to this value before use.
"""
Eigscal=0
"""
EigScal==1   Intensity are calculated WITH only eigenvector scalar products ; ==2 Intensity are calculated WITH only eigenvector scalar products PLUS Mass factor
"""
UniqueIon=-1
""" If >=0. Selects one ion. All the other will be silent.
"""
LINEAR = 1
""" LINEAR = 1 plotting intensity in linear scale, linear = 0 in log scale
""" 
COLOR = 1
""" Intensity in color or grayscale
"""
red4castep = 'q-vals.dat'
""" Write q-values to file
"""


redLine=[]
tickpos=[]
ticklabel=[]

redLine=numpy.array(redLine)

if(sys.argv[0][-12:]!="sphinx-build"):
    s=open(sys.argv[2], "r").read()
    exec(s)


def main():

    INPUT_TIMES=[ os.stat(sys.argv[2]  ).st_mtime]

    try:
      assert("redCenter"  in dir())
      assert("redNormal"  in dir())
      assert("redX_idea"  in dir())
    except:
      assert("redA"  in dir())
      assert("redB"  in dir())
      assert("redC"  in dir())



    assert("Nqs"  in dir())
    assert("DQ"  in dir())

    assert("Temperature"  in dir())
    assert("Nfour_interp"  in dir())
    assert("APPLYTIMEREVERSAL"  in dir())
    assert("Saturation"  in dir())
    assert("lowerLimit"  in dir())

    assert("bottom_meV"  in dir())


    if "NEUTRONCALC" in dir() and NEUTRONCALC:
        print( " Doing Neutron calculation ")
        assert( "CohB" in dir())
        assert( "NeutronE" in dir())
    else:
        NEUTRONCALC=0
        assert("Lambda"  in dir())




    if "RemoveBose" in dir():
        print( "RemoveBose option has been set , in input" )
    else:
        RemoveBose=0

    if RemoveBose==1:
      print( " Intensity are calculated WITHOUT  Bose factor")
    else:
      print( " Intensity are calculated WITH  Bose factor")


    if "EigScal" in dir():
        print( "EigScal  option has been set , in input" )
    else:
        EigScal=0

    if EigScal==1:
      print( " Intensity are calculated WITH only eigenvector scalar products ")
    elif   EigScal==2:
        print( " Intensity are calculated WITH only eigenvector scalar products PLUS Mass factor")
    else:
      print( " Intensity are calculated WITH  more than just   eigenvector scalar products")



    if "WREF" in dir():
        print( " WAVELET REFINEMENT = " , WREF)
    else:
        WREF=0

    CMGRAY=cm.gray
    if "COLOR" in dir():
      if COLOR:
        CMGRAY=None


    def LINEARFUNC(x):
        return x

    def LOG10(x):
        return numpy.log(x)/math.log(10.0)

    # LOGLINFUNC=numpy.log
    LOGLINFUNC=LOG10
    if "LINEAR" in dir():
        if LINEAR:
            LOGLINFUNC=LINEARFUNC


    # if "branchWeight" in dir() or "energyWindow" in dir():
    #   assert("outputname" in dir())
    # if "outputname" in dir():
    #   assert("branchWeight" in dir()  or  "energyWindow" in dir()  )


    ########################################################
    # 
    # Q=2*sin(theta)*2*Pi/lambda
    def Theta(Q, Lambda  ):
      return numpy.arcsin(  numpy.sqrt( numpy.sum(Q*Q, axis=1)) * Lambda/2.0/math.pi/2.0 )
    #########################################################################

    calculatedDatas = TDS_Reading.CalcDatas.get_CalcDatas_Object_From_File(sys.argv[1])




    if "UniqueIon" in dir():
        N_ions=len(calculatedDatas.atomNames)   
        UniqueFilter = numpy.zeros( N_ions  , dtype= numpy.float32 )
        UniqueFilter[UniqueIon]=1.0
    else:
        UniqueIon=-1

    tf0 = dabax.Dabax_f0_Table("f0_WaasKirf.dat")

    md5postfix= calculatedDatas.Get_md5postfix()
    filename = calculatedDatas.Get_filename()

    cella=TDS_Simmetry.OP_cella(cellvectors=calculatedDatas.cellVects ,
                   AtomNames_long=calculatedDatas.atomNames,
                   PositionsList_flat=calculatedDatas.atomAbsolutePositions )

    simmetries_dict = TDS_Simmetry.FindSimmetries(cella,   filename= filename,
                                                  md5postfix=md5postfix, overwrite= False,
                                                  key = "simmetries_dict" )

    if myrank==0 : print( "GOING TO RETRIEVE FOURIER TRASFORM  Nfour_interp = ", Nfour_interp)

    Replica = TDS_Simmetry.GetAllReplicated (  calculatedDatas, simmetries_dict,   filename= filename,
                                               md5postfix=md5postfix,
                                               overwrite= False ,
                                               key = "Codes_"+(str(APPLYTIMEREVERSAL)) ,
                                               APPLYTIMEREVERSAL=APPLYTIMEREVERSAL,
                                               CALCULATEFOURIER =1 ,
                                               CALCULATECOMPLEMENT=0,
                                               Nfour_interp=Nfour_interp,
                                               MAKING=0,
                                               MAKINGFOURIER=0
                                         )

    #################################################################################3
    ## 
    ##         A DISPERSION CURVE
    ##

    BV=calculatedDatas.GetBrillVects()
    BVinv=numpy.linalg.inv(BV)


    if "redA" in dir():
      Center =  numpy.array(redA)+numpy.array(redB)+numpy.array(redC)
      Center =  numpy.dot( Center , BV ) /3.0
      if "redCenter" in dir():
          print( "redCenter GIVEN IN INPUT. NOW TRANSLATING CENTER OF THE PLANE ")
          Center =  numpy.dot( redCenter,    BV)

      Xaxis  =  numpy.dot(  numpy.array(redB)-numpy.array(redA) ,    BV)
      Xaxis  =  numpy.array( Xaxis/math.sqrt(numpy.sum(Xaxis*Xaxis)) , "f")
      Yaxis  =  numpy.dot(  numpy.array(redC)-numpy.array(redA) ,    BV)
      Yaxis  =  Yaxis - Xaxis*(numpy.sum(Yaxis*Xaxis))
      Yaxis  =  numpy.array( Yaxis/math.sqrt(numpy.sum(Yaxis*Yaxis)) , "f")
      Zaxis  =  numpy.zeros(3, "f" )
      for i in range(3):
        Zaxis[i] = Xaxis[ (i+1)%3]* Yaxis[ (i+2)%3]-Yaxis[ (i+1)%3]* Xaxis[ (i+2)%3]



    else:
      Center =  numpy.dot( redCenter,    BV)
      Normal =  numpy.dot( redNormal,    BV)
      Normal =  numpy.array( Normal/math.sqrt(numpy.sum(Normal*Normal)) , "f")
      Xaxis  =  numpy.dot( redX_idea,    BV)
      Xaxis  =  Xaxis - Normal*(numpy.sum(Xaxis*Normal))
      Xaxis  =  numpy.array( Xaxis/math.sqrt(numpy.sum(Xaxis*Xaxis)) , "f")
      Yaxis  =  numpy.zeros(3, "f" )
      Zaxis=Normal
      for i in range(3):
        Yaxis[i] = Zaxis[ (i+1)%3]* Xaxis[ (i+2)%3]-Xaxis[ (i+1)%3]* Zaxis[ (i+2)%3]

    Center = numpy.array(Center,"f")
    Xaxis  = numpy.array(Xaxis,"f")
    Yaxis  = numpy.array(Yaxis,"f")
    Zaxis  = numpy.array(Zaxis,"f")

    Xtickpos = [  0  ]
    Xticklabels = [ str( numpy.dot(Xaxis,BVinv)  )   ]
    Ytickpos = [  0  ]
    Yticklabels = [ str( numpy.dot(Yaxis,BVinv)  )   ]


    qdispls = (numpy.arange(2*Nqs+1)-Nqs)*DQ
    intensity = numpy.zeros([ (2*Nqs+1), (2*Nqs+1)         ] , "f"   )
    Qs      =  Center  + qdispls[None,:, None]* Xaxis +  qdispls[:,None,None]  * Yaxis



    qsLine_notShifted = numpy.reshape(   Qs ,  [ (2*Nqs+1)* (2*Nqs+1), 3 ] )
    intensity_line    =  numpy.reshape( intensity,  [ (2*Nqs+1)* (2*Nqs+1) ])

    redq4Castep =  numpy.tensordot(  qsLine_notShifted  , BVinv, [[ 1  ],[0  ]] )
    qsLine_clipped =  calculatedDatas.GetClippedQs(QsReduced=redq4Castep)


    if "redq4castep" in dir():

        f=open(redq4castep, "w")
        for redq in  redq4Castep:
            f.write(" %e %e %e \n"%(redq[0] ,redq[1] ,redq[2] , ))
        f=None


    ##########################
    # intensity_array   2.27972242852e-06
    # qspeciale_shifted  [-10.0405426   -0.07479885  -1.11597443]

    # qspeciale_shifted =  numpy.array([-10.0405426 ,   -0.07479885,   -1.11597443])
    # qspeciale_shifted= numpy.array( [-4.53443861, -0.1495977  ,-2.23194885])
    # qsLine_notShifted[0]=qspeciale_shifted

    #########################



    if NEUTRONCALC:
        scatterers = numpy.array([  CohB[aname] for aname in  calculatedDatas.atomNames ] , "F" )   
    else:
        lambdas= [Lambda]*len(qsLine_notShifted)
        scatterers = [  tf0.Element(aname  ) for aname in  calculatedDatas.atomNames ] 
        scattFactors_site_q =numpy.array( [ scatterer.f0Lambda(numpy.array(lambdas), Theta(qsLine_notShifted, Lambda  )   )
                                        for scatterer in scatterers ]  ).astype(numpy.float32)

        scattFactors_q_site= scattFactors_site_q.T




    Nbranches=3*len(calculatedDatas.atomNames)

    kinematicalFactors      =  numpy.zeros( [  len(qsLine_notShifted),  Nbranches//3  ] , numpy.complex64)
    kinematicalFactors.imag = numpy.tensordot(   qsLine_notShifted,
                                              -calculatedDatas.atomAbsolutePositions  ,  # the minus sign is here
                                              [[1], [ 1 ]] ) 
    kinematicalFactors = numpy.exp( kinematicalFactors )
    massfactors  = numpy.sqrt(1.0/ calculatedDatas.atomMasses )


    h5=h5py.File(filename+"."+md5postfix, "r")
    DWs_3X3 = h5["Codes_"+(str(APPLYTIMEREVERSAL))]["DWs"][str(Temperature)]["DWf_33"][:]

    dwfacts_perq = numpy.tensordot(  qsLine_notShifted,DWs_3X3    ,  axes=[[1], [ 1]])

    dwfacts_perq  = dwfacts_perq  *qsLine_notShifted[:,None,:]
    dwfacts_perq  = numpy.exp(-numpy.sum(dwfacts_perq , axis=2 ))


    if EigScal==2 :
        dwfacts_mass_kin_scatt_perq  = massfactors*kinematicalFactors
    elif EigScal==1:
        dwfacts_mass_kin_scatt_perq  = kinematicalFactors
    else:
        if NEUTRONCALC:
            dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*  scatterers  
        else:
            dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*scattFactors_q_site


    if UniqueIon !=-1:
        dwfacts_mass_kin_scatt_perq  = dwfacts_mass_kin_scatt_perq  *UniqueFilter


    # dwfacts_mass_kin_scatt_perq  = massfactors*kinematicalFactors*scattFactors_q_site
    # dwfacts_mass_kin_scatt_perq  = kinematicalFactors


    # cambia temperatura in Hartree
    Temperature_Hartree=Temperature /11604.50520/27.211396132

    # amu = 1822.8897312974866 electrons
    # cm-1 ==> Hartree  cm-1 =  4.5563723521675276e-06 Hartree
    # Bohr = 0.529177249 Angstroems
    factor_forcoth = 4.5563723521675276e-06*1.0/(2.0*Temperature_Hartree)

    resevals=[]
    resstokes=[]

    stime=time.time()

    # DMspeciale = TDS_Simmetry.GetDM_fromF(qspeciale,  Replica["Four_Rvects"],   Replica["Four_DMs"]  ) 

    if nprocs==1:
    #  DMs = [  TDS_Simmetry.GetDM_fromF(q,  Replica["Four_Rvects"],   Replica["Four_DMs"]  ,  calculatedDatas ) for q in   qsLine_notShifted  ]
     DMs = [  TDS_Simmetry.GetDM_fromF(q,  Replica["Four_Rvects"],   Replica["Four_DMs"]  ,  calculatedDatas ) for q in   qsLine_clipped  ]
    else:
      DMs = numpy.zeros(  [ len(qsLine_notShifted), Nbranches, Nbranches]          , "F")
      count=-1
      # for q in   qsLine_notShifted:
      for q in   qsLine_clipped:
        count+=1
        if count%nprocs != myrank:
          continue
        DMs[count]= TDS_Simmetry.GetDM_fromF(q,  Replica["Four_Rvects"],   Replica["Four_DMs"]  ,  calculatedDatas )

    #   target = numpy.zeros(  DMs.shape   , "f")
    #   comm.Allreduce([numpy.array(DMs.real), MPI.FLOAT], [ target,MPI.FLOAT]  , MPI.SUM )
    #   DMs.real=target
    #   comm.Allreduce([numpy.array(DMs.imag), MPI.FLOAT], [ target,MPI.FLOAT]  , MPI.SUM )
    #   DMs.imag=target
    #   target=None



    if myrank==0 : print( " CREATED DMS in " , time.time()-stime, " seconds ")
    stime=time.time()



    # evals_speciale , evects_speciale  = numpy.linalg.eigh(DMspeciale)
    # print( " SPECIALE eigvals " , evals_speciale[-3:])
    # print( " SPECIALE eigvects " , evects_speciale[-3:, 2])

    # evals_speciale=numpy.sqrt(numpy.maximum(evals_speciale, numpy.array([1.0e-6], numpy.float32) ))


    count=-1
    for ( qnotshifted, 
         dwfacts_mass_kin_scatt,
         dm) in zip(
                    qsLine_notShifted,
                    dwfacts_mass_kin_scatt_perq,
                    DMs
                    ):
      count+=1
      if count%nprocs!=myrank:
        continue
      # dm = TDS_Simmetry.GetDM_fromF(q,  Replica["Four_Rvects"],   Replica["Four_DMs"]  )
      evals, evects = numpy.linalg.eigh(dm)
      evals=numpy.sqrt(numpy.maximum(evals, numpy.array([bottom_meV/( 0.0001239852 *1000)], numpy.float32) ))

      qvect_s = numpy.tensordot(  qnotshifted ,      evects.reshape([-1,3, Nbranches]) , [[0], [1]   ]  )

      Fjs = numpy.sum(dwfacts_mass_kin_scatt* (qvect_s.T), axis=-1)  # sommato sugli ioni

      exp_plus =  numpy.exp( factor_forcoth * evals)
      exp_minus = numpy.exp(-factor_forcoth * evals)

      intensity_array = ( Fjs*(Fjs.conjugate())).real

      if NEUTRONCALC:
          kinstokes = numpy.sqrt(numpy.maximum( (NeutronE-evals*(0.0001239852 *1000))/NeutronE, 0.0 ))
          kinantistk= numpy.sqrt((NeutronE+evals*(0.0001239852 *1000))/NeutronE)
      else:
          kinstokes = 1.0
          kinantistk= 1.0



      deno = (exp_plus-exp_minus)*evals
      if RemoveBose==0:
          totalstats = ( exp_plus*kinstokes+exp_minus*kinantistk)/deno
      else:
          totalstats = 1.0/evals


      if EigScal:
        totalstats = 1.0


      if "branchWeight" in dir() and branchWeight is not None:
          if count==0 : print( " CONSIDERING WEIGTHED BRANCHES AS GIVEN BY  branchWeight = ", branchWeight)
          totalstats= totalstats *numpy.array(branchWeight) 
      else:
          if count==0 :print( "no branchWeight provided, considering all branches \n"*10)


      if "energyWindow" in dir() and energyWindow is not None:
          if count==0 :print( " CONSIDERING energyWindow(meV) = ", energyWindow)
          totalstats= totalstats *numpy.less(evals  * 12398.5253/(10.0**8) *1000.0   , energyWindow[1])
          totalstats= totalstats *numpy.less(energyWindow[0]        ,evals * 12398.5253/(10.0**8) *1000.0) 
      else:
          if count==0 :print( "no energyWindow provided, considering all remaining branches \n"*10)


      intensity_line[count]=numpy.sum( intensity_array*  totalstats      )


      # print( " intensity_line[count]  " , intensity_line[count])
      # raise " OK " 
      # exp_plus_speciale =  numpy.exp( factor_forcoth *  evals_speciale)
      # exp_minus_speciale = numpy.exp(-factor_forcoth *  evals_speciale)
      # totalstats_speciale = (exp_plus_speciale+exp_minus_speciale)/((exp_plus_speciale-exp_minus_speciale)* evals_speciale )
      # print( " totalstats_speciale[-3:] " , totalstats_speciale[-3:])

      # freqmev = evals*0.0001239852 *1000
      # mask = numpy.less(0.5, freqmev)

    if nprocs>1:
      if myrank==0:
        comm.Reduce( MPI.IN_PLACE   , [ intensity_line  , MPI.FLOAT  ], op=MPI.SUM, root=0)
      else:
        comm.Reduce([  intensity_line , MPI.FLOAT  ], None, op=MPI.SUM, root=0)

    file=None

    if myrank==0 :




        print( " Calculated intensities  in " , time.time()-stime, " seconds ")
        print( "now writing intensities to file intensityplane.txt and image0.edf")

        edfname = ("image%d.edf" % myrank)
        if os.path.exists(edfname):
            os.remove(edfname)

        edf= EdfFile.EdfFile(edfname)
        edf.WriteImage({}, intensity)
        edf=None


        fileout=open("intensityplane.txt", "w")
        fileout.write("%e %e %e\n"%tuple((numpy.dot(Qs[0,0],BVinv)).tolist())    )
        fileout.write("%e %e %e\n"%tuple((numpy.dot(Qs[0,-1]-Qs[0,0],BVinv)).tolist())    )
        fileout.write("%e %e %e\n"%tuple((numpy.dot(Qs[-1,0]-Qs[0,0],BVinv)).tolist())    )
        for iline in range((2*Nqs+1) ):
            fileout.write(("%e "*(2*Nqs+1)+"\n")%tuple(( intensity[iline,:] ).tolist())    )
        fileout=None



    if myrank==1:

      nomefile = "VolumeIntensity_%s_.h5" %Temperature 
      import os.path
      if os.path.isfile(nomefile):
        h5=h5py.File( nomefile, "r")

        bigresult = h5["bigresult"][:] 
        subN1     = h5["subN1"    ] [()]
        subN2     = h5["subN2"    ] [()]
        subN3     = h5["subN3"    ] [()]
        try:
            N1        = (h5["N1end"       ] [()]-h5["N1start"       ] [()])/2
            N2        = (h5["N2end"       ] [()]-h5["N2start"       ] [()])/2
            N3        = (h5["N3end"       ] [()]-h5["N3start"       ] [()])/2
        except:
            N1        = h5["N1"       ] [()]
            N2        = h5["N2"       ] [()]
            N3        = h5["N3"       ] [()]

        BV        = h5["BV"][:]              

        redQs = numpy.tensordot(  qsLine_notShifted  , BVinv, [[ 1  ],[0  ]] )

        float_pos = numpy.array([N1*subN1,N2*subN2,N3*subN3]) + numpy.array([subN1,subN2,subN3])*redQs

        disc_pos = {}
        flor_pos = numpy.floor(float_pos  )

        disc_pos[(0,0,0)] = numpy.maximum( numpy.floor(float_pos  )  , numpy.array([0,0,0])   )

        res=0
        for i in [0,1]:
          for j in [0,1]:
            for k in [0,1]:
              poss = numpy.minimum(numpy.maximum( numpy.floor(float_pos + numpy.array([i,j,k])), numpy.array([0,0,0])),
                                   numpy.array([(2*N1)*subN1-1,
                                                (2*N2)*subN2-1,
                                                (2*N3)*subN3-1])  ).astype(numpy.int32)
              # disc_pos[(i,j,k)] 
              add = bigresult[  poss[:,0], poss[:,1], poss[:, 2] ]
              for ipos, ival in zip( (0,1,2), (i,j,k) ) :
                if(ival==1):
                  add =add * (float_pos[:,ipos]-flor_pos[:,ipos])
                else:
                  add =add * (flor_pos[:,ipos]+1 -float_pos[:,ipos])
              res=res+add

        intensity_line[:] = res
      else:
          message = nomefile + " NOT FOUND " 
          raise Exception( message)




    def double(A):
        A=double1D(A)
        B=double1D(numpy.transpose(A))
        return numpy.transpose(B)


    def double1D(A):
        N,M=A.shape
        B=numpy.zeros([2*N, M],  A.dtype )
        coeff= numpy.array([1.0,  9.0/16.0, 0.0,-1.0/16])

        B[::2    ,: ] = B[::2 ,:]+ coeff[0]* A[ :,:]
        B[1::2   ,: ] = B[1::2 ,:]+ coeff[1]* A[ :,:]
        B[3::2   ,: ] = B[3::2 ,:]+ coeff[3]* A[ :-1,:]
        B[1:-2 :2,: ] = B[1:-2:2,: ]+ coeff[1]* A[1:,: ]
        B[3:-4 :2,: ] = B[3:-4:2,: ]+ coeff[3]* A[3:,:]
        return B



    if myrank in [0,1]:

      # la res, plottee ca commence par le haut
      originalintensity=intensity
      if myrank==0:
        intensity= numpy.minimum(intensity, Saturation)
        intensity[0,0] = Saturation*1.15
      if myrank==1:
        intensity= numpy.minimum(intensity, Saturation)

      if myrank==0: intensity= numpy.maximum(intensity, lowerLimit )
      if myrank==1: intensity= numpy.maximum(intensity, lowerLimit )


      if WREF!=0:
          for i in range(WREF):
              intensity=double(intensity)


      res=intensity[::-1]
      Qs_ordered = Qs[::-1]


      import numpy as np
      import matplotlib
      import matplotlib.cm as cm
      import matplotlib.mlab as mlab
      import matplotlib.pyplot as plt
      from matplotlib.pyplot import figure, show, axes, sci
      from matplotlib import cm, colors
      from matplotlib.font_manager import FontProperties

      fig=figure()
      axes=fig.add_subplot(111)
      if myrank==0:
          axes.set_title("Plane from plane calculation")
      else:
          axes.set_title("Plane from Volume interpolation")

      ima=axes.imshow(LOGLINFUNC(numpy.maximum(res,1.0e-14)),cmap=CMGRAY)
      fig.colorbar(ima)
      axes.set_aspect('auto')

      axes.xaxis.set_ticks(numpy.array(Xtickpos) )
      axes.xaxis.set_ticklabels(Xticklabels )
      axes.yaxis.set_ticks(numpy.array(Ytickpos) )
      axes.yaxis.set_ticklabels(Yticklabels, rotation='vertical'  )

      ima.set_extent([-Nqs-0.5,Nqs+0.5, -Nqs-0.5 ,  Nqs+0.5] )


      last_line=[None,None, None,None, None]

      def checkfilechanged( ):
        global originalintensity, fig, axes
        if  INPUT_TIMES[0]!=os.stat(sys.argv[2]).st_mtime:
            INPUT_TIMES[0]=os.stat(sys.argv[2]).st_mtime
            s=open(sys.argv[2], "r").read()
            exec(s, globals(), globals())
            intensity= numpy.minimum(originalintensity, Saturation)
            intensity= numpy.maximum(intensity, lowerLimit)
            res=intensity[::-1]
            ima=axes.imshow(LOGLINFUNC(numpy.maximum(res,1.0e-14)),cmap=CMGRAY)
            # fig.colorbar(ima)
            fig.show()
            return


      def onclick(event ):
        if event.button in [1,2] and last_line[2] is not None:
          data=last_line[2]
          if event.button == 1:
            data[:]=(data+Nqs)/1.5-Nqs
          else:
            data[:]=(data+Nqs)*1.5-Nqs

          last_line[1].remove( )

          if last_line[4]==1:
            last_line[1] = matplotlib.lines.Line2D( numpy.arange(-Nqs,Nqs+1) ,  data  ,    color="r"   )
          else:
            last_line[1] = matplotlib.lines.Line2D( data  ,  numpy.arange(-Nqs,Nqs+1)[::-1]   ,   color="r"   )

          if last_line[3] is None :
            last_line[3]=ax=fig.add_subplot(111)
          else:
            ax = last_line[3]

          ax.add_line( last_line[1] )
          fig.canvas.draw()
          fig.canvas.flush_events()
          
          fig.show()


        if event.button == 3: #right click

          x0_=event.xdata
          y0_=event.ydata
          x0 = int(event.xdata+100000+0.4999)-100000
          y0 = int(event.ydata+100000+0.4999)-100000
          x = event.x
          y = event.y

          if(x0_>y0_):
            last_line[4]=0
            pinredLine = x0
          else:
            last_line[4]=1
            pinredLine = y0


          if pinredLine>=-Nqs and pinredLine<=Nqs:
            print( "     POINT =  "  ,  Qs[x0+Nqs,y0+Nqs   ])
            print( " RED POINT =  "  ,  numpy.dot(Qs[y0+Nqs, x0+Nqs]  , BVinv))
            print( "x,y", x,y)
            print( "x0,y0", x0,y0)
            print( "VALUE ", res[::-1][Nqs+y0, Nqs+x0])
            print( "VALUE ", intensity[Nqs+y0, Nqs+x0])


            if last_line[0] is not None:
              last_line[0].remove( )
              last_line[1].remove( )

            if last_line[3] is None :
              last_line[3]=ax=fig.add_subplot(111)
            else:
              ax = last_line[3]


            if(x0_>y0_):
              last_line[0] = matplotlib.lines.Line2D([x0, x0], [-Nqs  ,Nqs   ], color="r")
              last_line[2] = -Nqs + (2*Nqs+1)*intensity[:,pinredLine+Nqs ]/numpy.max(intensity[:, pinredLine +Nqs ])
              last_line[1] = matplotlib.lines.Line2D(  last_line[2]  ,numpy.arange(-Nqs, Nqs+1) , color="r")
              print( "NORMALIZATION TO ", numpy.max(res[:,pinredLine+Nqs ]))
            else:
              last_line[0] = matplotlib.lines.Line2D([-Nqs  ,Nqs   ],[y0, y0],  color="r")
              last_line[2] = -Nqs + (2*Nqs+1)*intensity[pinredLine+Nqs , :]/numpy.max(intensity[pinredLine+Nqs , : ])
              last_line[1] = matplotlib.lines.Line2D( numpy.arange(-Nqs, Nqs+1) ,  last_line[2]  ,color="r")
              print( "NORMALIZATION TO ", numpy.max(res[pinredLine+Nqs , :]))

            ax.add_line( last_line[0] )
            ax.add_line( last_line[1] )

            fig.canvas.draw()
            fig.canvas.flush_events()
            
            fig.show()





      cid = fig.canvas.mpl_connect('button_press_event', onclick)

      def on_key(event):
          print( 'you pressed', event.key, event.xdata, event.ydata)
      cidk = fig.canvas.mpl_connect('key_press_event', on_key)


      # Create a new timer object. Set the interval 500 milliseconds (1000 is default)
      # and tell the timer what function should be called.

      fig.canvas.draw()
      timer = fig.canvas.new_timer(interval=100)
      timer.add_callback(checkfilechanged)
      timer.start()


      show()
      fig.savefig( ("image%d.eps"%myrank), dpi=1200)


                  ##  http://matplotlib.sourceforge.net/api/pyplot_api.html#matplotlib.pyplot.savefig


    if( nprocs>1):
        MPI.Finalize()
  
