
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/

r""" The script make_ElasticTensor .....
    
    * The usage is  ::

            make_ElasticTensor castep_filename 

      the file *castep_filename* can be either the name of the original castep output or
      the associated hdf5 file.)

    * No   extra input_file 
 
""" 

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function



from . import startmessage

from . import TDS_Simmetry      
from . import TDS_Reading
import h5py

import numpy


import sys
import time
import copy

import string
import math
import os
import tempfile
import subprocess




def main():
  calculatedDatas = TDS_Reading.CalcDatas.get_CalcDatas_Object_From_File(sys.argv[1])

  md5postfix= calculatedDatas.Get_md5postfix()
  filename = calculatedDatas.Get_filename()

  # cella=TDS_Simmetry.OP_cella(cellvectors=calculatedDatas.cellVects ,
  #                AtomNames_long=calculatedDatas.atomNames,
  #                PositionsList_flat=calculatedDatas.atomAbsolutePositions )

  res = TDS_Simmetry.GetSimplyDMs (  calculatedDatas,   filename,
                               md5postfix,
                                     overwrite= False, acustic = True)

  dms = res["dms"]
  Qs  = res["Qs" ]
  evals = res["evals" ]
  
  massfactors  = numpy.sqrt(calculatedDatas.atomMasses )
  massfactors  = massfactors[:,None]* numpy.ones([3])
  massfactors = massfactors.flatten()

  gammas = []
  el_ene = []

  print( dms.shape)

  mods = numpy.sqrt( (Qs*Qs).sum(axis=-1)  ) 
  md = mods.max()

 # qmins = Qs[0]
 # qmaxs = Qs[0]
 # for q in Qs:
 #   qmins = numpy.minimum(qmins,q)
 #   qmaxs = numpy.maximum(qmaxs,q)   
 # steps = numpy.array( [1.0e30]*3 ) 
 # for q in Qs:
 #   newsteps = numpy.minimum(  abs( q-Qs[0] ) ,   steps) 
 #   goods = numpy.less(  (qmaxs-qmins)/10000,  newsteps ) 
 #   steps[goods] = newsteps[goods]
 # Ns = numpy.round((qmaxs-qmins)/steps).astype("i")
 # dicotrack={}

  count=0
  points = [[0,0,0]]
  values = [10000]
  for q,dm, vals  in zip(Qs, dms, evals):


    
    
    if math.sqrt( (q*q).sum() )>md:
      continue

    #print( q, vals[:3])
    
    values.append( vals[:3].max()  )
    points.append(q)
    
    count+=1
    #print( count, md)
    dm = (massfactors * ( dm*massfactors).T).T

    # dicotrack[  tuple(numpy.round((q-Qs[0])/steps).astype("i") ) ]=dm
    
    # g0=g00   g1=g11  g2=g22  g3= 2g12  g4=2g02  g5=2g01

    #  gij = 1/2(  dui/dxj + duj/dxi  ) 

    #                0         1         2        3     4         5
    #                00        11        22      12     02        01
    polX  =  numpy.array([  q[0],   0      ,    0   ,   0   ,  q[2]    ,   q[1]  ] )
    polY  =  numpy.array([     0 ,   q[1]  ,    0   ,  q[2],  0        ,   q[0]  ] )
    polZ  =  numpy.array([     0 ,   0      ,  q[2] ,  q[1],  q[0]    ,   0      ] )

    gammas.append( polX) ## polarisation X

    gammas.append( polY) ## polarisation Y
    gammas.append( polZ) ## polarisation Z

    gammas.append( polZ+polY ) ## polarisation Z+Y
    gammas.append( polZ+polX ) ## polarisation Z+X
    gammas.append( polY+polX ) ## polarisation Y+X
    
    # X
    
    vect = numpy.zeros([dm.shape[0]],"d")
    vect[0::3]=1
    el_ene.append( (numpy.dot( vect, numpy.dot(dm, vect) )).real )

    # Y
    vect[:]=0
    vect[1::3]=1
    el_ene.append( (numpy.dot( vect, numpy.dot(dm, vect) )).real )
    
    # Z
    vect[:]=0
    vect[2::3]=1
    el_ene.append( (numpy.dot( vect, numpy.dot(dm, vect) )).real )
    
    # YZ
    vect[:]=0
    vect[2::3]=1
    vect[1::3]=1
    el_ene.append( (numpy.dot( vect, numpy.dot(dm, vect) )).real )
    
    # XZ
    vect[:]=0
    vect[2::3]=1
    vect[0::3]=1
    el_ene.append( (numpy.dot( vect, numpy.dot(dm, vect) )).real )
    
    # XY
    vect[:]=0
    vect[1::3]=1
    vect[0::3]=1
    el_ene.append( (numpy.dot( vect, numpy.dot(dm, vect) )).real )

  # -----------------------------------------------------  
  M_par2ene = numpy.zeros([len(el_ene), 21],"d")

  # index2alphabeta = [[0,0]]*21
  # index2factor    = [[1]]*21
  index2alphabeta = numpy.zeros([21,2],"i")
  index2factor    = numpy.zeros(21,"f")

  count=0
  for alpha in range(6):
    for beta in range(6):
      if beta>= alpha:
        index2alphabeta[count][0] = alpha
        index2alphabeta[count][1] = beta
        
        if(alpha==beta):
          index2factor[count]       = 0.5 
        else:
          index2factor[count]       = 1.0
          
        count += 1
 
  #print( numpy.array(gammas).shape)
  for Gamma, ene, iene in zip( gammas, el_ene, range(len(el_ene))):

    for count in range(21):
      M_par2ene[iene, count]  = index2factor[count] * Gamma[index2alphabeta[count][0]]*Gamma[index2alphabeta[count][1]]

  leastsq = numpy.linalg.pinv( M_par2ene )

  solution = numpy.dot(leastsq, el_ene  )
  
  # print( numpy.abs(el_ene).sum())
  # print( numpy.abs(el_ene-numpy.dot(M_par2ene, solution  ) ).sum())
  # solution = solution + numpy.dot(leastsq,el_ene-numpy.dot(M_par2ene, solution     ))
  # print( numpy.abs(el_ene-numpy.dot(M_par2ene, solution  ) ).sum())
  # dd2 = numpy.dot(M_par2ene, solution  ) 
  # sol2 =  numpy.dot(leastsq, dd2  )
  # print( numpy.abs(dd2-numpy.dot(M_par2ene, sol2  ) ).sum())
  # print( "==========================")

  C = numpy.zeros([6,6],"f")
  for count in range(21):
    C[ index2alphabeta[count][0],index2alphabeta[count][1] ]=solution[count]
    C[ index2alphabeta[count][1],index2alphabeta[count][0] ]=solution[count]


  ## FACTORS

  #####################################################################
  ###  castep eigenval are given in cm-1
  ###  Below is the factor to change evals**2 to omega squared in seconds-2
  F1 = (2*math.pi   * 2.99792458e+10 )**2

  ######################################################
  ### The energy of an harmonic oscillator is omega**2*amplitude**2 /2
  ### below is 1.0/2
  F2=0.5

  ##############################################3
  ## This converts the mass au to grams
  ##
  F3 = 1.66053892e-24

  #######################################################
  ## 
  ##  in the fit the variations depends on Qs. Qs are in Angstroems-1
  ##  Below is the factor A->cm squared
  F4 = 1.0e-16
  
  #############################################################################
  ##
  ##  We have calculated for a unit cell and elastic tensor is energy 
  ##  over volume.  Below is the inverse of the cell volume  in cm-3
  F5 = 1.0/ calculatedDatas.GetCellVolume() *1.0e+24
  
  ##############################################
  ##
  ## factor 1e-10 to get GPa
  F6 = 1.0e-10

  C=C*F1*F2*F3*F4*F5*F6
  

  ##################################################################

  print( "Elastic tensor in GPa")
  for c1, c2, c3, c4, c5, c6  in C:  
    print( "%12.8e | %12.8e | %12.8e | %12.8e | %12.8e | %12.8e" % (c1, c2, c3  , c4, c5, c6  ))

  # print( Ns)
  ### A1-A2 + A3-A4 = 2( A2-A3)
  ### A4   =  A1-3*A2 + 3*A3

  # pos = numpy.array([1,4,8],"i")
  # ds  = numpy.array([10,1,3],"i")

  # A1 = dicotrack[ tuple(pos)       ] 
  # A2 = dicotrack[ tuple(pos+ds)    ] 
  # A3 = dicotrack[ tuple(pos+2*ds)  ] 
  # A4 = dicotrack[ tuple(pos+3*ds)  ] 
  
  # B4 = A1-3*A2 + 3*A3

  # print( "----------")
  # diff = numpy.abs(A4-B4)
  # pos = numpy.argmax(diff)


  points = numpy.array(points)
  values =  numpy.array(values)

  D = points.max()

  xi = numpy.arange(0,D,D/200)
  N = len(xi)

  xis = numpy.zeros( [N,3],"f" )
  xis[:,2] = xi
  
  import scipy.interpolate
  
  xis = numpy.zeros( [N,3],"f" )
  xis[:,2] = xi
  valsZ = scipy.interpolate.griddata(points, values, xis, method='linear', fill_value=0.0)
  
  
  xis = numpy.zeros( [N,3],"f" )
  xis[:,1] = xi
  valsY = scipy.interpolate.griddata(points, values, xis, method='linear', fill_value=0.0)
  
  
  xis = numpy.zeros( [N,3],"f" )
  xis[:,0] = xi
  valsX = scipy.interpolate.griddata(points, values, xis, method='linear', fill_value=0.0)

  res = numpy.array([xi, valsX, valsY, valsZ   ]).T

  numpy.savetxt( "curvemin.txt", res)
  
  
