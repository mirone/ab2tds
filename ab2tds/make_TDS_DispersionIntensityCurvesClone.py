
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/

""" The script make_TDS_DispersionIntensityCurvesClone calculates IXS and INS intensities using only the eigenvectors and eigenvalues provided py the input file. The following steps must have been performed beforehand: 
    
      * symmetrisation
      * Fourier transform
      * Debye waller calculation for the temperature of choice
    
    * The usage is  ::

            make_TDS_DispersionIntensityCurvesClone QW_castep_filename input_filename castep_filename

      the file *castep_filename* can be either the name of the original castep output containing eigenvectors and eigenvalues on the MP grid.
      or the associated hdf5 file. In any case the associated hdf5 file must exist already ( make_TDS_Simmetrization
      must be runned beforehand )

      the file *QW_castep_filename*  must contain eigenvectors and eigenvalues for the q-values of interest. A list of q-values can be created by the script make_TDS_DispersionIntensityCurves, eigenvectors and eigenvalues on these q-points must be calculated by an external program (e.g. CASTEP)

    * The input_file must set the variables :

      * Mandatories

        *  APPLYTIMEREVERSAL
        *  Temperature
        *  resolutionfile
        *  Saturation
        *  lowerLimit
        *  bottom_meV
        
      * If NEUTRONCALC==0 ( default )

        * Lambda

      * If NEUTRONCALC==1

        * CohB
        * NeutronE

      * Optional

        * branchWeight
        * Eigscal
        * UniqueIon

The input variables are documented with docstrings below
  
""" 





from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from . import startmessage

from . import TDS_Simmetry      
from . import TDS_Reading
from . import TDS_Interpolation
import h5py
import matplotlib.cm as cm


import numpy

import sys
import time
import copy

import string
from . import  dabax
import math
from . import EdfFile
import os
import tempfile
import subprocess

if(sys.argv[0][-12:]!="sphinx-build"):
  s=open(sys.argv[2], "r").read()
  exec(s)

def check_input():
  
  assert("Temperature"  in dir())

  assert("resolutionfile"  in dir())
  assert("APPLYTIMEREVERSAL"  in dir())
  assert("Saturation"  in dir())
  assert("lowerLimit"  in dir())
  assert("bottom_meV"  in dir())

  if "NEUTRONCALC" in dir() and NEUTRONCALC:
    assert( "CohB" in dir())
    assert( "NeutronE" in dir())
  else:
    assert("Lambda"  in dir())

if(sys.argv[0][-12:]!="sphinx-build"):
  check_input()

APPLYTIMEREVERSAL=1
"""
  this is one by default. Must be coherent with previous steps
"""

Temperature=100
""" 
The hdf5 file must include DW factors calculated ALSO at this temperature.
 The temperature at which DW factors have been calculated.
   Units are Kelvin
"""
resolutionfile=""
""" a two column file : first the energy in cm-1, second the value of resolution function
"""

Lambda=1.0
""" For X-ray scattering : the wavelenght in Angstroems.
"""

Saturation=0
""" To limit intensity at peaks : intensity is saturated at this value
"""

lowerLimit=0
""" To correct the dynamical range, when displaying, the intensity is clipped to prevent it going below this value
"""

bottom_meV=0
""" Calculated eigenvalues are clipped to this value before use.
"""

NEUTRONCALC=0
""" This activate neutron scattering calculations.
"""
CohB=None
""" When neutroncalc is on, this must be a dictionary : for each atom name the Coherent lenght.
"""
NeutronE=0
""" When neutroncalc is on, the neutron energy in meV
"""
branchWeight=None
""" a list of weights : one per branch.
"""

Eigscal=0
"""
EigScal==1   Intensity are calculated WITH only eigenvector scalar products ; ==2 Intensity are calculated WITH only eigenvector scalar products PLUS Mass factor
"""

UniqueIon=-1
""" If >=0. Selects one ion. All the other will be silent.
"""



def main():
  

  if "EigScal" in dir():
      print( "EigScal  option has been set , in input" )
  else:
      EigScal=0

  if EigScal==1:
    print( " Intensity are calculated WITH only eigenvector scalar products ")
  elif   EigScal==2:
      print( " Intensity are calculated WITH only eigenvector scalar products PLUS Mass factor")
  else:
    print( " Intensity are calculated WITH  more than just   eigenvector scalar products")


  if branchWeight is not None:
    print( " CONSIDERING WEIGTHED BRANCHES AS GIVEN BY  branchWeight = ", branchWeight)

  CMGRAY=cm.gray
  if "COLOR" in globals():
    if COLOR:
      CMGRAY=None

  resolution=[]
  file=open(resolutionfile, "r")
  for line in file:
    toks = map(string.atof, string.split(line))
    if len(toks)==2:
      resolution.append(toks)

  resolution=numpy.array(resolution)

  ########################################################
  # 
  # Q=2*sin(theta)*2*Pi/lambda
  def ThetaCheck(Q, Lambda  ):
    dum = numpy.sqrt( numpy.sum(Q*Q, axis=0)) * Lambda/2.0/math.pi/2.0
    if dum<=1.0:
      return numpy.arcsin(  dum )
    else:
      return None

  def Theta(Q, Lambda  ):
    dum = numpy.sqrt( numpy.sum(Q*Q, axis=1)) * Lambda/2.0/math.pi/2.0
    return numpy.arcsin( dum  )


  #########################################################################

  calculatedDatas = TDS_Reading.CalcDatas.get_CalcDatas_Object_From_File(sys.argv[1])


  if UniqueIon != -1 :
      N_ions=len(calculatedDatas.atomNames)   
      UniqueFilter = numpy.zeros( N_ions  , dtype= numpy.float32 )
      UniqueFilter[UniqueIon]=1.0



  redLine=[]
  tickpos=[]
  ticklabel=[]

  count=0
  for redStart, redEnd, Nqline in zip(     redStarts, redEnds, Nqlines     ):
    count=count+1

    if Nqline<2:
      raise Exception( "Nqline<2")

    tickpos.append(len(redLine))
    ticklabel.append(str(redStart ) )

    if count==len(Nqlines):
      ndemo=Nqline-1
    else:
      ndemo=Nqline

    addLine = numpy.array(redStart)+(((numpy.array(redEnd)-numpy.array(redStart) )))*(( numpy.arange(Nqline)*1.0/(ndemo))[:,None])
    redLine.extend( addLine )


#  redLine=calculatedDatas.QsReduced
#  tickpos=[len(redLine)]
#  ticklabel=[ str(redLine[-1]) ]
#  tickpos=numpy.array(tickpos, numpy.float32)/(len( redLine))
#  redLine=numpy.array(redLine)



  md5postfix= calculatedDatas.Get_md5postfix()
  filename = calculatedDatas.Get_filename()

  cella=TDS_Simmetry.OP_cella(cellvectors=calculatedDatas.cellVects ,
                 AtomNames_long=calculatedDatas.atomNames,
                 PositionsList_flat=calculatedDatas.atomAbsolutePositions )

  simmetries_dict = TDS_Simmetry.FindSimmetries(cella,   filename= filename,
                                                md5postfix=md5postfix, overwrite= False,
                                                key = "simmetries_dict" )




  # print( "GOING TO RETRIEVE FOURIER TRASFORM  Nfour_interp = ", Nfour_interp)

  # Replica = TDS_Simmetry.GetAllReplicated (  calculatedDatas, simmetries_dict,   filename= filename,
  #                                            md5postfix=md5postfix,
  #                                            overwrite= False ,
  #                                            key = "Codes_"+(str(APPLYTIMEREVERSAL)) ,
  #                                            APPLYTIMEREVERSAL=APPLYTIMEREVERSAL,
  #                                            CALCULATEFOURIER =1 ,
  #                                            CALCULATECOMPLEMENT=0,
  #                                            Nfour_interp=Nfour_interp,
  #                                            MAKING=0,
  #                                            MAKINGFOURIER=0
  #                                      )

  #################################################################################3
  ## 
  ##         A DISPERSION CURVE
  ##

  BV=calculatedDatas.GetBrillVects()
  qsLine =  calculatedDatas.GetClippedQs(QsReduced=redLine)
  qsLine_notShifted =   numpy.dot( redLine ,  BV  )



  if NEUTRONCALC:
      scatterers = numpy.array([  CohB[aname] for aname in  calculatedDatas.atomNames ] , "F" )   
  else:
      tf0 = dabax.Dabax_f0_Table("f0_WaasKirf.dat")
      lambdas= [Lambda]*len(qsLine_notShifted)
      scatterers = [  tf0.Element(aname  ) for aname in  calculatedDatas.atomNames ] 
      scattFactors_site_q =numpy.array( [ scatterer.f0Lambda(numpy.array(lambdas), Theta(qsLine_notShifted, Lambda  )   )
                                      for scatterer in scatterers ]  ).astype(numpy.float32)

      scattFactors_q_site= scattFactors_site_q.T
      

  file=open("alongthelineF.dat", "w")
  Nbranches=3*len(calculatedDatas.atomNames)

  kinematicalFactors      =  numpy.zeros( [  len(qsLine_notShifted),  Nbranches//3  ] , numpy.complex64)
  kinematicalFactors.imag = numpy.tensordot(   qsLine_notShifted,
                                            -calculatedDatas.atomAbsolutePositions  ,  # the minus sign is here
                                            [[1], [ 1 ]] ) 
  kinematicalFactors = numpy.exp( kinematicalFactors )

  massfactors  = numpy.sqrt(1.0/ calculatedDatas.atomMasses )





  h5=h5py.File(sys.argv[3] ,  "r")
  DWs_3X3 = h5["Codes_"+(str(APPLYTIMEREVERSAL))]["DWs"][str(Temperature)]["DWf_33"][:]



  dwfacts_perq = numpy.tensordot(  qsLine_notShifted,DWs_3X3    ,  axes=[[1], [ 1]])

  dwfacts_perq  = dwfacts_perq  *qsLine_notShifted[:,None,:]
  dwfacts_perq  = numpy.exp(-numpy.sum(dwfacts_perq , axis=2 ))



  if EigScal==2 :
      dwfacts_mass_kin_scatt_perq  = massfactors*kinematicalFactors
  elif EigScal==1:
      dwfacts_mass_kin_scatt_perq  = kinematicalFactors
  else:
      if NEUTRONCALC:
          dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*  scatterers  
      else:
          dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*scattFactors_q_site

  if UniqueIon !=-1:
      dwfacts_mass_kin_scatt_perq  = dwfacts_mass_kin_scatt_perq  *UniqueFilter





  # cambia temperatura in Hartree
  Temperature_Hartree=Temperature /11604.50520/27.211396132

  # amu = 1822.8897312974866 electrons
  # cm-1 ==> Hartree  cm-1 =  4.5563723521675276e-06 Hartree
  # Bohr = 0.529177249 Angstroems
  factor_forcoth = 4.5563723521675276e-06*1.0/(2.0*Temperature_Hartree)

  resevals=[]
  resstokes=[]

  stime=time.time()

  # DMs = [  TDS_Simmetry.GetDM_fromF(q,  Replica["Four_Rvects"],   Replica["Four_DMs"]  ) for q in   qsLine  ]

  # print( " CREATED DMS in " , time.time()-stime, " seconds ")
  # stime=time.time()


  # la linea sotto perche' Castep da gli autovettori non periodici
  shiftsToBz = calculatedDatas.GetClippedQs()-calculatedDatas.GetNonClippedQs()
  positions = calculatedDatas.atomAbsolutePositions
  phase_facts = numpy.tensordot( shiftsToBz ,  positions, axes= [(1),(1)])
  #  phase is already OK 
  phase_facts=(numpy.exp(-0.0j*phase_facts)).astype( numpy.complex64 )





  count=0  

  for (redQ, q , qnotshifted, 
       dwfacts_mass_kin_scatt , phfct  ) in zip(redLine,
                                               qsLine,
                                               qsLine_notShifted,
                                               dwfacts_mass_kin_scatt_perq,
                                               phase_facts 
                                               ):

    # dm = TDS_Simmetry.GetDM_fromF(q,  Replica["Four_Rvects"],   Replica["Four_DMs"]  )
    # evals, evects = numpy.linalg.eigh(dm)


    evals= calculatedDatas.frequencies[count]
    evects= numpy.array((calculatedDatas.eigenvectors[count]))


    count=count+1 

    evectsshape= evects.shape
    evects.shape=(evectsshape[0], evectsshape[1]//3, 3)
    evects[:] = evects*phfct[None,:,None]
    evects.shape =  evectsshape

    # originally the procedure beklow which has been copied,
    # was getting vects obtained from numpy.linalg.eig
    # which gives vectors as columns
    evects=numpy.transpose(evects)


    evals=numpy.abs(evals )
    evals=(numpy.maximum(evals, numpy.array([ bottom_meV/(0.0001239852 *1000) ], numpy.float32) ))

    qvect_s = numpy.tensordot(  qnotshifted ,      evects.reshape([-1,3, Nbranches]) , [[0], [1]   ]  )

    Fjs = numpy.sum(dwfacts_mass_kin_scatt* (qvect_s.T), axis=-1)  # sommato sugli ioni


    exp_plus =  numpy.exp( factor_forcoth * evals)
    exp_minus = numpy.exp(-factor_forcoth * evals)

    intensity_array = ( Fjs*(Fjs.conjugate())).real

    deno = (exp_plus-exp_minus)*evals

    if NEUTRONCALC:
        kinstokes = numpy.sqrt(numpy.maximum( (NeutronE-evals*(0.0001239852 *1000))/NeutronE, 0.0 ))
        kinantistk= numpy.sqrt((NeutronE+evals*(0.0001239852 *1000))/NeutronE)
    else:
        kinstokes = 1.0
        kinantistk= 1.0


    if EigScal==0:
      intensity_stokes     =       intensity_array*      exp_plus/deno *kinstokes  
      intensity_antistokes =       intensity_array*      exp_minus/deno  *kinantistk
    else:
      intensity_stokes     =       intensity_array *kinstokes
      intensity_antistokes =       intensity_array *kinantistk

    if branchWeight is not None:
      intensity_stokes     =   intensity_stokes     *numpy.array(branchWeight)  *kinstokes  
      intensity_antistokes =    intensity_stokes    *numpy.array(branchWeight) *kinantistk

    freqmev = evals*0.0001239852 *1000
    # mask = numpy.less(0.5, freqmev)
    # The problem of almost-zero frequencies is treated with an impot parameter
    # and a maximum operator
    mask = numpy.less(0.0, freqmev)

    towrite=  numpy.array([freqmev,  intensity_stokes*mask ,  intensity_antistokes*mask  ])


    resevals.append(numpy.concatenate( [-evals*0.0001239852 *1000,  evals*0.0001239852 *1000] ) )
    resstokes.append( numpy.concatenate( [   intensity_antistokes*mask , intensity_stokes*mask  ] )   )

    towrite=numpy.reshape(towrite.T, [-1])


    file.write("%e %e %e " % tuple(redQ.tolist())  )
    file.write((("%e "*(len(towrite)) )+"\n") % tuple( towrite.tolist()
        )  
               )
    # file.write((("%e "*len(dm) )+"\n") % tuple(dm[0].real.tolist())  )

  print( " Calculated intensities  in " , time.time()-stime, " seconds ")

  file=None

  resevals=numpy.array(resevals)

  evalsWmin=numpy.amin(resevals)
  evalsWmax=numpy.amax(resevals)
  step = (resolution[-1,0]-resolution[0,0])/len(resolution)
  Wmin = evalsWmin+resolution[0,0]
  Wmax = evalsWmax+resolution[-1,0]

  print( " MIN MAX , " , numpy.amin(resevals) ,  numpy.amax(resevals))
  Ws=numpy.arange(Wmin,Wmax+step*0.9, step )
  Wmax = Ws[-1]

  res = numpy.zeros( [len(redLine), len(Ws)], numpy.float32  )
  minres = numpy.min( resolution[:,1] )
  reso=numpy.interp( Ws ,resolution[:,0] ,resolution[:,1] , left=minres, right= minres)

  reso[Ws<resolution[0,0]] = minres*(resolution[0,0]/Ws[ Ws<resolution[0,0]])**2
  reso[Ws> resolution[-1,0]] = minres*(resolution[-1,0]/Ws[ Ws>resolution[-1,0]])**2



  print( " ")
  print( "minres est ", minres)
  file=open("reso.dat", "w")
  for om,da in zip(Ws, reso):
    file.write("%e %e \n"%(om,da ))

  resofft=numpy.fft.fft(reso)
  freqs = numpy.fft.fftfreq (len(Ws),step)*2*math.pi


  for (redQ, ws , intens , target ) in zip(redLine,resevals, resstokes, res ):
    facts = numpy.sum( numpy.exp ( -1.0j* freqs[:,None]*  ws    ) * intens , axis = 1  )

    target[:]=numpy.fft.ifft(  facts*resofft  ).real

  #
  res=res.T

  # la res, plottee ca commence par le haut  
  res=res[::-1]

  import numpy as np
  import matplotlib
  import matplotlib.cm as cm
  import matplotlib.mlab as mlab
  import matplotlib.pyplot as plt
  from matplotlib.pyplot import figure, show, axes, sci
  from matplotlib import cm, colors
  from matplotlib.font_manager import FontProperties

  fig=figure()
  axes=fig.add_subplot(111)



  log10=math.log(10.0)

  ima=axes.imshow(numpy.log(    numpy.maximum( numpy.minimum(numpy.maximum(res,1.0e-14),Saturation ), lowerLimit)    )  /log10    ,cmap=CMGRAY)


  n_pix_y, n_pix_x=  res.shape

  step_x = 1.0/n_pix_x
  step_y = (Wmax-Wmin)/(n_pix_y-1)

  ima.set_extent([0-0.5*step_x,1.+0.5*step_x, Wmin -0.5*step_y,  Wmax + 0.5*step_y] )

  fig.colorbar(ima)

  axes.set_aspect('auto')


  axes.xaxis.set_ticks(numpy.array(tickpos) )


  axes.xaxis.set_ticklabels(ticklabel )

  class event_manager:
    def __init__(self, fig, log_lin):
      self.fig=fig
      self.log_lin=log_lin
      self.last_line=[None,None, None,None]
      self.inSelection=False
      self.Cqred=None
      self.CnbA = None
      self.CnbB = None
      self.Clast_line=[None,None, None,None]
      self.pinredline = None
    def onpress(self, event ):

      print( " pressed key" , event.key)
      if event.key=='C':
        self.inSelection=not  self.inSelection
      if event.key in ['M'] and None not in [ self.Cqred,self.CnbA,self.CnbB]:
        print( " MASSIMIZZO CONTRASTO " )
        CalculateContrast(self.Cqred,self.CnbA,self.CnbB)

      if event.key in ['m'] :
        (fd, filename) = tempfile.mkstemp()
        try:
          tfile = os.fdopen(fd, "w")
          tfile.write("Cqred=numpy.%s\nCnbA=%s\nCnbB=%s\n"%(repr(self.Cqred) ,self.CnbA,self.CnbB ))
          tfile.close()
          subprocess.Popen(["emacs", filename]).wait() 
          s=open(filename,"r").read()
        finally:
          dic={}
          exec(s ,globals(),dic)
          self.Cqred ,self.CnbA,self.CnbB= dic["Cqred"], dic["CnbA"], dic["CnbB"]
          os.remove(filename)
          self.plot_lines( doA=1, doB=1)




    def plot_lines(self, doA=0, doB=0):
      if self.Clast_line[3] is None :
        self.Clast_line[3]=ax=self.fig.add_subplot(111)
      else:
        ax = self.Clast_line[3]

      if doA:
        if self.Clast_line[0] is not None:
          self.Clast_line[0].remove( )
        print( " PLOTTO ")
        self.Clast_line[0] = matplotlib.lines.Line2D(numpy.linspace(0.0,1.0,len(resevals)),
                                                       resevals[:,self.CnbA] , linewidth=2.0, color="b")
        ax.add_line( self.Clast_line[0] )
        self.fig.show()


      if doB:
        if self.Clast_line[1] is not None:
          self.Clast_line[1].remove( )

        self.Clast_line[1] = matplotlib.lines.Line2D(numpy.linspace(0.0,1.0,len(resevals)),
                                                       resevals[:,self.CnbB], linewidth=2.0 , color="w")
        ax.add_line( self.Clast_line[1] )
        self.fig.show()





    def onclick(self, event ):
      if self.inSelection:
        print( " inSelection " , event.xdata, event.ydata,  event.x, event.y)
        x0 = event.xdata
        y0 = event.ydata
        x = event.x
        y = event.y

        pinredLine = int( 0.5  +  (len(redLine)-1)*x0)
        if pinredLine>=0 and pinredLine<len(redLine):
          self.pinredline =pinredLine
          evals=resevals[pinredLine]
          imin=numpy.argmin(numpy.abs(evals-y0))

          self.plot_lines( doA=0, doB=0)

          if event.button == 1: 
            self.Cqred = redLine[pinredLine]
            self.CnbA  = imin

            self.plot_lines( doA=1, doB=0)


          if event.button == 3: #right click
            self.CnbB  = imin
            self.plot_lines( doA=0, doB=1)

          print( " SELECTED BRANCHES A and B " , self.CnbA,self.CnbB, "  AT  " ,  self.Cqred)

        return

      if event.button in [1,2] and self.last_line[2] is not None:
        data=self.last_line[2]
        if event.button == 1:
          data[:]=data/1.5
        else:
          data[:]=data*1.5


        self.last_line[1].remove( )

        self.last_line[1] = matplotlib.lines.Line2D( data  ,Ws[::-1] , color="r")

        if self.last_line[3] is None :
          self.last_line[3]=ax=self.fig.add_subplot(111)
        else:
          ax = self.last_line[3]

        ax.add_line( self.last_line[1] )
        self.fig.show()


      if event.button == 3: #right click
        x0 = event.xdata
        y0 = event.ydata
        x = event.x
        y = event.y


        pinredLine = int( 0.5  +  (len(redLine)-1)*x0)
        if pinredLine>=0 and pinredLine<len(redLine):

          print( redLine[pinredLine])
          if self.last_line[0] is not None:
            self.last_line[0].remove( )
            self.last_line[1].remove( )

          if self.last_line[3] is None :
            self.last_line[3]=ax=self.fig.add_subplot(111)
          else:
            ax = self.last_line[3]

          self.last_line[0] = matplotlib.lines.Line2D([0.0+x0,0.0+x0], [Wmin  ,Wmax   ], color="r")

          if(self.log_lin=="lin"):
            self.last_line[2] = res[:,pinredLine]/numpy.max(res[:,pinredLine])

            f=open("click.dat", "w")
            for x,y in zip(Ws[::-1],res[:,pinredLine] ):
              f.write("%e %e \n"%(x,y   ))
            f=None
          else:
            fmin= numpy.log(numpy.min(res[:,pinredLine]) )
            fmax= numpy.log(numpy.max(res[:,pinredLine]))

            self.last_line[2] = (numpy.log( res[:,pinredLine]) - fmin ) /(fmax-fmin)

          print( "NORMALIZATION TO ", numpy.max(res[:,pinredLine]))
          print( "MIN   ", numpy.min(res[:,pinredLine]))


          self.last_line[1] = matplotlib.lines.Line2D(  self.last_line[2]  ,Ws[::-1] , color="r")
          #self.last_line[1] = matplotlib.lines.Line2D( [1.0,0.0] ,[ 0 ,Wmax   ], color="r")


          ax.add_line( self.last_line[0] )
          ax.add_line( self.last_line[1] )
          self.fig.show()


  ##########################################################################


  def CalculateContrast(CredQ,CnA,CnB):
  #################################################################################3
  ## 
  ##         A DISPERSION CURVE
  ##

    BV=calculatedDatas.GetBrillVects()
    Credline =  [ ]
    thetas=[]
    for i in range(-6,7):
      for j in range(-6,7) :
        for k in range(-6,7)   :
          dumRedQ = CredQ+numpy.array([i,j,k])
          dumQ  = numpy.dot( dumRedQ ,  BV  )
          dumTheta= ThetaCheck(dumQ, Lambda)
          if dumTheta is not None:
            Credline.append(dumRedQ)
            thetas.append(dumTheta*180.0/math.pi )

    CqsLine =  calculatedDatas.GetClippedQs(QsReduced=Credline)
    CqsLine_notShifted =   numpy.dot( Credline ,  BV  )

    if NEUTRONCALC:
      scatterers = numpy.array([  CohB[aname] for aname in  calculatedDatas.atomNames ] , "F" )   
    else:
      tf0 = dabax.Dabax_f0_Table("f0_WaasKirf.dat")
      lambdas= [Lambda]*len(CqsLine_notShifted)
      scatterers = [  tf0.Element(aname  ) for aname in  calculatedDatas.atomNames ] 
      scattFactors_site_q =numpy.array( [ scatterer.f0Lambda(numpy.array(lambdas), Theta(CqsLine_notShifted, Lambda  )   )
                                          for scatterer in scatterers ]  ).astype(numpy.float32)
      scattFactors_q_site= scattFactors_site_q.T

    totNbranches=3*len(calculatedDatas.atomNames)

    if CnA>= totNbranches:
      CnA = CnA-totNbranches
      stokA=1
#    else:
#      CnA = (totNbranches-1)-CnA
#      stokA=0
    else:
      CnA = CnA
      stokA=0


    if CnB>= totNbranches:
      CnB = CnB-totNbranches
      stokB=1
#    else:
#      CnB = (totNbranches-1)-CnB
#      stokB=0
    else:
      CnB = CnB
      stokB=0

    print( " VADO A LEGGERE HD5")

    Nbranches=2
    Natoms = len(calculatedDatas.atomNames )
    kinematicalFactors      =  numpy.zeros( [  len(CqsLine_notShifted),  Natoms  ] , numpy.complex64)
    kinematicalFactors.imag = numpy.tensordot(   CqsLine_notShifted,
                                                 -calculatedDatas.atomAbsolutePositions  ,  # the minus sign is here
                                                 [[1], [ 1 ]] ) 
    kinematicalFactors = numpy.exp( kinematicalFactors )
    massfactors  = numpy.sqrt(1.0/ calculatedDatas.atomMasses )

    h5=h5py.File(sys.argv[3] ,  "r")
    DWs_3X3 = h5["Codes_"+(str(APPLYTIMEREVERSAL))]["DWs"][str(Temperature)]["DWf_33"][:]

    dwfacts_perq = numpy.tensordot(  CqsLine_notShifted,DWs_3X3    ,  axes=[[1], [ 1]])

    dwfacts_perq  = dwfacts_perq  *CqsLine_notShifted[:,None,:]
    dwfacts_perq  = numpy.exp(-numpy.sum(dwfacts_perq , axis=2 ))


    print( "CALCOLO QUALCHE FATTORE ")
    if EigScal==2 :
      dwfacts_mass_kin_scatt_perq  = massfactors*kinematicalFactors
    elif EigScal==1:
      dwfacts_mass_kin_scatt_perq  = kinematicalFactors
    else:
      if NEUTRONCALC:
        dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*  scatterers  
      else:
        dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*scattFactors_q_site

    if UniqueIon !=-1:
      dwfacts_mass_kin_scatt_perq  = dwfacts_mass_kin_scatt_perq  *UniqueFilter

      # cambia temperatura in Hartree
    Temperature_Hartree=Temperature /11604.50520/27.211396132

    # amu = 1822.8897312974866 electrons
    # cm-1 ==> Hartree  cm-1 =  4.5563723521675276e-06 Hartree
    # Bohr = 0.529177249 Angstroems
    factor_forcoth = 4.5563723521675276e-06*1.0/(2.0*Temperature_Hartree)

    resevals=[]
    resstokes=[]

    stime=time.time()

    print( " ESTRAGGO DM ED EVALS EVECTS " )


    ## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#    dm = TDS_Simmetry.GetDM_fromF(CqsLine[0],Replica["Four_Rvects"],Replica["Four_DMs"],calculatedDatas )
#    evals, evects = numpy.linalg.eigh(dm)    
#    evals=numpy.array([evals[CnA], evals[CnB]])
#    print( 'evals' , evals)
#    evects =  numpy.array([evects[:,CnA], evects[:,CnB]])
#    evals=numpy.sqrt(numpy.maximum(evals, numpy.array([1.0e-16], numpy.float32) ))
#    print( 'evects' , evects)

    print( " CHIAMO NUOVI PUNTI " )
#    for (redQ, q , qnotshifted, 
#         dwfacts_mass_kin_scatt) in zip(Credline,
#                    CqsLine,
#                    CqsLine_notShifted,
#                    dwfacts_mass_kin_scatt_perq
#                    ):
    count = 0
    for (redQ, q , qnotshifted, 
          dwfacts_mass_kin_scatt , phfct  ) in zip(redLine,
                    qsLine,
                    qsLine_notShifted,
                    dwfacts_mass_kin_scatt_perq,
                    phase_facts 
                    ):
      evals= calculatedDatas.frequencies[count]
      evects= numpy.array((calculatedDatas.eigenvectors[count]))


      count=count+1 

      evectsshape= evects.shape
      evects.shape=(evectsshape[0], evectsshape[1]/3, 3)
      evects[:] = evects*phfct[None,:,None]
      evects.shape =  evectsshape

    # originally the procedure beklow which has been copied,
    # was getting vects obtained from numpy.linalg.eig
    # which gives vectors as columns
      evects=numpy.transpose(evects)


      evals=numpy.abs(evals )
      evals=(numpy.maximum(evals, numpy.array([ bottom_meV/(0.0001239852 *1000) ], numpy.float32) ))


      evals=numpy.array([evals[CnA], evals[CnB]])
#      print( 'evals' , evals)
      evects =  numpy.array([evects[:,CnA], evects[:,CnB]])
      evals=numpy.sqrt(numpy.maximum(evals, numpy.array([1.0e-16], numpy.float32) ))


      qvect_s = numpy.tensordot(  qnotshifted ,      evects.reshape([-1,3, Nbranches]) , [[0], [1]   ]  )

      Fjs = numpy.sum(dwfacts_mass_kin_scatt* (qvect_s.T), axis=-1)  # sommato sugli ioni

      exp_plus =  numpy.exp( factor_forcoth * evals)
      exp_minus = numpy.exp(-factor_forcoth * evals)

      intensity_array = ( Fjs*(Fjs.conjugate())).real

      deno = (exp_plus-exp_minus)*evals

      if NEUTRONCALC:
        kinstokes = numpy.sqrt(numpy.maximum( (NeutronE-evals*(0.0001239852 *1000))/NeutronE, 0.0 ))
        kinantistk= numpy.sqrt((NeutronE+evals*(0.0001239852 *1000))/NeutronE)
      else:
        kinstokes = 1.0
        kinantistk= 1.0


      if EigScal==0:
        intensity_stokes     =       intensity_array*      exp_plus/deno    *kinstokes
        intensity_antistokes =       intensity_array*      exp_minus/deno   *kinantistk
      else:
        intensity_stokes     =       intensity_array   *kinstokes
        intensity_antistokes =       intensity_array   *kinantistk

      if branchWeight is not None:
        intensity_stokes     =   intensity_stokes     *numpy.array(branchWeight) *kinstokes
        intensity_antistokes =    intensity_stokes    *numpy.array(branchWeight) *kinantistk

      freqmev = evals*0.0001239852 *1000
      mask = numpy.less(0.0, freqmev)

      towrite=  numpy.array([freqmev,  intensity_stokes*mask ,  intensity_antistokes*mask  ])

      resevals.append(numpy.concatenate( [-evals*0.0001239852 *1000,  evals*0.0001239852 *1000] ) )
      resstokes.append( numpy.concatenate( [   intensity_antistokes*mask , intensity_stokes*mask  ] )   )

    print( " Calculated intensities  in " , time.time()-stime, " seconds ")
    file=open("choosen_modes.txt","w")
    l=[]
    for redQ,theta,resE,resV in zip(Credline,thetas,resevals,resstokes):
      if(stokA ):
        AAe = resE[2]
        AAi = resV[2]
      else:
        AAe = resE[0]
        AAi = resV[0]
      if(stokB ):
        BBe = resE[3]
        BBi = resV[3]
      else:
        BBe = resE[1]
        BBi = resV[1]
      l.append( [AAi/BBi, AAi,BBi ] +  (redQ-CredQ).tolist() +[theta] )

    l.sort()
    l.reverse()

    file.write("#CreQ = %s   CnA = %s stokA=%d  CnB = %s stokB=%d  ENERGIES ==>   Ea=%e   Eb=%e  \n"%(str(CredQ),CnA,stokA,CnB,stokB, AAe, BBe ))
    for contr, AAi,BBi, sa,sb,sc , theta in l:
      file.write(" %e %e %e %e %e %e %e\n"% (sa,sb,sc, theta, AAi,BBi,contr ))
    


  #########################################################################

  dumlog=event_manager(fig, "log")

  cid = fig.canvas.mpl_connect('button_press_event', dumlog.onclick)
  fig.canvas.mpl_connect('key_press_event',dumlog.onpress)

  fig2=figure()
  axes=fig2.add_subplot(111)


  
  cumulated = res.sum(axis=1)/res.shape[1]
  np.savetxt('summedoverQs.dat', np.column_stack([Ws, cumulated])[::-1,:] , fmt='%20.10e', delimiter=' ')
  

  ima=axes.imshow(numpy.maximum( numpy.minimum(res,Saturation ), lowerLimit) ,cmap=CMGRAY)
  fig2.colorbar(ima)
  ima.set_extent([0-0.5*step_x,1.+0.5*step_x, Wmin -0.5*step_y,  Wmax + 0.5*step_y] )
  axes.xaxis.set_ticks(tickpos )
  axes.xaxis.set_ticklabels(ticklabel )

  axes.set_aspect('auto')

  dum=event_manager(fig2, "lin")
  cid2 = fig2.canvas.mpl_connect('button_press_event', dum.onclick)
  fig2.canvas.mpl_connect('key_press_event', dum.onpress)

  data4plot = numpy.sum(numpy.maximum( numpy.minimum(res,Saturation ), lowerLimit),axis=0)
  fig3=figure()
  axes=fig3.add_subplot(111)

  plt.plot(numpy.arange(len(data4plot)), data4plot )

  axes.xaxis.set_ticks(numpy.array(tickpos)*(len(data4plot)-1 ) )
  axes.xaxis.set_ticklabels(ticklabel )
  np.savetxt('alongtheline_TDS.dat', np.column_stack([numpy.arange(len(data4plot)), data4plot]) , fmt='%12.5f', delimiter=' ')


  show()
  fig.savefig("pippo.png")






