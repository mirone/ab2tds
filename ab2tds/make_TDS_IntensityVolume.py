
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function



""" The script make_TDS_intensityVolume calculates 3D distribution of TDS intensities for x-rays and neutrons. It can calculate partial and weighted densities of states, but this should be preferably done with make_TDS_IntensityVolumeOnlyHisto. It requires that you have previously done the following operations:

      * symmetrisation
      * Fourier transform
      * Debye waller calculation for the temperature of choice
    
    * The usage is ::

        make_TDS_IntensityVolume castep_filename input_filename  

      the file *castep_filename*  can be either the name of the original castep output or the associated hdf5 file. In any case the associated hdf5 file must exist already (make_TDS_Simmetrization must be run beforehand)

    * The input_file must set the variables :

      * Mandatories

          * APPLYTIMEREVERSAL
          * subN1
          * subN2
          * subN3
          * N1 
          * N2
          * N3
          * Nfour_interp
          * Temperature

      * If NEUTRONCALC==0 ( default )

          * Lambda
      * If NEUTRONCALC==1

          * CohB
          * NeutronE

      * If do_histogram==1

           * Nbins
           * xvdos

             * DOQLIMITS

                * QMIN=0
                * QMAX=0

      * The following block of three parameters are eithe all None (default), or outputname must be set together with 
         one  of branchWeight and energyWindow

           * outputname
           * branchWeight
           * energyWindow

      * set RemoveBose=1  if you want to remove Bose statistics factors

    * Output files

      The calculated intensities are written to a .ccp and .h5 file for visualization with chimera and pymca respectively.
      Density of states are written to histogram.h5, use pymca to open

The input variables are documented with docstrings below
  
""" 


from . import startmessage

from . import TDS_Simmetry      
from . import TDS_Reading
from . import TDS_Interpolation
import h5py


import numpy

import sys
import time
import copy
import string
from . import  dabax
import math


if(sys.argv[0][-12:]!="sphinx-build"):
  s=open(sys.argv[2], "r").read()
  exec(s)


def check_input(): 
  assert("Temperature"  in dir())
  assert("Nfour_interp"  in dir())
  assert("subN1"  in dir())
  assert("subN2"  in dir())
  assert("subN3"  in dir())
  assert("N1"  in dir())
  assert("N2"  in dir())
  assert("N3"  in dir())
  assert("APPLYTIMEREVERSAL"  in dir())
 
  if "NEUTRONCALC" in dir() and NEUTRONCALC:
      assert( "CohB" in dir())
      assert( "NeutronE" in dir())
  else:
      NEUTRONCALC=0
      assert("Lambda"  in dir())

  assert("do_histogram" in dir() )
  if(locals()["do_histogram"]):
    assert("Nbins"  in dir())
    assert("xvdos"  in dir())
    assert("DOQLIMITS" in dir())

    if (locals()["DOQLIMITS"]==1 ):
      print( " ACTIVATED QLIMITATIONS ")
      print( " between QMIN = " , locals()["QMIN"])
      print( " and QMAX = "     , locals()["QMAX"])


  if "branchWeight" in dir() or "energyWindow" in dir():
    assert("outputname" in dir())
  if "outputname" in dir():
    assert("branchWeight" in dir()  or  "energyWindow" in dir()  )

if(sys.argv[0][-12:]!="sphinx-build"):
  check_input()



APPLYTIMEREVERSAL=1
"""
  this is one by default. Must be coherent with previous steps
"""
subN1=20
""" Subdivision, in the elementary brillouin zone, along the Z dimension of the reconstructed volume, 
    which is parallel to the first axis of the brilloin zone.
"""
subN2=20
""" Subdivision, in the elementary brillouin zone, along the Y dimension of the reconstructed volume, 
    which is parallel to the second axis of the brilloin zone.
"""
subN3=20
""" Subdivision, in the elementary brillouin zone, along the X dimension of the reconstructed volume, 
    which is parallel to the third axis of the brilloin zone.
"""
N1 =  4
""" Besides being subdivised, the Brillouin zone, is replicated alonx the first axis  for -N1 up to N1-1 included. In reciprocal cell vector units. N1 can be either an integer or a pair of integer. In the latter case the first element is the starting shift,   the second is the ending shift+1.
"""
N2 =  4
"""
  same thing as N1 but along the second reciprocal cell vector
"""
N3 =  4
"""
  same thing as N1 but along the third reciprocal  cell vector
"""

Nfour_interp=4
"""  The number of points in each direction of the 3D reciprocal grid.
     The hdf5 file must contain a previous pretreatement done with the same parameter.
"""
Temperature=100
""" 
The hdf5 file must include DW factors calculated ALSO at this temperature.
 The temperature at which DW factors have been calculated.
   Units are Kelvin
"""
Lambda=0.2
""" For X-ray scattering : the wavelenght in Angstroems.
"""

NEUTRONCALC=0
""" This activate neutron scattering calculations.
"""
CohB=None
""" When neutroncalc is on, this must be a dictionary : for each atom name the Coherent lenght.
"""
NeutronE=0
""" When neutroncalc is on, the neutron energy in meV
"""
do_histogram=0
"""  this variable activsates the histogram calculations
"""
Nbins=0
"""
  Numer of bins in the histogram
"""
xvdos=0
"""
 when this option is on, the DOS is calculated with a weight given by scattering factors, structure factor, DW etc. etc.
  almost the same as the one used for scattering but without bose statistics.
"""
DOQLIMITS=0
""" If on : when calculating xvdos only contributions from q points having modulus inside a given window are considered.
"""
QMIN=0
"""  the minimum Q modulus for the DOQLIMITS=1 window
"""
QMAX=0
""" the maximum Q modulus for the DOQLIMITS=1 window
"""

outputname= None
""" a name to design the output file with data from calculation considering branchWeight or energyWindow
"""
branchWeight=None
""" a list of weights : one per branch.
"""
energyWindow=None
"""  a list of two numbers : minimum and maximum in meV
"""

RemoveBose=0
""" RemoveBose=1 desactivates the Bose statistics.
"""

try:
    from mpi4py import MPI
    myrank = MPI.COMM_WORLD.Get_rank()
    nprocs = MPI.COMM_WORLD.Get_size()
    procnm = MPI.Get_processor_name()
    comm = MPI.COMM_WORLD
    print( "MPI LOADED , nprocs = ", nprocs)
except:
    nprocs=1
    myrank = 0
    print( "MPI NOT LOADED ")

def main():

  DOQLIMITS=0

  if "ONLY_HISTO"  not in dir():
    ONLY_HISTO=0

  if type(N1)==type(1):
    N1=[-N1,N1]
  if type(N2)==type(2):
    N2=[-N2,N2]
  if type(N3)==type(3):
    N3=[-N3,N3]


  if RemoveBose==1:
    print( " Intensity are calculated WITHOUT  Bose factor")
  else:
    print( " Intensity are calculated WITH  Bose factor")



  ########################################################
  # 
  # Q=2*sin(theta)*2*Pi/lambda
  def Theta(Q, Lambda  ):
    return numpy.arcsin(  numpy.sqrt( numpy.sum(Q*Q, axis=-1)) * Lambda/2.0/math.pi/2.0 )
  #########################################################################

  calculatedDatas = TDS_Reading.CalcDatas.get_CalcDatas_Object_From_File(sys.argv[1])

  tf0 = dabax.Dabax_f0_Table("f0_WaasKirf.dat")
  scatterers = [  tf0.Element(aname  ) for aname in  calculatedDatas.atomNames ] 


  md5postfix= calculatedDatas.Get_md5postfix()
  filename = calculatedDatas.Get_filename()

  cella=TDS_Simmetry.OP_cella(cellvectors=calculatedDatas.cellVects ,
                 AtomNames_long=calculatedDatas.atomNames,
                 PositionsList_flat=calculatedDatas.atomAbsolutePositions )

  simmetries_dict = TDS_Simmetry.FindSimmetries(cella,   filename= filename,
                                                md5postfix=md5postfix, overwrite= False,
                                                key = "simmetries_dict" )


  if myrank==0: print( "GOING TO RETRIEVE FOURIER TRASFORM  Nfour_interp = ", Nfour_interp)

  Replica = TDS_Simmetry.GetAllReplicated (  calculatedDatas, simmetries_dict,   filename= filename,
                                             md5postfix=md5postfix,
                                             overwrite= False ,
                                             key = "Codes_"+(str(APPLYTIMEREVERSAL)) ,
                                             APPLYTIMEREVERSAL=APPLYTIMEREVERSAL,
                                             CALCULATEFOURIER =1 ,
                                             CALCULATECOMPLEMENT=0,
                                             Nfour_interp=Nfour_interp,
                                             MAKING=0,
                                             MAKINGFOURIER=0
                                       )
  #################################################################################3
  ## 
  ##        
  ##

  BV=calculatedDatas.GetBrillVects()


  if myrank==0: print( " GOING TO IFFT out DMs ........ ")

  stime=time.time()
  DMs_nnn =  TDS_Simmetry.GetDM_fromFFT(  Replica["Four_Rvects"],   Replica["Four_DMs"],  calculatedDatas, subN1, subN2, subN3   ) 


  Qs_n1 =  numpy.array( numpy.arange(subN1)[:, None]*  BV[0]/subN1, "f")
  Qs_n2 =  numpy.array( numpy.arange(subN2)[:, None]*  BV[1]/subN2, "f")
  Qs_n3 =  numpy.array( numpy.arange(subN3)[:, None]*  BV[2]/subN3, "f")
  Qs_nnn = Qs_n1[:,None,None,: ] + Qs_n2[None,:,None,: ]  + Qs_n3[None,None,:,: ] 



  # print( " SPECIALE " , Qs_nnn[7,3,2])
  # qspeciale =  numpy.array(Qs_nnn[7,3,2])
  # DMspeciale = numpy.array(DMs_nnn[7,3,2])
  Nspeciale = 2+3*subN3 + 7*subN3*subN2


  Nbranches=3*len(calculatedDatas.atomNames)

  # kinematicalFactors      =  numpy.zeros( list(Qs_nnn.shape[:3])+[  Nbranches/3  ] , numpy.complex64)
  # kinematicalFactors.imag = numpy.tensordot( Qs_nnn  ,
  #                                           -calculatedDatas.atomAbsolutePositions  ,  # the minus sign is here
  #                                           [[3], [ 1 ]] ) 
  # kinematicalFactors = numpy.exp( kinematicalFactors )


  # numpy.multiply(  numpy.reshape( DMs_nnn, [subN1, subN2, subN3,  Nbranches , -1, 3 ] )  ,    
  #                  kinematicalFactors.conjugate()[ :,:,:, None, :,None],
  #                  numpy.reshape( DMs_nnn, [ subN1, subN2, subN3 , Nbranches  , -1, 3 ] )
  #                  ) 
  # numpy.multiply(  numpy.reshape( numpy.swapaxes( DMs_nnn , -2,-1 ), [subN1, subN2, subN3,Nbranches   , -1, 3 ] )  ,    
  #                  kinematicalFactors[ :,:,:, None, :,None],
  #                  numpy.reshape( numpy.swapaxes( DMs_nnn , -2,-1 ) , [subN1, subN2, subN3, Nbranches  , -1, 3 ] )
  #                  ) 

  if myrank==0:  print( subN1*subN2*subN3,  " DMs FFT-RETRIEVED in ", time.time()-stime, "  seconds ")

  eigenvectors_nnn = numpy.zeros(DMs_nnn.shape      , numpy.complex64)
  eigenvalues_nnn  = numpy.zeros(DMs_nnn.shape[:-1] , numpy.float32)



  stime=time.time()
  if myrank==0:  print( " EIGEN-SOLVING ...... ")
  count=0
  for eigenvalues,eigenvectors,  dm in zip (
    numpy.reshape (eigenvalues_nnn, [subN1*subN2*subN3,Nbranches ]),
    numpy.reshape (eigenvectors_nnn, [subN1*subN2*subN3,Nbranches,Nbranches ]),
    numpy.reshape (DMs_nnn , [subN1*subN2*subN3,Nbranches,Nbranches ]),
    ):
    count+=1
    if myrank!= count%nprocs:
      continue
    eigenvalues[:] , eigenvectors[:]  = numpy.linalg.eigh(dm)


  if( nprocs>1):

    target = numpy.zeros(  eigenvalues_nnn.shape          , "f")

    comm.Allreduce([numpy.array(eigenvalues_nnn.real), MPI.FLOAT], [ target,MPI.FLOAT]  , MPI.SUM )
    eigenvalues_nnn.real=target

    target = numpy.zeros(  eigenvectors_nnn.shape          , "f")

    comm.Allreduce([numpy.array(eigenvectors_nnn.real), MPI.FLOAT], [ target,MPI.FLOAT]  , MPI.SUM )
    eigenvectors_nnn.real=target
    comm.Allreduce([numpy.array(eigenvectors_nnn.imag), MPI.FLOAT], [ target,MPI.FLOAT]  , MPI.SUM )
    eigenvectors_nnn.imag=target

    target=None



  # eigenvalues_speciale , eigenvectors_speciale  = numpy.linalg.eigh(DMspeciale)
  # eigenvalues_speciale =numpy.sqrt(numpy.maximum([eigenvalues_speciale] , numpy.array([1.0e-6], numpy.float32) ))
  # print( " SPECIALE eigvals " , eigenvalues_speciale[-3:])
  # print( " SPECIALE eigvects[-3:,2] " , eigenvectors_speciale[-3:,2])

  if myrank==0:  print( subN1*subN2*subN3, "   ",  Nbranches , "X",Nbranches , "   DMs eigensolved in    ", time.time()-stime, " seconds ")



  eigenvalues_flatq  = numpy.reshape( eigenvalues_nnn , [ subN1*subN2*subN3 , Nbranches ]   )
  eigenvectors_flatq = numpy.reshape( eigenvectors_nnn, [ subN1*subN2*subN3 , Nbranches , Nbranches]   )

  eigenvalues_flatq  =numpy.sqrt(numpy.maximum(eigenvalues_flatq, numpy.array([1.0e-6], numpy.float32) ))

  if do_histogram:
    MAX_W = numpy.max( eigenvalues_flatq  )
    dwhisto = MAX_W/Nbins
    Nbins=Nbins+1

    namesdict={}
    for aname in  calculatedDatas.atomNames:
      namesdict[aname]=1

    filtri={}
    histograms = {}
    for aname in  namesdict.keys():
      filtri[aname] = [   [ (aname==name)*1.0 ]*3   for name in      calculatedDatas.atomNames       ]
      filtri[aname] =numpy.array( filtri[aname] ).reshape([Nbranches])
      histograms[aname] = numpy.zeros(Nbins,dtype= numpy.float32)



  # print( " eigenvalues_flatq[Nspeciale]  " , eigenvalues_flatq[Nspeciale])
  # print( "  eigenvectors_flatq [Nspeciale ][-3:,2] " , eigenvectors_flatq [Nspeciale ][-3:,2])
  # qspeciale = numpy.array([qspeciale])
  # eigenvectors_speciale= numpy.array([eigenvectors_speciale])

  if not ONLY_HISTO:
    bigresult = numpy.zeros( [(N1[1]-N1[0]) ,subN1,(N2[1]-N2[0]),subN2,(N3[1]-N3[0]),subN3  ]  , numpy.float32)

  h5=h5py.File(filename+"."+md5postfix, "r")
  DWs_3X3 = h5["Codes_"+(str(APPLYTIMEREVERSAL))]["DWs"][str(Temperature)]["DWf_33"][:]
  h5=None

  # cambia temperatura in Hartree
  Temperature_Hartree=Temperature /11604.50520/27.211396132

  # amu = 1822.8897312974866 electrons
  # cm-1 ==> Hartree  cm-1 =  4.5563723521675276e-06 Hartree
  # Bohr = 0.529177249 Angstroems
  factor_forcoth = 4.5563723521675276e-06*1.0/(2.0*Temperature_Hartree)


  exp_plus =  numpy.exp( factor_forcoth *  eigenvalues_flatq)
  exp_minus = numpy.exp(-factor_forcoth *  eigenvalues_flatq)


  if NEUTRONCALC:
    kinstokes = numpy.sqrt(numpy.maximum( (NeutronE-eigenvalues_flatq*(0.0001239852 *1000))/NeutronE, 0.0 ))
    kinantistk= numpy.sqrt((NeutronE+eigenvalues_flatq*(0.0001239852 *1000))/NeutronE)
  else:
    kinstokes = 1.0
    kinantistk= 1.0


  if RemoveBose==0:
    totalstats = (exp_plus*kinstokes+exp_minus*kinantistk)/((exp_plus-exp_minus)* eigenvalues_flatq )
  else:
    totalstats = 1.0/eigenvalues_flatq






  if branchWeight is not None:
    print( " CONSIDERING WEIGTHED BRANCHES AS GIVEN BY  branchWeight = ", branchWeight)
    totalstats= totalstats *numpy.array(branchWeight) 
  else:
    print( "no branchWeight provided, considering all branches \n"*10)


  if energyWindow is not None:
    print( " CONSIDERING energyWindow(meV) = ", energyWindow)
    totalstats= totalstats *numpy.less(eigenvalues_flatq  * 12398.5253/(10.0**8) *1000.0   , energyWindow[1])
    totalstats= totalstats *numpy.less(energyWindow[0]        ,eigenvalues_flatq * 12398.5253/(10.0**8) *1000.0) 
  else:
    print( "no energyWindow provided, considering all remaining branches \n"*10)







  # exp_plus_speciale =  numpy.exp( factor_forcoth *  eigenvalues_speciale)
  # exp_minus_speciale = numpy.exp(-factor_forcoth *  eigenvalues_speciale)
  # totalstats_speciale = (exp_plus_speciale+exp_minus_speciale)/((exp_plus_speciale-exp_minus_speciale)* eigenvalues_speciale )
  # print( " totalstats_speciale  ", totalstats_speciale)
  # print( " totalstats[Nspeciale]  ", totalstats[Nspeciale])



  count=0
  procwithedges=0

  for ishift1 in range(N1[0],N1[1]):
    for ishift2 in range(N2[0],N2[1]):
      for ishift3 in range(N3[0],N3[1]):

        count+=1
        if do_histogram and  ( not DOQLIMITS)  and  ( ishift1==0 and ishift2==0 and ishift2==0):
          procwithedges= ( count % nprocs  )  #   besoin de envoyer edges ???

        if( count % nprocs != myrank):
          continue


        Qshift = ishift1*BV[0] +  ishift2*BV[1] + ishift3*BV[2]
        Qshift =numpy.array( Qshift , numpy.float32 )


        Qs_nnn_shifted = Qs_nnn+Qshift
        Qs_nnn_shifted_flatq = numpy.reshape(Qs_nnn_shifted,  [     subN1*subN2*subN3, 3  ]   )




        if NEUTRONCALC:
          scatterers = numpy.array([  CohB[aname] for aname in  calculatedDatas.atomNames ] , "F" )   
        else:
          scattFactors_site_q =numpy.array( [ scatterer.f0Lambda(numpy.array([Lambda]) ,
                                                                 Theta(Qs_nnn_shifted_flatq, Lambda  )   )
                                              for scatterer in scatterers ]  ).astype(numpy.float32)
          scattFactors_site_q = numpy.reshape(scattFactors_site_q,  [   len(scatterers),   subN1*subN2*subN3  ]     )
          scattFactors_q_site= scattFactors_site_q.T


        # scattFactors_site_q_speciale =numpy.array( [ scatterer.f0Lambda(numpy.array([Lambda]) ,
        #                                                        Theta(qspeciale+Qshift, Lambda  )   )
        #                                     for scatterer in scatterers ]  ).astype(numpy.float32)
        # scattFactors_site_q_speciale = numpy.reshape(scattFactors_site_q_speciale,  [   len(scatterers),  1  ]     )
        # scattFactors_q_site_speciale= scattFactors_site_q_speciale.T
        # print( " scattFactors_q_site_speciale=  " , scattFactors_q_site_speciale)
        # print( " scattFactors_q_site[Nspeciale] " ,scattFactors_q_site[Nspeciale])

        kinematicalFactors      =  numpy.zeros( [   len(Qs_nnn_shifted_flatq)  , Nbranches//3 ]  , numpy.complex64)
        kinematicalFactors.imag = numpy.tensordot( Qs_nnn_shifted_flatq  ,
                                                     -calculatedDatas.atomAbsolutePositions  ,  # the minus sign is here
                                                     [[1], [ 1 ]] ) 
        kinematicalFactors = numpy.exp( kinematicalFactors )


        # kinematicalFactors_speciale      =  numpy.zeros(scattFactors_q_site_speciale.shape , numpy.complex64)
        # kinematicalFactors_speciale.imag = numpy.tensordot( qspeciale +Qshift ,
        #                                              -calculatedDatas.atomAbsolutePositions  ,  # the minus sign is here
        #                                              [[1], [ 1 ]] ) 
        # kinematicalFactors_speciale = numpy.exp( kinematicalFactors_speciale )
        # print( " kinematicalFactors_speciale  " , kinematicalFactors_speciale)
        # print( " kinematicalFactors[Nspeciale]  " , kinematicalFactors[Nspeciale])



        massfactors  = numpy.sqrt(1.0/ calculatedDatas.atomMasses )

        dwfacts_perq = numpy.tensordot(Qs_nnn_shifted_flatq ,DWs_3X3    ,  axes=[[1], [ 1]])
        dwfacts_perq  = dwfacts_perq  *Qs_nnn_shifted_flatq[:,None,:]  # il None va sui siti che vengono dal tensordot
        dwfacts_perq  = numpy.exp(-numpy.sum(dwfacts_perq , axis=2 ))


        # dwfacts_perq_speciale = numpy.tensordot(qspeciale+Qshift,DWs_3X3    ,  axes=[[1], [ 1]])
        # dwfacts_perq_speciale  = dwfacts_perq_speciale  *(qspeciale+Qshift)[:,None,:]  # il None va sui siti che vengono dal tensordot
        # dwfacts_perq_speciale  = numpy.exp(-numpy.sum(dwfacts_perq_speciale , axis=2 ))
        # print( "dwfacts_perq[Nspeciale] " , dwfacts_perq[Nspeciale])
        # print(( "dwfacts_perq_speciale " , dwfacts_perq_speciale))




        if NEUTRONCALC:
          dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*  scatterers  
        else:
          dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*scattFactors_q_site

        # dwfacts_mass_kin_scatt_perq_speciale  = dwfacts_perq_speciale*massfactors*kinematicalFactors_speciale*scattFactors_q_site_speciale
        # print( "dwfacts_mass_kin_scatt_perq[Nspeciale] " ,dwfacts_mass_kin_scatt_perq [Nspeciale])
        # print( "dwfacts_mass_kin_scatt_perq_speciale  " , dwfacts_mass_kin_scatt_perq_speciale )


        dum=numpy.multiply(Qs_nnn_shifted_flatq.reshape([ subN1*subN2*subN3,1,3, 1]),
                           eigenvectors_flatq.reshape([subN1*subN2*subN3 , -1,3, Nbranches]) )
        dum=numpy.sum(dum, axis=2)
        Fjs = numpy.sum(dwfacts_mass_kin_scatt_perq.reshape([subN1*subN2*subN3 ,Nbranches//3,1])
                        * dum, axis=-2)  # sommato sugli ioni     
        intensity_array = numpy.sum(( Fjs*(Fjs.conjugate())).real  *totalstats, axis=-1)



        if DOQLIMITS or ( ishift1==0 and ishift2==0 and ishift2==0):


          if DOQLIMITS:

            if not xvdos:
              raise Exception( " DOQLIMITS has not much meaning without xvdos ")

            moduli = numpy.sqrt(numpy.sum(Qs_nnn_shifted_flatq*Qs_nnn_shifted_flatq, -1))
            QMASK =  ( numpy.less(QMIN, moduli  ) * numpy.less(moduli, QMAX) )


          if do_histogram:
            contribs = {}
            for name in filtri.keys():
              dum =  numpy.swapaxes(
                numpy.multiply( filtri[name],
                                numpy.swapaxes(eigenvectors_flatq.reshape([subN1*subN2*subN3 , Nbranches, Nbranches]), 1,2)),
                1,2
                )
              if( xvdos):
                dum=numpy.multiply(Qs_nnn_shifted_flatq.reshape([ subN1*subN2*subN3,1,3, 1]),
                                   dum.reshape([subN1*subN2*subN3 , -1,3, Nbranches]) )
                dum=numpy.sum(dum, axis=2)
                Fjs = numpy.sum(dwfacts_mass_kin_scatt_perq.reshape([subN1*subN2*subN3 ,Nbranches//3,1])
                                * dum, axis=-2)  # sommato sugli ioni     
                dum = ( Fjs*(Fjs.conjugate())).real  # *totalstats
                # dum = numpy.sum(( Fjs*(Fjs.conjugate())).real  *totalstats) , axis=-1)

                if DOQLIMITS:
                  dum= dum.reshape(   [subN1*subN2*subN3 , Nbranches]  ) * QMASK.reshape([subN1*subN2*subN3,1])
              else:
                dum= numpy.sum((dum *(dum.conjugate())).real, axis=-2)


              print( " dum.shape" )
              print( dum.shape)
              print( " reshapes to " , [subN1*subN2*subN3, Nbranches])

              newhisto , edges= numpy.histogram(eigenvalues_flatq. reshape([subN1*subN2*subN3*Nbranches])  ,
                                                bins=Nbins ,
                                                range=( 0, Nbins*dwhisto  ),
                                                normed=False,
                                                weights=dum .reshape([subN1*subN2*subN3*Nbranches]) )
                                                ## density=None)


              histograms[name][:]=histograms[name]+newhisto





        # dum=numpy.multiply(((qspeciale+Qshift)).reshape([ 1,1,3, 1]),
        #                    eigenvectors_speciale.reshape([1 , -1,3, Nbranches]) )
        # dum=numpy.sum(dum, axis=2)
        # Fjs_speciale = numpy.sum(dwfacts_mass_kin_scatt_perq_speciale.reshape([1 ,Nbranches/3,1])
        #                 * dum, axis=-2)  # sommato sugli ioni  
        # print( Fjs_speciale.shape)
        # intensity_array_speciale = numpy.sum(( Fjs_speciale*(Fjs_speciale.conjugate())).real  *totalstats_speciale, axis=-1)

        # print( "intensity_array_speciale " , intensity_array_speciale)
        # print( " intensity_array  " , intensity_array[Nspeciale])
        # print( " qspeciale_shifted " , Qs_nnn_shifted_flatq[Nspeciale])
        # raise( " OK " )

        # if myrank==0:
        print(  ishift1, ishift2, ishift3)
        # print( numpy.min(intensity_array))
        if not ONLY_HISTO:
          bigresult[ishift1-N1[0], :, ishift2-N2[0], :, ishift3-N3[0]  , :  ] =  numpy.reshape( intensity_array, [subN1,subN2,subN3] )



  print( " procwithedges" ,  procwithedges)
  if procwithedges!=0:
    if myrank==0:
      print( " WAITING " )
      edges=comm.recv( obj=None, source=procwithedges, tag=16273)
      print( " WAITING OK " )
    elif myrank==procwithedges :
      edges=comm.send(obj=edges,dest=0,tag=16273)


  if( nprocs>1):

    if not ONLY_HISTO:
      if myrank==0:
        comm.Reduce( MPI.IN_PLACE   , [  bigresult , MPI.FLOAT  ], op=MPI.SUM, root=0)
      else:
        comm.Reduce([  bigresult , MPI.FLOAT  ], None, op=MPI.SUM, root=0)

    if do_histogram:
      for name in histograms.keys():
        if myrank==0:
          comm.Reduce( MPI.IN_PLACE   , [  histograms[name] , MPI.FLOAT  ], op=MPI.SUM, root=0)
        else:
          comm.Reduce([ histograms[name]  , MPI.FLOAT  ], None, op=MPI.SUM, root=0)

  if myrank==0:

    if not ONLY_HISTO:
      bigresult.shape = [ (N1[1]-N1[0])*subN1,(N2[1]-N2[0] )*subN2,(N3[1]-N3[0])*subN3 ]


    if do_histogram:

      h5=h5py.File("histogram.h5", "w")
      key="histograms"
      if(key in h5):
        del h5[key]
      h5 .create_group(key)
      h5[key+"/X"]  =  (edges[:-1]+edges[1:])*0.5
      for name in histograms.keys():
        h5[key+"/"+name]  =  histograms[name]
      h5=None


    if not ONLY_HISTO:
      if outputname is not None:
        h5=h5py.File("%s_%s_.h5" %( outputname,  Temperature) , "w")
      else:
        h5=h5py.File("VolumeIntensity_%s_.h5" %Temperature , "w")

      h5["bigresult"] = bigresult
      h5["subN1"    ]=subN1
      h5["subN2"    ]=subN2    
      h5["subN3"    ]=subN3    
      h5["N1start"       ]=N1[0]     
      h5["N2start"       ]=N2[0]       
      h5["N3start"       ]=N3[0]
      h5["N1end"       ]=N1[1]     
      h5["N2end"       ]=N2[1]       
      h5["N3end"       ]=N3[1]
      h5["BV"]       =BV 
      h5=None

      print( " NOW WRITING VOLUME ALSO TO A CCP4 FILE ")
      from . import ccp4_writer
      if outputname is not None:
        fname ="%s_%s_.ccp4" %( outputname,  Temperature) 
      else:
        fname ="VolumeIntensity_%s_.ccp4" %Temperature 
      ccp4_writer.write_ccp4_grid_data(bigresult , fname)

  if( nprocs>1):
    comm.Barrier()
