
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/



from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


""" The script make_TDS_intensityVolumeOnlyHisto calculates partial and x-ray or neutron weighted phonon densities of states.

    * it requires that you have previously done the following operations :

      * symmetrisation
      * Fourier transform
      * Debye waller calculation for the temperature of choice
    
    * The usage is ::

        make_TDS_IntensityVolumeOnlyHisto castep_filename input_filename  

      the file *castep_filename* can be either the name of the original castep output or the associated hdf5 file.In any case the associated hdf5 file must exist already (make_TDS_Simmetrization
      must be runned beforehand)

    * The input_file must set the variables :

      * Mandatories

        * APPLYTIMEREVERSAL
        * subN1
        * subN2
        * subN3
        * N1 
        * N2
        * N3
        * Nfour_interp
        * Temperature  


      * If NEUTRONCALC==0 ( default )

        * Lambda
      * If NEUTRONCALC==1

        * CohB
        * NeutronE

      * Nbins
      * MAX_W
      * xvdos
      * DOQLIMITS

        * QMIN=0
        * QMAX=0

      * The following block of three parameters are either all None (default), or outputname must be set together with 
        one of branchWeight and energyWindow

           * outputname
           * branchWeight
           * energyWindow

      * set RemoveBose=0  if you want to remove Bose statistics factors

The input variables are documented with docstrings below
  
""" 

from . import startmessage

from . import TDS_Simmetry      
from . import TDS_Reading
from . import TDS_Interpolation
import h5py


import numpy

import sys
import time
import copy
import string
from . import dabax  
import math

if(sys.argv[0][-12:]!="sphinx-build"):
  s=open(sys.argv[2], "r").read()
  exec(s)


def check_input(): 
  assert("Temperature"  in dir())
  assert("Nfour_interp"  in dir())
  assert("subN1"  in dir())
  assert("subN2"  in dir())
  assert("subN3"  in dir())
  assert("N1"  in dir())
  assert("N2"  in dir())
  assert("N3"  in dir())
  assert("MAX_W"  in dir())
  assert("APPLYTIMEREVERSAL"  in dir())
 
  if "NEUTRONCALC" in dir() and NEUTRONCALC:
      assert( "CohB" in dir())
      assert( "NeutronE" in dir())
  else:
      NEUTRONCALC=0
      assert("Lambda"  in dir())
  assert("Nbins"  in dir())
  assert("MAX_W" in dir())
  assert("xvdos"  in dir())
  assert("DOQLIMITS" in dir())
  if (DOQLIMITS==1 ):
    print( " ACTIVATED QLIMITATIONS ")
    print( " between QMIN = " , QMIN)
    print( " and QMAX = "     , QMAX)

  if "branchWeight" in dir() or "energyWindow" in dir():
    assert("outputname" in dir())
  if "outputname" in dir():
    assert("branchWeight" in dir()  or  "energyWindow" in dir()  )

if(sys.argv[0][-12:]!="sphinx-build"):
  check_input()

APPLYTIMEREVERSAL=1
"""
  this is one by default. Must be coherent with previous steps
"""
subN1=20
""" Subdivision, in the elementary brillouin zone, along the Z dimension of the reconstructed volume, 
    which is parallel to the first axis of the brilloin zone.
"""
subN2=20
""" Subdivision, in the elementary brillouin zone, along the Y dimension of the reconstructed volume, 
    which is parallel to the second axis of the brilloin zone.
"""
subN3=20
""" Subdivision, in the elementary brillouin zone, along the X dimension of the reconstructed volume, 
    which is parallel to the third axis of the brilloin zone.
"""
N1 =  4
""" Besides being subdivised, the Brillouin zone, is replicated alonx the first axis   for -N1 up to N1-1 included. In reciprocal cell vector units. N1 can be either an integer or a pair of integer. In the latter case the first element is the starting shift,
         the second is the ending shift+1.
"""
N2 =  4
"""
  same thing as N1 but along the second reciprocal cell vector
"""
N3 =  4
"""
  same thing as N1 but along the third reciprocal  cell vector
"""

Nfour_interp=5
"""  The number of points in each direction of the 3D reciprocal grid.
     The hdf5 file must contain a previous pretreatement done with the same parameter.
"""
Temperature=100
""" 
The hdf5 file must include DW factors calculated ALSO at this temperature. The temperature at which DW factors have been calculated. The calculation of partial DOS doesn not involve DW factors but they must be present for the script to run.   Units are Kelvin
"""
Lambda=0.2
""" For X-ray scattering : the wavelenght in Angstroems.
"""


NEUTRONCALC=0
""" This activate neutron scattering calculations.
"""
CohB=None
""" When neutroncalc is on, this must be a dictionary : for each atom name the Coherent lenght.
"""
NeutronE=0
""" When neutroncalc is on, the neutron energy in meV
"""

Nbins=0
"""
  Numer of bins in the histogram
"""

MAX_W=0
"""
  The histogram spans the range [0, MAX_W]. Units are cm-1.
"""

xvdos=0
"""
 when this option is on, the DOS is calculated with a weight given by scattering factors, structure factor, DW etc. etc.
  almost the same as the one used for scattering but without bose statistics.
"""

DOQLIMITS=0
""" If on : when calculating xvdos only contributions from q points having modulus inside a given window are considered.
"""

QMIN=0
"""  the minimum Q modulus for the DOQLIMITS=1 window
"""
QMAX=0
""" the maximum Q modulus for the DOQLIMITS=1 window
"""

outputname= None
""" a name to design the output file with data from calculation considering branchWeight or energyWindow
"""
branchWeight=None
""" a list of weights : one per branch.
"""
energyWindow=None
"""  a list of two numbers : minimum and maximum in meV
"""

RemoveBose=0
""" RemoveBose=0 desactivates the Bose statistics.
"""


try:
    from mpi4py import MPI
    myrank = MPI.COMM_WORLD.Get_rank()
    nprocs = MPI.COMM_WORLD.Get_size()
    procnm = MPI.Get_processor_name()
    comm = MPI.COMM_WORLD
    print( "MPI LOADED , nprocs = ", nprocs)
except:
    nprocs=1
    myrank = 0
    print( "MPI NOT LOADED ")


def main():
  

  if type(N1)==type(1):
    N1=[-N1,N1]
  if type(N2)==type(2):
    N2=[-N2,N2]
  if type(N3)==type(3):
    N3=[-N3,N3]

  if  NEUTRONCALC:
      print( " Doing Neutron calculation ")
      assert( "CohB" in dir())
      assert( "NeutronE" in dir())
  else:
      NEUTRONCALC=0
      assert("Lambda"  in dir())

  if RemoveBose==1:
    print( " Intensity are calculated WITHOUT  Bose factor")
  else:
    print( " Intensity are calculated WITH  Bose factor")

  ########################################################
  # 
  # Q=2*sin(theta)*2*Pi/lambda
  def Theta(Q, Lambda  ):
    return numpy.arcsin(  numpy.sqrt( numpy.sum(Q*Q, axis=-1)) * Lambda/2.0/math.pi/2.0 )
  #########################################################################

  calculatedDatas = TDS_Reading.CalcDatas.get_CalcDatas_Object_From_File(sys.argv[1])

  tf0 = dabax.Dabax_f0_Table("f0_WaasKirf.dat")
  scatterers = [  tf0.Element(aname  ) for aname in  calculatedDatas.atomNames ] 


  md5postfix= calculatedDatas.Get_md5postfix()
  filename = calculatedDatas.Get_filename()

  cella=TDS_Simmetry.OP_cella(cellvectors=calculatedDatas.cellVects ,
                 AtomNames_long=calculatedDatas.atomNames,
                 PositionsList_flat=calculatedDatas.atomAbsolutePositions )

  simmetries_dict = TDS_Simmetry.FindSimmetries(cella,   filename= filename,
                                                md5postfix=md5postfix, overwrite= False,
                                                key = "simmetries_dict" )


  if myrank==0: print( "GOING TO RETRIEVE FOURIER TRASFORM  Nfour_interp = ", Nfour_interp)

  Replica = TDS_Simmetry.GetAllReplicated (  calculatedDatas, simmetries_dict,   filename= filename,
                                             md5postfix=md5postfix,
                                             overwrite= False ,
                                             key = "Codes_"+(str(APPLYTIMEREVERSAL)) ,
                                             APPLYTIMEREVERSAL=APPLYTIMEREVERSAL,
                                             CALCULATEFOURIER =1 ,
                                             CALCULATECOMPLEMENT=0,
                                             Nfour_interp=Nfour_interp,
                                             MAKING=0,
                                             MAKINGFOURIER=0
                                       )

  Nbranches=3*len(calculatedDatas.atomNames)

  dwhisto = MAX_W*1.0/Nbins
  Nbins=Nbins+1

  namesdict={}
  for aname in  calculatedDatas.atomNames:
    namesdict[aname]=1

  filtri={}
  histograms = {}
  for aname in  namesdict.keys():
    filtri[aname] = [   [ (aname==name)*1.0 ]*3   for name in      calculatedDatas.atomNames       ]
    filtri[aname] =numpy.array( filtri[aname] ).reshape([Nbranches])
    histograms[aname] = numpy.zeros(Nbins,dtype= numpy.float32)

  #################################################################################3
  ## 
  ##        
  ##

  BV=calculatedDatas.GetBrillVects()

  if myrank==0: print( " GOING TO IFFT out DMs ........ ")


  stime=time.time()

  for iN3 in range(subN3):
    if myrank!= iN3%nprocs:
      continue

    DMs_nnn =  TDS_Simmetry.GetDM_fromFFT_shifted(  Replica["Four_Rvects"],   Replica["Four_DMs"],  calculatedDatas, subN1, subN2, iN3,  subN3   )  

    Qs_n1 =  numpy.array( numpy.arange(subN1)[:, None]*  BV[0]/subN1, "f")
    Qs_n2 =  numpy.array( numpy.arange(subN2)[:, None]*  BV[1]/subN2, "f")
    Qs_n3 =  numpy.array( numpy.arange(iN3,iN3+1)[:, None]*  BV[2]/subN3, "f") 
    Qs_nnn = Qs_n1[:,None,None,: ] + Qs_n2[None,:,None,: ]  + Qs_n3[None,None,:,: ] 

    if myrank==0:  print( subN1*subN2*1,  " DMs FFT-RETRIEVED in ", time.time()-stime, "  seconds ")

    eigenvectors_nnn = numpy.zeros(DMs_nnn.shape      , numpy.complex64)
    eigenvalues_nnn  = numpy.zeros(DMs_nnn.shape[:-1] , numpy.float32)

    stime=time.time()
    if myrank==0:  print( " EIGEN-SOLVING ...... ")
    count=0
    for eigenvalues,eigenvectors,  dm in zip (
      numpy.reshape (eigenvalues_nnn, [subN1*subN2*1,Nbranches ]),
      numpy.reshape (eigenvectors_nnn, [subN1*subN2*1,Nbranches,Nbranches ]),
      numpy.reshape (DMs_nnn , [subN1*subN2*1,Nbranches,Nbranches ]),
      ):
      eigenvalues[:] , eigenvectors[:]  = numpy.linalg.eigh(dm)

    if myrank==0:  print( subN1*subN2*1, "   ",  Nbranches , "X",Nbranches , "   DMs eigensolved in    ", time.time()-stime, " seconds ")

    eigenvalues_flatq  = numpy.reshape( eigenvalues_nnn , [ subN1*subN2*1 , Nbranches ]   )
    eigenvectors_flatq = numpy.reshape( eigenvectors_nnn, [ subN1*subN2*1 , Nbranches , Nbranches]   )

    eigenvalues_flatq  =numpy.sqrt(numpy.maximum(eigenvalues_flatq, numpy.array([1.0e-6], numpy.float32) ))


    h5=h5py.File(filename+"."+md5postfix, "r")
    DWs_3X3 = h5["Codes_"+(str(APPLYTIMEREVERSAL))]["DWs"][str(Temperature)]["DWf_33"][:]
    h5=None

    # cambia temperatura in Hartree
    Temperature_Hartree=Temperature /11604.50520/27.211396132

    # amu = 1822.8897312974866 electrons
    # cm-1 ==> Hartree  cm-1 =  4.5563723521675276e-06 Hartree
    # Bohr = 0.529177249 Angstroems
    factor_forcoth = 4.5563723521675276e-06*1.0/(2.0*Temperature_Hartree)

    exp_plus =  numpy.exp( factor_forcoth *  eigenvalues_flatq)
    exp_minus = numpy.exp(-factor_forcoth *  eigenvalues_flatq)

    if NEUTRONCALC:
      kinstokes = numpy.sqrt(numpy.maximum( (NeutronE-eigenvalues_flatq*(0.0001239852 *1000))/NeutronE, 0.0 ))
      kinantistk= numpy.sqrt((NeutronE+eigenvalues_flatq*(0.0001239852 *1000))/NeutronE)
    else:
      kinstokes = 1.0
      kinantistk= 1.0

    if RemoveBose==0:
      totalstats = (exp_plus*kinstokes+exp_minus*kinantistk)/((exp_plus-exp_minus)* eigenvalues_flatq )
    else:
      totalstats = 1.0/eigenvalues_flatq


    if branchWeight is not None:
      print( " CONSIDERING WEIGTHED BRANCHES AS GIVEN BY  branchWeight = ", branchWeight)
      totalstats= totalstats *numpy.array(branchWeight) 
    else:
      print( "no branchWeight provided, considering all branches \n"*10)


    if  energyWindow  is not None :
      print( " CONSIDERING energyWindow(meV) = ", energyWindow)
      totalstats= totalstats *numpy.less(eigenvalues_flatq  * 12398.5253/(10.0**8) *1000.0   , energyWindow[1])
      totalstats= totalstats *numpy.less(energyWindow[0]        ,eigenvalues_flatq * 12398.5253/(10.0**8) *1000.0) 
    else:
      print( "no energyWindow provided, considering all remaining branches \n"*10)

    count=0
    procwithedges=0

    for ishift1 in range(N1[0],N1[1]):
      for ishift2 in range(N2[0],N2[1]):
        for ishift3 in range(N3[0],N3[1]):

          count+=1
          if   ( not DOQLIMITS)  and  ( ishift1==0 and ishift2==0 and ishift2==0):
            procwithedges= ( count % nprocs  )  #   besoin de envoyer edges ???

          if( count % nprocs != myrank):
            continue

          Qshift = ishift1*BV[0] +  ishift2*BV[1] + ishift3*BV[2]
          Qshift =numpy.array( Qshift , numpy.float32 )

          Qs_nnn_shifted = Qs_nnn+Qshift
          Qs_nnn_shifted_flatq = numpy.reshape(Qs_nnn_shifted,  [     subN1*subN2*1, 3  ]   )

          if NEUTRONCALC:
            scatterers = numpy.array([  CohB[aname] for aname in  calculatedDatas.atomNames ] , "F" )   
          else:
            scattFactors_site_q =numpy.array( [ scatterer.f0Lambda(numpy.array([Lambda]) ,
                                                                   Theta(Qs_nnn_shifted_flatq, Lambda  )   )
                                                for scatterer in scatterers ]  ).astype(numpy.float32)
            scattFactors_site_q = numpy.reshape(scattFactors_site_q,  [   len(scatterers),   subN1*subN2*1  ]     )
            scattFactors_q_site= scattFactors_site_q.T

          kinematicalFactors      =  numpy.zeros( [   len(Qs_nnn_shifted_flatq)  , Nbranches//3 ]  , numpy.complex64)
          kinematicalFactors.imag = numpy.tensordot( Qs_nnn_shifted_flatq  ,
                                                       -calculatedDatas.atomAbsolutePositions  ,  # the minus sign is here
                                                       [[1], [ 1 ]] ) 
          kinematicalFactors = numpy.exp( kinematicalFactors )

          massfactors  = numpy.sqrt(1.0/ calculatedDatas.atomMasses )

          dwfacts_perq = numpy.tensordot(Qs_nnn_shifted_flatq ,DWs_3X3    ,  axes=[[1], [ 1]])
          dwfacts_perq  = dwfacts_perq  *Qs_nnn_shifted_flatq[:,None,:]  # il None va sui siti che vengono dal tensordot
          dwfacts_perq  = numpy.exp(-numpy.sum(dwfacts_perq , axis=2 ))

          if NEUTRONCALC:
            dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*  scatterers  
          else:
            dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*scattFactors_q_site

          dum=numpy.multiply(Qs_nnn_shifted_flatq.reshape([ subN1*subN2*1,1,3, 1]),
                             eigenvectors_flatq.reshape([subN1*subN2*1 , -1,3, Nbranches]) )
          dum=numpy.sum(dum, axis=2)
          Fjs = numpy.sum(dwfacts_mass_kin_scatt_perq.reshape([subN1*subN2*1 ,Nbranches//3,1])
                          * dum, axis=-2)  # sommato sugli ioni     
          intensity_array = numpy.sum(( Fjs*(Fjs.conjugate())).real  *totalstats, axis=-1)

          if DOQLIMITS or ( ishift1==0 and ishift2==0 and ishift2==0):

            if DOQLIMITS:

              if not xvdos:
                raise Exception( " DOQLIMITS has not much meaning without xvdos ")

              moduli = numpy.sqrt(numpy.sum(Qs_nnn_shifted_flatq*Qs_nnn_shifted_flatq, -1))
              QMASK =  ( numpy.less(QMIN, moduli  ) * numpy.less(moduli, QMAX) )

            contribs = {}
            for name in filtri.keys():
              dum =  numpy.swapaxes(
                numpy.multiply( filtri[name],
                                numpy.swapaxes(eigenvectors_flatq.reshape([subN1*subN2*1 , Nbranches, Nbranches]), 1,2)),
                1,2
                )
              if( xvdos):
                dum=numpy.multiply(Qs_nnn_shifted_flatq.reshape([ subN1*subN2*1,1,3, 1]),
                                   dum.reshape([subN1*subN2*1 , -1,3, Nbranches]) )
                dum=numpy.sum(dum, axis=2)
                Fjs = numpy.sum(dwfacts_mass_kin_scatt_perq.reshape([subN1*subN2*1 ,Nbranches//3,1])
                                * dum, axis=-2)  # sommato sugli ioni     
                dum = ( Fjs*(Fjs.conjugate())).real  # *totalstats
                # dum = numpy.sum(( Fjs*(Fjs.conjugate())).real  *totalstats) , axis=-1)

                if DOQLIMITS:
                  dum= dum.reshape(   [subN1*subN2*1 , Nbranches]  ) * QMASK.reshape([subN1*subN2*1,1])
              else:
                dum= numpy.sum((dum *(dum.conjugate())).real, axis=-2)

              print( " dum.shape" )
              print( dum.shape)
              print( " reshapes to " , [subN1*subN2*1, Nbranches])

              newhisto , edges= numpy.histogram(eigenvalues_flatq. reshape([subN1*subN2*1*Nbranches])  ,
                                                bins=Nbins ,
                                                range=( 0, Nbins*dwhisto  ),
                                                normed=False,
                                                weights=dum .reshape([subN1*subN2*1*Nbranches]) )
                                                ## density=None)

              histograms[name][:]=histograms[name]+newhisto


  if( nprocs>1):   
      for name in histograms.keys():
        if myrank==0:
          comm.Reduce( MPI.IN_PLACE   , [  histograms[name] , MPI.FLOAT  ], op=MPI.SUM, root=0)
        else:
          comm.Reduce([ histograms[name]  , MPI.FLOAT  ], None, op=MPI.SUM, root=0)

  if myrank==0:
      bin_pos = dwhisto/2 + numpy.arange(Nbins)*dwhisto
      h5=h5py.File("histogram.h5", "w")
      key="histograms"
      if(key in h5):
        del h5[key]
      h5 .create_group(key)
      h5[key+"/X"]  = bin_pos
      for name in histograms.keys():
        h5[key+"/"+name]  =  histograms[name]
      h5=None


  if( nprocs>1):
    comm.Barrier()
