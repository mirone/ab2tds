#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/
""" The script  make_TDS_intensityPlaneClone calculates x-ray and neutron TDS intensities using only the eigenvectors and eigenvalues provided py the input file. The following steps must have been performed beforehand:

      * Symmetrysation
      * Fourier Interpolation
      * Debye-Waller factors for the temperature of choice
    
    * The usage is ::

            make_TDS_tds2elHarvest_Clone QW_castep_filename input_filename castep_filename 

      the file *castep_filename* can be either the name of the original castep output
      our the associated hdf5 file. In any case the associated hdf5 file must exist already.The file *QW_castep_filename*  must contain eigenvectors and eigenvalues for the q-values of interest. A list of q-values can be created by the script make_TDS_IntensityPlane, eigenvectors and eigenvalues on these q-points must be calculated by an external program (e.g. CASTEP)

    * The input_file must set the variables:

      * Mandatories

        *  APPLYTIMEREVERSAL
        *  Temperature

        *  bottom_meV

      * If NEUTRONCALC==0 ( default )

        * Lambda

      * If NEUTRONCALC==1

        * CohB
        * NeutronE

      * Cloud

        * DeltaQ_max = 0.07
        * DeltaQ_min = 0.01
        * fitSpots = [[-2,-1,-2],[-1,0,4],[0,1,-8] ,[2,0,-2], [-2,-1,-2],  ]

      * Cell transformation

        * Ctransf       = [ [1,0,0],[0,1,0],[0,0,1]  ]


The input variables are documented with docstrings below
  
""" 
NOREPLICA=True
DOSECONDORDER = True

from . import startmessage

from . import TDS_Simmetry      
from . import TDS_Reading
from . import TDS_Interpolation
import h5py
import os
import matplotlib.cm as cm

import numpy

import sys
import time
import copy
# import octree
import string
from . import  dabax
import math

try:
    from mpi4py import MPI
    myrank = MPI.COMM_WORLD.Get_rank()
    nprocs = MPI.COMM_WORLD.Get_size()
    procnm = MPI.Get_processor_name()
    comm = MPI.COMM_WORLD
    print("MPI LOADED , nprocs = ", nprocs)
except:
    nprocs=1
    myrank = 0
    print("MPI NOT LOADED ")

DEBUGCLOUDS = 0
DEBUGTESTLUT = 0
DEBUGVECTNORM = 0

class LUT:
    def __init__(self, qs_reds_all, qs_all, ws_all,  freqs_all, vects_all ):
        self.luts_spec = []
        self.Qs_reds_joined=[]
        for  qs_reds, qs, ws, freqs, vects in zip(qs_reds_all,qs_all,  ws_all, freqs_all,vects_all):

             print(" CREO PER ")
             print(qs_reds)
            
             self.luts_spec.append( LUT_special(  qs_reds, qs, ws, freqs, vects  ) )
             if len(self.luts_spec)==1:
                 self.Qs_reds_joined.extend ( list(qs_reds)    )
                 
                 if DEBUGCLOUDS : numpy.savetxt( "debug1.txt",qs_reds   )
                 
             else:
                 if DEBUGCLOUDS :file = open("debug"+str(len(self.luts_spec)) +".txt","w")
                 oldlut = self.luts_spec[-2]
                 print(oldlut.origin)
                 print(oldlut.ends)
                 for q in qs_reds:
                     if not oldlut.is_inside(q):
                         
                         if DEBUGCLOUDS :  file.write("%e %e %e\n"% tuple(q.tolist()))
                         
                         self.Qs_reds_joined.append ( q  )
                         
                 if DEBUGCLOUDS : file.close()
                         
    def getFV(self,q):
        for lut in self.luts_spec:
            if lut.is_inside(q):
                return lut.getFV(q)
        return None



class LUT_special:
    def is_inside(self,qred):
        piupiccoli= numpy.less(  qred                   , self.origin-self.steps*0.49  )
        piugrandi = numpy.less (  self.ends + self.steps*0.49 , qred                    )
        isinside =  (piupiccoli.sum()==0 and piugrandi.sum()==0)
        if not isinside :
            return 0
        
        w   =   ( qred-self.origin  )/self.steps
        iw  =   numpy.round(w).astype("i")

        i  = self.lut[tuple(iw)]
        if i==-1:
            return 0
        return 1


    def __init__(self, qs_reds, qs, ws, freqs, vects ):
        self.qs_reds = qs_reds
        self.qs = qs
        self.ws = ws
        self.freqs = freqs
        self.vects = vects
        
        self.origin  =  qs_reds.min( axis=0 )
        self.ends  =  qs_reds.max( axis=0 )
        self.steps   = []
        for i in range(3):
            xs = numpy.sort(numpy.array(qs_reds[:,i]))
            dists = xs[1:]-xs[:-1]
            dists = dists[dists>1.0e-3]
            self.steps.append( dists.min() )

        self.steps = numpy.array( self.steps ) 
        self.normal_weight = self.steps[0]*self.steps[1]*self.steps[2]
        self.maxws = self.ws.max()



        shape = numpy.array( ( self.ends- self.origin   ) /self.steps ).astype("i")+1

        self.lut = numpy.zeros( shape ,"i" )
        self.lut[:]=-1

        for i, q in enumerate(qs_reds):            
            w   =   ( q-self.origin  )/self.steps
            iw  =   numpy.round(w).astype("i")
            if numpy.abs(w-iw).sum()>0.1:
                raise Exception("LUT is not working properly")
            self.lut[tuple(iw)] = i
            
    def getFV(self,q):
        w   =   ( q-self.origin  )/self.steps

#         if  numpy.less( w,  -1.0e-3  ).sum():
#             return None
#         if  numpy.less( self.ends + 1.0e-3 ,q ).sum():
#             return None

        iw  =   numpy.round(w).astype("i")

#         if numpy.abs(w-iw).sum()  >  1.0e-3  :
#             raise Exception("LUT is not working properly")

        i  = self.lut[tuple(iw)]
        if i==-1:
            raise Exception("Problem")



        w = self.normal_weight*self.ws[i]/self.maxws
        
        return  self.qs_reds[i], self.qs[i], self.freqs[i], self.vects[i], w



def lut_getFV(q,
              lut_origin,
              lut_steps,
              lut_ends,
              lut_lut,
              lut_qs_reds, lut_qs, lut_freqs, lut_vects):
        w   =   ( q-lut_origin  )/lut_steps

        if  numpy.less( w,  -1.0e-3  ).sum():
            return None

        if  numpy.less( lut_ends + 1.0e-3 ,q ).sum():
            return None

        iw  =   numpy.round(w).astype("i")

        if numpy.abs(w-iw).sum()  >  1.0e-3  :
            raise Exception("LUT is not working properly")

        i  = lut_lut[tuple(iw)]
        if i==-1:
            return None

        return  lut_qs_reds[i], lut_qs[i], lut_freqs[i], lut_vects[i]



APPLYTIMEREVERSAL=1
"""
  this is one by default. Must be coherent with previous steps
"""
Temperature=100
""" 
The hdf5 file must include DW factors calculated ALSO at this temperature.
 The temperature at which DW factors have been calculated.
   Units are Kelvin
"""
Lambda=0.2
""" For X-ray scattering: the wavelenght in Angstroems.
"""

NEUTRONCALC=0
""" This activate neutron scattering calculations.
"""
CohB=None
""" When neutroncalc is on, this must be a dictionary: for each atom name the Coherent lenght.
"""

NeutronE=0
""" When neutroncalc is on, the neutron energy in meV
"""

bottom_meV = 0
""" Calculated eigenvalues are clipped to this value before use.
"""

DeltaQ_max = 0.07
""" 
"""
DeltaQ_min = 0.01
"""
"""
fitSpots = [[-2,-1,-2],[-1,0,4],[0,1,-8] ,[2,0,-2], [-2,-1,-2],  ]
"""
"""

Ctransf       = None
"""
    if Ctransf is None, no transformation is done. This is equivalent to do the transformation [ [1,0,0],[0,1,0],[0,0,1]  ].
    The transformation is done on the data, passing to a new unit cell UCp such that ::

         UCp = Ctransf * UC

    As an example ::

      Ctransf = [ [0,1,-1],[1,-1,0],[1,1,1]  ]

    transforms a trigonal UC into an hexagonal one

"""



def  secondorder(qnotshifted,red,   qshifted,  Qs_reds, 
                 lut , massfactors, dwfacts_mass_kin_scatt,factor_forcoth,
                 # lut_origin,
                 # lut_steps,
                 # lut_ends,
                 # lut_lut,
                 # lut_qs_reds, lut_qs, lut_freqs, lut_vects 
             ):
    add=0.0
    for q1_red in Qs_reds:
        q2_red = red-q1_red
        
        if 1:
            q1_data = lut.getFV(q1_red)
            q2_data = lut.getFV(q2_red)
        # else:
        #     q1_data = lut_getFV(q1_red,
        #          lut_origin,
        #          lut_steps,
        #          lut_ends,
        #          lut_lut,
        #          lut_qs_reds, lut_qs, lut_freqs, lut_vects)
        #     q2_data = lut_getFV(q2_red,
        #          lut_origin,
        #          lut_steps,
        #          lut_ends,
        #          lut_lut,
        #          lut_qs_reds, lut_qs, lut_freqs, lut_vects)

        if q1_data is None or q2_data is None:
            continue

        fattore_part = 2.0
        norm1 = numpy.linalg.norm(q1_red)
        norm2 = numpy.linalg.norm(q2_red)
        if norm1>norm2+1.0e-4 :
            continue
        if abs(norm2-norm1)<1.0e-4:
            fattore_part=1
        
        

        
        q1_red_p , q1, evals1, evects1 ,w1  = q1_data
        q2_red_p , q2, evals2, evects2 , w2 = q2_data

        # assert(    (abs(q1_red_p-q1_red    ) ).sum<1.0e-6 )
        # assert(    (abs(q2_red_p-q2_red    ) ).sum<1.0e-6 )


        # originally the procedure beklow which has been copied,
        # was getting vects obtained from numpy.linalg.eig
        # which gives vectors as columns
        evects1=numpy.transpose(evects1)
        evects2=numpy.transpose(evects2)

        evals1=(numpy.maximum(evals1, numpy.array([bottom_meV/(0.0001239852 *1000)], numpy.float32) ))
        evals2=(numpy.maximum(evals2, numpy.array([bottom_meV/(0.0001239852 *1000)], numpy.float32) ))

        qvect_s1 = numpy.tensordot(  qshifted ,      evects1.reshape([-1,3, Nbranches]) , [[0], [1]   ]  )
        qvect_s2 = numpy.tensordot(  qshifted ,      evects1.reshape([-1,3, Nbranches]) , [[0], [1]   ]  )

        Fjs = numpy.sum(dwfacts_mass_kin_scatt* (qvect_s1.T * qvect_s2.T ) * massfactors, axis=-1)  # sommato sugli ioni e con un mass fctors aggiuntivo

        #####   MASSA ATOMICA 1,007 825 032 u (1,673 534·10-27 kg)  
        #####   h plank 6.626070040(81)×10−34 J s
        #####           4.135667662(25)×10−15 eV s
        #####   c = 29979245800 cm/s
        hplanck = 6.626070041e-27  ## ergs seconds
        clight = 2.9979245800e+10   ## cm/seconds
        massaatomica = 1.673534e-24 /1.007825032  ## grammi

        ## deve equilibrare gli (2pi A-1)^2 di E.Q  ei fattori 2( che va messo), e l'omega in cm-1 (senza pi) al denominatore

        equilibrating_factor = ( hplanck/( 2* clight * massaatomica *(2*math.pi)*(2*math.pi)  )  ) *1.0e16
        #########
        ##  il 2pi al quadrato perche c'e' h invece di hbar e gli omega sono dati in cm-1 senza 2pi.
        ## la velocita della luce perche' gli omega sono in cm-1. La massa atomica perche i massfactors
        ## sono formati con quella relativa solamente, infine il 2 a denominatore perche' nella formula c'e'
        ## Si ottengono cosi' dei cm**2 e si motiplica per 1.0e16 per trasformarli in A-1

        Fjs = Fjs * equilibrating_factor *0.5
        ## l'equilibrating factor va applicato prima del quadrato perche ci sono i due fattori sqrt(hbar/omega/m/2 ) E.Q
        ## Il fattore 0.5 c' e' nella formula : viene dall' espansione in potenze della funzione exp
        intensity_array = ( Fjs*(Fjs.conjugate())).real

        exp_plus1  =  numpy.exp( factor_forcoth * evals1)
        exp_minus1 =  numpy.exp(-factor_forcoth * evals1)
        deno1 = (exp_plus1-exp_minus1)*evals1
        totalstats1 = ( exp_plus1+exp_minus1)/deno1

        exp_plus2  =  numpy.exp( factor_forcoth * evals2)
        exp_minus2 =  numpy.exp(-factor_forcoth * evals2)
        deno2 = (exp_plus2-exp_minus2)*evals2
        totalstats2 = ( exp_plus2+exp_minus2)/deno2

        totalstats = totalstats1*totalstats2

        add += numpy.sum( intensity_array* totalstats      ) * w1  * fattore_part *2
        ## il due finale per la statistica di bose
        

    return add

if(sys.argv[0][-12:]!="sphinx-build"):
    s=open(sys.argv[2], "r").read()
    exec(s)



def main():

    # INPUT_TIMES=[ os.stat(sys.argv[2]  ).st_mtime]

    assert("Temperature"  in dir())

    assert("APPLYTIMEREVERSAL"  in dir())
    assert("bottom_meV"  in dir())

    if "NEUTRONCALC" in dir() and NEUTRONCALC:
        print(" Doing Neutron calculation ")
        assert( "CohB" in dir())
        assert( "NeutronE" in dir())
    else:
        NEUTRONCALC=0
        assert("Lambda"  in dir())

    # if "branchWeight" in dir() or "energyWindow" in dir():
    #   assert("outputname" in dir())
    # if "outputname" in dir():
    #   assert("branchWeight" in dir()  or  "energyWindow" in dir()  )

    redLine=[]
    tickpos=[]
    ticklabel=[]

    redLine=numpy.array(redLine)

    ########################################################
    # 
    # Q=2*sin(theta)*2*Pi/lambda
    def Theta(Q, Lambda  ):
      return numpy.arcsin(  numpy.sqrt( numpy.sum(Q*Q, axis=1)) * Lambda/2.0/math.pi/2.0 )
    #########################################################################

    calculatedDatas = TDS_Reading.CalcDatas.get_CalcDatas_Object_From_File(sys.argv[1])

    NFINES = 4
    calculatedDatas_fines = []
    for ifine in range(1,NFINES+1):
        tok=TDS_Reading.CalcDatas.get_CalcDatas_Object_From_File(sys.argv[1]+"_"+str(ifine))
        calculatedDatas_fines.append(  tok  )
    


    md5postfix= calculatedDatas.Get_md5postfix()
    filename = calculatedDatas.Get_filename()
    cella=TDS_Simmetry.OP_cella(cellvectors=calculatedDatas.cellVects ,
                   AtomNames_long=calculatedDatas.atomNames,
                   PositionsList_flat=calculatedDatas.atomAbsolutePositions )
    simmetries_dict = TDS_Simmetry.FindSimmetries(cella,   filename= filename,
                                                  md5postfix=md5postfix, overwrite= False,
                                                  key = "simmetries_dict" )


    #################################################################################3
    ## 
    ##         A DISPERSION CURVE
    ##

    BV=calculatedDatas.GetBrillVects()
    BVinv=numpy.linalg.inv(BV)

    if Ctransf is not None:
        BV_simple=calculatedDatas.GetBrillVects(Ctransf)
        BVinv_simple=numpy.linalg.inv(BV_simple)
    else:
        BV_simple = BV
        BVinv_simple = BVinv

    
    maxBrill = numpy.max(numpy.sqrt(    (BV_simple*BV_simple).sum(axis=-1) ))

    qradius_min = maxBrill * DeltaQ_min
    qradius_max = maxBrill * DeltaQ_max   ## in ab2tds i vettori Q hanno il 2pi


    if NOREPLICA:
        Qs =     calculatedDatas.GetNonClippedQs()
        frequencies  = calculatedDatas.frequencies
        eigenvectors = calculatedDatas.eigenvectors

        todo = [ calculatedDatas ]+calculatedDatas_fines
        Qs_all = []
        Ws_all = []
        Qs_reds_all = []
        frequencies_all = []
        eigenvectors_all = []
        for tok in todo:
            Ws_all.append( tok.weights )
            
            _qs = tok.GetNonClippedQs() 
            Qs_all.append( _qs  )
   
            Qtok  = numpy.tensordot( _qs , BVinv, axes = [[-1],[0]]    )
            Qs_reds_all.append( Qtok )

            d=calculatedDatas.QsReduced-Qtok
            print(" confronto ")
            print(numpy.abs(d).max())

            frequencies_all.append(  tok.frequencies  )
            eigenvectors_all.append( tok.eigenvectors )

    else:

        todo = [ calculatedDatas ]+calculatedDatas_fines
        Qs_all = []
        Ws_all = []
        Qs_reds_all = []
        frequencies_all = []
        eigenvectors_all = []
        
        for tok in todo:
            res = TDS_Simmetry.GetAllReplicated (  tok, simmetries_dict ,  filename=filename , Nfour_interp=None,
                                                   md5postfix=md5postfix,
                                                   overwrite= False,
                                                   key = "Codes_1" ,
                                                   APPLYTIMEREVERSAL=1,
                                                   CALCULATEFOURIER=0,
                                                   CALCULATECOMPLEMENT=0,
                                                   MAKING=0,
                                                   MAKINGFOURIER=0,
                                                   ZBORN_info=None
                                                   )
            
            tok = res["totWeights"]
            Ws_all.append( tok )

            tok = res["totQs"]
            Qs_all.append( tok )

            tok  = numpy.tensordot(  tok , BVinv, axes = [[-1],[0]]    )
    
            Qs_reds_all.append( tok )

            totDMs =  res["totDMs"]
            frequencies = []
            eigenvectors = []
            for iprog, dm in enumerate(totDMs):
                if iprog%100 ==0:
                    print("iprog ", iprog)
                vals, vects = numpy.linalg.eigh(dm)
                vals = numpy.sqrt(   numpy.maximum( vals,0   )  )
                frequencies.append(vals)
                eigenvectors.append(vects.T)

            frequencies_all.append(  frequencies  )
            eigenvectors_all.append( eigenvectors )


    Ws , Ws_all = Ws_all[0],Ws_all[1:]

    Qs , Qs_all = Qs_all[0],Qs_all[1:]
    Qs_reds , Qs_reds_all = Qs_reds_all[0],Qs_reds_all[1:]
    frequencies , frequencies_all = frequencies_all[0],frequencies_all[1:]
    eigenvectors , eigenvectors_all = eigenvectors_all[0],eigenvectors_all[1:]

    norme = numpy.sqrt( numpy.sum(  Qs * Qs    , axis = -1  )) 
    print("   qradius_min ,    qradius_max        " , qradius_min ,    qradius_max   )
    print("   DeltaQ_min  ,   DeltaQ_max  " , DeltaQ_min  ,   DeltaQ_max)
    print( "  maxBrill  "  , maxBrill)
    print( " norme.max(), norme.min()  ",  norme.max(), norme.min())

    Qs_reds = numpy.tensordot(  Qs, BVinv_simple, axes = [[-1],[0]]    )
    reds =  numpy.max(Qs_reds, axis=-1)


    ####################################################################################

    lut = LUT( Qs_reds_all, Qs_all,Ws_all, frequencies_all ,eigenvectors_all)  
    if DEBUGTESTLUT:
        q1_data = lut.getFV(  Qs_reds_all[-1][-1]  )
        q1_red_p , q1, evals1, evects1 ,w1  = q1_data
        print(" DEBUG LUT")
        print(  numpy.max(evects1-eigenvectors_all[-1][-1] ) .max())
        print(  numpy.max(evals1-frequencies_all[-1][-1] ) .max())



    
    Qs_roi           = Qs[  numpy.less(qradius_min, norme)*numpy.less(norme, qradius_max ) *numpy.less(reds, 0.5)  ]
    frequencies_roi  = frequencies[numpy.less(qradius_min, norme)*numpy.less(norme, qradius_max )*numpy.less(reds, 0.5)  ]
    eigenvectors_roi = eigenvectors[numpy.less(qradius_min, norme)*numpy.less(norme, qradius_max )*numpy.less(reds, 0.5) ]

    dic_ints = {}
    hkl_max=0
    countspot=-1

    # file = open("DEBUG","w")

    for spot in fitSpots:

        countspot+=1
        intensity = numpy.zeros([ Qs_roi.shape[0]  ] , "f"   )

        # qsLine_notShifted = numpy.reshape(   Qs ,  [ (2*Nqs+1)* (2*Nqs+1), 3 ] )
        # intensity_line    =  numpy.reshape( intensity,  [ (2*Nqs+1)* (2*Nqs+1) ])

        qsLine_notShifted  =   Qs_roi 
        intensity_line     =   intensity
        
        qsLine_shifted  =   Qs_roi  + numpy.dot(spot, BV_simple)
        
        if NEUTRONCALC:
            scatterers = numpy.array([  CohB[aname] for aname in  calculatedDatas.atomNames ] , "F" )   
        else:
            tf0 = dabax.Dabax_f0_Table("f0_WaasKirf.dat")
            lambdas= [Lambda]*len(qsLine_notShifted)
            scatterers = [  tf0.Element(aname  ) for aname in  calculatedDatas.atomNames ]

            # print("  SCATTS ")
            # print(calculatedDatas.atomNames)
            # print(numpy.array(lambdas))
            # print(Theta(qsLine_shifted, Lambda  ))
            # for  scatterer in scatterers :
            #     print( scatterer.f0Lambda(numpy.array(lambdas), Theta(qsLine_shifted, Lambda  )   ))
            
            scattFactors_site_q =numpy.array( [ scatterer.f0Lambda(numpy.array(lambdas), Theta(qsLine_shifted, Lambda  )   )
                                             for scatterer in scatterers ]  ).astype(numpy.float32)
            scattFactors_q_site= scattFactors_site_q.T

        Nbranches=3*len(calculatedDatas.atomNames)

        kinematicalFactors      =  numpy.zeros( [  len(qsLine_notShifted),  Nbranches/3  ] , numpy.complex64)
        kinematicalFactors.imag = numpy.tensordot(   qsLine_shifted,
                                                  -calculatedDatas.atomAbsolutePositions  ,  # the minus sign is here
                                                  [[1], [ 1 ]] ) 
        kinematicalFactors = numpy.exp( kinematicalFactors )
        massfactors  = numpy.sqrt(1.0/ calculatedDatas.atomMasses )
        print(" MASSES ")
        print(calculatedDatas.atomMasses)

        h5=h5py.File(sys.argv[3] ,  "r")
        DWs_3X3 = h5["Codes_"+(str(APPLYTIMEREVERSAL))]["DWs"][str(Temperature)]["DWf_33"][:]
        h5.close()
        h5=None

        dwfacts_perq = numpy.tensordot(  qsLine_shifted,DWs_3X3    ,  axes=[[1], [ 1]])

        dwfacts_perq  = dwfacts_perq  *qsLine_shifted[:,None,:]
        dwfacts_perq  = numpy.exp(-numpy.sum(dwfacts_perq , axis=2 ))

        # DA FARE	 : METTER DIPENDENZA DA Q DI SCATTS E DW IN tds2el
        # scattFactors_q_site.T[:] = scattFactors_q_site.T[:]/(scattFactors_q_site.sum(axis=-1))
        # dwfacts_perq.T[:]= dwfacts_perq.T[:]/(dwfacts_perq.sum(axis=-1))

        if NEUTRONCALC:
            dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*  scatterers  
        else:
            dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*scattFactors_q_site

        # fake_int = scattFactors_q_site.sum(axis=-1)
            
        print(dwfacts_perq[0])
        print(scattFactors_q_site[0,:])
        print(qsLine_shifted[0,:])

        debugfacts =  massfactors*kinematicalFactors

        # cambia temperatura in Hartree
        Temperature_Hartree=Temperature /11604.50520/27.211396132

        # amu = 1822.8897312974866 electrons
        # cm-1 ==> Hartree  cm-1 =  4.5563723521675276e-06 Hartree
        # Bohr = 0.529177249 Angstroems

        factor_forcoth = 4.5563723521675276e-06*1.0/(2.0*Temperature_Hartree)

        resevals=[]
        resstokes=[]

        stime=time.time()

        count=-1
        for ( qnotshifted,  qshifted, 
             dwfacts_mass_kin_scatt,
             ) in zip(
                        qsLine_notShifted,qsLine_shifted,
                        dwfacts_mass_kin_scatt_perq
                        ):
          count+=1
          if count%nprocs!=myrank:
              continue
          evals = frequencies_roi[count]
          evects= eigenvectors_roi[count]
          
          # originally the procedure beklow which has been copied,
          # was getting vects obtained from numpy.linalg.eig
          # which gives vectors as columns
          if DEBUGVECTNORM:
              tmp = (evects.conjugate()*evects).sum(axis=-1)
              print(" VECTS NORMS " , tmp)
              
          evects=numpy.transpose(evects)
          evals=(numpy.maximum(evals, numpy.array([bottom_meV/(0.0001239852 *1000)], numpy.float32) ))
          qvect_s = numpy.tensordot(  qshifted ,      evects.reshape([-1,3, Nbranches]) , [[0], [1]   ]  )
          Fjs = numpy.sum(dwfacts_mass_kin_scatt* (qvect_s.T), axis=-1)  # sommato sugli ioni
          Fjs_debug = numpy.sum(debugfacts[count] * (qvect_s.T), axis=-1)  # sommato sugli ioni
          exp_plus =  numpy.exp( factor_forcoth * evals)
          exp_minus = numpy.exp(-factor_forcoth * evals)
          intensity_array = ( Fjs*(Fjs.conjugate())).real
          intensity_array_debug = ( Fjs_debug*(Fjs_debug.conjugate())).real
          if NEUTRONCALC:
              kinstokes = numpy.sqrt(numpy.maximum( (NeutronE-evals*(0.0001239852 *1000))/NeutronE, 0.0 ))
              kinantistk= numpy.sqrt((NeutronE+evals*(0.0001239852 *1000))/NeutronE)
          else:
              kinstokes = 1.0
              kinantistk= 1.0
          deno = (exp_plus-exp_minus)*evals
          totalstats = ( exp_plus*kinstokes+exp_minus*kinantistk)/deno

          #####   MASSA ATOMICA 1,007 825 032 u (1,673 534·10-27 kg)  
          #####   h plank 6.626070040(81)×10−34 J s
          #####           4.135667662(25)×10−15 eV s
          #####   c = 29979245800 cm/s
          hplanck = 6.626070041e-27  ## ergs seconds
          clight = 2.9979245800e+10   ## cm/seconds
          massaatomica = 1.673534e-24 /1.007825032  ## grammi

          ## deve equilibrare gli (2pi A-1)^2 di E.Q  ei fattori 2, e l'omega in cm-1 (senza pi) al denominatore


          equilibrating_factor = ( hplanck/( 2* clight * massaatomica *(2*math.pi)*(2*math.pi)  )  ) *1.0e16
          #########
          ##  il 2pi al quadrato perche c'e' h invece di hbar e gli omega sono dati in cm-1 senza 2pi.
          ## la velocita della luce perche' gli omega sono in cm-1. La massa atomica perche i massfactors
          ## sono formati con quella relativa solamente, infine il 2 a denominatore perche' nella formula c'e'
          ## Si ottengono cosi' dei cm**2 e si motiplica per 1.0e16 per trasformarli in A-1


          intensity_line[count] = numpy.sum( (intensity_array * totalstats *  equilibrating_factor)   )
          if DOSECONDORDER:
              red = numpy.dot( BVinv , qnotshifted )

              if count%100 ==0:
                      print(" in second order ", count)

              
              add = secondorder(qnotshifted,red,   qshifted,  lut.Qs_reds_joined, 
                                lut , massfactors, dwfacts_mass_kin_scatt,factor_forcoth,
                                # lut.origin,
                                # lut.steps,
                                # lut.ends,
                                # lut.lut,
                                # lut.qs_reds, lut.qs, lut.freqs, lut.vects
                                )

              intensity_line[count] += add

          


          # if countspot == 0 and count == -21971 : 
          #     file.write("count=%d\n"%count ) 
          #     file.write( "evals*factor_forcoth\n" ) 
          #     file.write( str(evals*factor_forcoth)+"\n" ) 
          #     file.write( "evects\n" ) 
          #     mf = massfactors[:,None]*numpy.array([1,1,1])
          #     mf.shape =   3*len(massfactors)  ,
          #     file.write( str((evects.T)[:3]*mf)+"\n" ) 
          #     file.write("Q\n")
          #     file.write( str(qnotshifted)+"\n" ) 
          #     file.write("QS\n")
          #     file.write( str(qshifted)+"\n" ) 
          #     file.write("STATS\n") 
          #     file.write(str((totalstats*evals)[:3])+"\n") 
          #     file.write("tmp**2\n") 
          #     file.write(str(intensity_array_debug[:3])+"\n") 
          #     file.close()


        if myrank==0:
            comm.Reduce( MPI.IN_PLACE   , [ intensity_line  , MPI.FLOAT  ], op=MPI.SUM, root=0)
        else:
            comm.Reduce([  intensity_line , MPI.FLOAT  ], None, op=MPI.SUM, root=0)

          
        hkl_max = max( hkl_max, (numpy.abs( numpy.array( spot )   )).max())
        dic_ints[tuple(spot)] = intensity_line

    if myrank==0:

        H = hkl_max
        H2p1 = 2*H+1

        np4hkl = numpy.ones( [ H2p1**3     ],"i"  ) 
        hkls_start = numpy.ones( [ H2p1**3+1     ],"i"  ) 
        count=0
        tot=0
        for h in range(-H,H+1,1):
            for k in range(-H,H+1,1):
                for l in range(-H,H+1,1):
                    if dic_ints.has_key( (h,k,l)  ) :
                        nel = dic_ints[(h,k,l)].size
                    else:
                        nel=0
                    np4hkl[count] =   nel
                    hkls_start[count] = tot
                    tot = tot + nel
                    count=count+1
        hkls_start[count] = tot
        compacted_data = numpy.zeros([tot,4+3],"f")

        count=0
        for h in range(-H,H+1,1):
            for k in range(-H,H+1,1):
                for l in range(-H,H+1,1):
                    if dic_ints.has_key( (h,k,l)  ) :
                        print("--> " , Qs_roi.shape)
                        print(hkls_start[count],hkls_start[count+1])
                        compacted_data[  hkls_start[count]:hkls_start[count+1],:3 ]  = Qs_roi/2.0/numpy.pi
                        compacted_data[hkls_start[count]:hkls_start[count+1] ,3]  = dic_ints[(h,k,l)]
                    count=count+1
        Qmax = qradius_max/2/numpy.pi

        h5=h5py.File( "collection4tds2el.h5" , "w")
        if("harvest" not in h5):
            h5 .create_group("harvest")
        h5["harvest"].create_dataset("hkls_start" , shape=(hkls_start.size,),  dtype=numpy.int32, data=hkls_start.astype("i"))
        h5["harvest"].create_dataset("np4hkl" , shape=(np4hkl.size,),  dtype=numpy.int32, data=np4hkl.astype("i"))
        h5["harvest"].create_dataset("compacted_data" , shape=(compacted_data.size,),  dtype=numpy.float32, data=compacted_data)

        h5["harvest/H" ] =  H
        h5["harvest/H2p1" ] =  H2p1
        h5["harvest/Qmax" ] =  Qmax
        h5["harvest/qcube" ] =  0
        h5["harvest/cellvectors" ] =  numpy.zeros([3,3],"f")
        h5["harvest/Q0" ] =  numpy.zeros([3,3],"f")

        dtypepyob = numpy.dtype({
            'phi'  :( numpy.float32 , 0                        ),
            'merit':( numpy.float32 , 4 ),
            'ij'   :( numpy.int32   , 8 )
        })
        pyob = numpy.zeros([np4hkl.size,],dtype= dtypepyob )
        h5["harvest"].create_dataset("AC_data" , shape=pyob.shape ,  dtype=dtypepyob, data=pyob)
        h5.close()
        h5 = None        

        

                            
