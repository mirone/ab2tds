#include<math.h>


// 3 posizioni
// dimensione
// 1 oppure zero ( 0 --> non foglia )
// 8 puntatori a nodi oppure 3 elementi


#define ROOT 1
#define FOGLIA 0

class OctTree {
public:
  OctTree( double P0, double P1, double P2, double D );     /// python
  OctTree(int len, double * data );   /// python 

  void  insertNodeS( int N,  int N4 ,  double *NodePos);  // python

  void  insertNode( int root,  double *NodePos);

  int findBranch(int  root,double *Pos  );
 
  int  findPosition(int root,double *Pos ) ; 
  void   findPosition(int root,double *Pos, OctTree & collector, double lato ) ; 

  void checkLocality( int root,double * Pos  ) ; 

  inline void setNodePos(int inode, double * P) {
    data[inode*stride+0]=P[0];
    data[inode*stride+1]=P[1];
    data[inode*stride+2]=P[2];
  };

  inline void setNodeSize(int inode, double D) {
    data[inode*stride+3]=D;
  };

  inline void setNodeRootFoglia(int inode, int uno_o_zero) {
    data[inode*stride+4]=uno_o_zero;
    if ( uno_o_zero == FOGLIA ) {
      data[inode*stride+5]=0;
    }
  };



  void   findNearestPosition(int root,double *Pos, double *maxdist, double *bestpos) ;

  void  getcontactelements(int N, int N3, double *posin, int &Nout, int * &pids , double lato);  // python
  void  getdatapointer(int &N, double *&data);                             // python

  inline int tooclose(double *a, double*  b) {
    for(int i=0; i<3; i++) {      
      if (  a[i]> b[i]-tolerance  && a[i]  <   b[i]+tolerance) {
	return 1; 
      }
    }
    return 0;
  }
  inline double distance(double *a, double*  b) {
     double dist=0.0;
     for(int i=0; i<3; i++) {      
       dist +=   (a[i]- b[i])*(a[i]- b[i]);
     }
     dist=sqrt(dist);
     return dist;
  }
  
  double * data;
  double dumbuffer[8];
  int lendata; 
  int stride;
  int Nnodes;
  int MAXPRESENTI;
  double tolerance;
};
