#include <numpy/oldnumeric.h>
#include<numpy/arrayobject.h>


%module octree
%{
#include <numpy/oldnumeric.h>
#include<numpy/arrayobject.h>
#include"octree.h"
%}


%include typemaps.i




%exception {
	try {
	$action
	}
	catch ( char const *msg ) {
	   PyErr_SetString(PyExc_RuntimeError , msg );
	   return NULL;	
	}	

}


%typemap(in) (int  n1,int  n2, double * ArrayFLOAT) (PyArrayObject* tmp=NULL) {

	
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_DOUBLE, 2, 2);

  if(tmp == NULL) return NULL;
  $1 =  tmp->dimensions[0];
  $2 =  tmp->dimensions[1];
  $3 = (double *)tmp->data;
}
%typemap(freearg) ( int  n1,int  n2, double * ArrayFLOAT) {Py_DECREF(tmp$argnum);}



%typemap(in) (int  n, double * ArrayFLOAT) (PyArrayObject* tmp=NULL) {

	
  tmp = (PyArrayObject *)PyArray_ContiguousFromObject($input, PyArray_DOUBLE, 1, 1);

  if(tmp == NULL) return NULL;
  $1 =  tmp->dimensions[0];
  $2 = (double *)tmp->data;
}
%typemap(freearg) (int n,double * ArrayFLOAT) {Py_DECREF(tmp$argnum);}



%typemap(in, numinputs=0) (    double *&mat, int &ncF , int *&corrF,  int &ncT , int *&corrT) ( int ncT ,  int ncF , double *mat,  int *corrF,   int *corrT   ) {
	$1=&mat;	
	$2=&ncF;	
	$3=&corrF;	
	$4=&ncT;	
	$5=&corrT;	
} 	

%typemap(in, numinputs=0) (     int &n , int *&ArrIntRet ) (   int n  ,  int *  ArrIntRet ) {
	$1=&n;	
	$2=&ArrIntRet;	
} 	


%typemap(argout,fragment="t_output_helper") (  int &n , int *&ArrIntRet  )  {
  int nd=1;
  npy_intp dims[1];
  PyObject *arr_a;


  nd=1;
  dims[0]= n$argnum;

  // arr_a = PyArray_FromDims(nd,dims,NPY_INT) ;
  arr_a = PyArray_SimpleNew(nd,dims,NPY_INT) ;
  memcpy( ( (PyArrayObject *) arr_a )->data  ,  *$2 , dims[0]*sizeof(int) );

  delete [] *$2;  

  resultobj = t_output_helper(resultobj,arr_a );
} 



%typemap(in, numinputs=0) (     int &n , double *&ArrFloatRetBorrowed) (   int n  ,  double *  ArrFloatRet ) {
	$1=&n;	
	$2=&ArrFloatRet;	
} 	


%typemap(argout) (  int &n ,  double *&ArrFloatRetBorrowed)  {
  int nd=1;
  npy_intp dims[1];
  PyObject *arr_a;


  nd=1;
  dims[0]= n$argnum;

  // arr_a = PyArray_FromDims(nd,dims,PyArray_DOUBLE) ;
  arr_a = PyArray_SimpleNew(nd,dims,PyArray_DOUBLE) ;
  memcpy( ( (PyArrayObject *) arr_a )->data  ,  *$2 , dims[0]*sizeof(double) );

   //  delete [] *$2; //  perche'Borrowed 

  resultobj = t_output_helper(resultobj,arr_a );
} 




class OctTree {
public:

  OctTree( double P0, double P1, double P2, double D );     /// python

  /* OctTree(int len, double * data );   /// python  */
  OctTree( int  n, double * ArrayFLOAT );   /// python 
  
  
  void  insertNodeS( int  n1,int  n2, double * ArrayFLOAT);  // python


  void  getcontactelements(int  n1,int  n2, double * ArrayFLOAT ,
			   int &n , int *&ArrIntRet, double lato );  // python

  // void  getdatapointer(int &N, double *&data);                             // python
  void  getdatapointer(int &n , double *&ArrFloatRetBorrowed );                             // python

};
%init %{	
  import_array();
%}	

%exception ; 
