#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/


// 3 posizioni
// dimensione
// 1 oppure zero ( 0 --> non foglia )
// 8 puntatori a nodi oppure 3 elementi

#include<string.h>
#include<stdlib.h>
#include<stdio.h>
#include <time.h>



#include"octree.h"


/* #define SAFE_DATA_MODIF( darray, pos, value) {do {	\
//        // cudaError err = call;    \
//        if( darray==15*1203+3 ) {   \
// 	   printf( " changing critical parameter  in file '%s' in line %i : %s.n\n", \
//                    __FILE__, __LINE__, cudaGetErrorString( err) );     \
//            // fprintf(stdout, messaggio );     \
// 	   // throw messaggio;			    \
//        } } while (0); \
//        darray[pos]=value; \
//          }
*/

OctTree::OctTree(  double P0, double P1, double P2, double D ) {

  this->MAXPRESENTI=2;

  this->tolerance = 1.0e-8;
  this->lendata=7;
  this->stride=15;

  this->data=new double[this->stride*this->lendata];
  this->Nnodes=1;

  int inode=0;

  data[inode*stride+0]=P0;
  data[inode*stride+1]=P1;
  data[inode*stride+2]=P2;
  data[inode*stride+3]=D;
  data[inode*stride+4]=FOGLIA;
  data[inode*stride+5]=0;
  

}
  

OctTree::OctTree(int len, double * data ) {

  this->MAXPRESENTI=2;

  this->stride=15;

  len = len /  this->stride; 


  if( len % this->stride  ) {
    throw "  in OctTree::OctTree(int len, double * data )  len must be a multiple of this->stride "; 
  }

  this->lendata=len ; 

  this->data=new double[this->stride * this->lendata];
  this->Nnodes= len    ;
  memcpy( this->data, data, this->stride * len * sizeof(double)  );
}  



void  OctTree::insertNodeS(  int N, int N4 ,  double *NodePos) { 

  if( N4 != 4  ) {
    throw "  in  OctTree::insertNodeS(  int N4 ,  double *NodePos) len must be a multiple of 4 "; 
  }
  
  for (int i=0; i<N; i++) {
    

    this->insertNode( 0 ,  NodePos +4*i );  
  }
} 



void OctTree::insertNode( int root,  double *NodePos) { 



  int target = this-> findPosition( root, NodePos); 


  


  double targetsize = this->data[ target*this->stride + 3   ]; 
  

  //printf("  target size %e \n", targetsize);


  int npresenti =   data[target*stride+5];
  //if (MAXPRESENTI==1)printf(" giaprensenti %d\n", npresenti);
  for(int itok =0; itok<npresenti; itok++) {
    double *p=  this->data + this->stride * target+ 6+ itok*4; 
    // if (MAXPRESENTI==1)printf( " controllo vicinanza %e %e %e \n", *(p+0),*(p+1), *(p+2) ) ;


    if( this->tooclose( NodePos, this->data + this->stride * target+ 6+ itok*4 ) ) {
      return ; 
    }
  }


  if( npresenti<this->MAXPRESENTI) {
    //if (MAXPRESENTI==1)printf( " Aggiungo  \n" ) ;

    data[target*stride+5]+=1 ; 
    memcpy( this->data+ target*this->stride+6 +npresenti*4   ,  NodePos      , 4*sizeof(double)     ) ; 
  } else {
    // if (MAXPRESENTI==1)printf( " ridistribuisco  \n" ) ;

    data[target*this->stride+5]=0; 
    data[target*this->stride+4]=ROOT; 
    

    memcpy( this->dumbuffer , data+ target*stride+6, this->MAXPRESENTI*4*sizeof(double)) ; 
    double newsize = targetsize/2 ; 

    int ibranch=0;
    for(int ix=-1; ix<2; ix+=2) {
      for(int iy=-1; iy<2; iy+=2) {
	for(int iz=-1; iz<2; iz+=2) {

	  if(this->Nnodes==this->lendata) {


	    double  *newdata = new double [ 2*this->lendata* this->stride ] ; 
	    memcpy( newdata, this->data, this->stride* this->lendata*sizeof(double)     );
	    
	    this->data=newdata;
	    this->lendata*=2;
	  }
	  this->data[ this->Nnodes*this->stride + 0   ] = this->data[ target*this->stride + 0   ] +ix* newsize/2 ;  
	  this->data[ this->Nnodes*this->stride + 1   ] = this->data[ target*this->stride + 1   ] +iy* newsize/2 ;  
	  this->data[ this->Nnodes*this->stride + 2   ] = this->data[ target*this->stride + 2   ] +iz* newsize/2 ;  

	  // if( newsize> 5) printf("  new size %e \n", newsize);
	  this->data[ this->Nnodes*this->stride + 3   ] =  newsize;

	  this->data[ this->Nnodes*this->stride + 4   ] =  FOGLIA ;
	  this->data[ this->Nnodes*this->stride + 5   ] =  0 ;
    


	  this->data[ target*this->stride + 6+ibranch   ]= this->Nnodes;

	  this->Nnodes++; 
	  ibranch++;
	}
      }
    } 
    for(int itok =0; itok< MAXPRESENTI; itok++) {
      this-> insertNode(  target,   this->dumbuffer + itok*4); 
    }

    this-> insertNode(  target,  NodePos);

  }
}

void OctTree::checkLocality( int inode,double * Pos  ) {
  
  double halfsize = (this->data[inode*this->stride+3])/2;
  double *rpos = this->data + inode*this->stride ; 


  for(int i=0; i<3; i++) {
   
    if (  Pos[i]< rpos[i]-halfsize || Pos[i]>  rpos[i]+halfsize) {

      // printf("dimension %d  rpos %e halfsize %e  point %e \n",   i ,  rpos[i], halfsize,Pos[i] ); 

      throw " Point falls outside of OcTree   "  ; 
    }
  }

}


int OctTree::findBranch(int  root,double * Pos  ) {
  int branch=0;
  if( Pos[0]> this->data[root*this->stride+0 ]) branch=branch+4 ; 
  if( Pos[1]> this->data[root*this->stride+1 ]) branch=branch+2 ; 
  if( Pos[2]> this->data[root*this->stride+2 ]) branch=branch+1 ; 

  return branch;
}

void   OctTree::findPosition(int root,double *Pos, OctTree & collector, double lato ) {


  if( data[root*stride+4]==FOGLIA) {
    int npresenti =   data[root*stride+5];
    for(int itok =0; itok< npresenti ; itok++) {
      // printf("inserisco \n");
      collector.insertNode(  0 , data + root*stride+6+itok*4   ); 
    }
  } else {
    int ibranch=0;
    int shifts[3]; 
    for(int ix=-1; ix<2; ix+=2) {
      for(int iy=-1; iy<2; iy+=2) {
	for(int iz=-1; iz<2; iz+=2) {

	  shifts[0]=ix;
	  shifts[1]=iy;
	  shifts[2]=iz;

	  int interseca=1; 

	  int inode = this->data[ root*this->stride + 6+ibranch   ];

	  double nodesize = this->data[ inode*this->stride + 3 ] ;

	  for(int icord=0; icord<3; icord++) {


	    
	    // printf( "  %e %e    %e   %e   \n" ,
	    // 	    Pos[icord]-lato/2,
	    // 	    Pos[icord]+lato/2,  
	    // 	    this->data[ inode*this->stride + icord  ] - nodesize/2, 
	    // 	    this->data[ inode*this->stride + icord  ] + nodesize/2
	    // 	    );

 
	    if(     Pos[icord]-lato/2 >  this->data[ inode*this->stride + icord  ] + nodesize/2  ||
		    Pos[icord]+lato/2 <  this->data[ inode*this->stride + icord  ] - nodesize/2 ) {
	      interseca=0; 
	      break;
	    }
	  }
	  if(interseca) {
	    int newroot  = this->data[root*this->stride+6 + ibranch]; 
	    this->findPosition(newroot ,Pos,  collector, lato ); 
	  } else { 
	    // printf( " non interseca \n");
	  }
	  ibranch++;
	}
      }
    }
  }
}

double OctTree::DistFromNearests(int N, double *Pos            ){
  double res=0,d, bestpos[3];
  for(int i=0; i<N; i++) {
    this->findNearestPosition( 0  ,Pos+3*i,  &d , bestpos);
    res=res+d*d;
  }
  return res;
}

void   OctTree::findNearestPosition(int root,double *Pos, double *maxdist, double *bestpos) {

  if( data[root*stride+4]==FOGLIA) {
    int npresenti =   data[root*stride+5];
    for(int itok =0; itok< npresenti ; itok++) {
      double dist ;
      dist = this->distance(Pos, data + root*stride+6+itok*4  );
      if(dist<*maxdist)  {
	*maxdist = dist;
	memcpy(bestpos , data + root*stride+6+itok*4, 3*sizeof(double));
      }
    }
  } else {
    int ibranch=0;
    for(int ix=-1; ix<2; ix+=2) {
      for(int iy=-1; iy<2; iy+=2) {
	for(int iz=-1; iz<2; iz+=2) {
	  
	  int newroot  = this->data[root*this->stride+6 + ibranch]; 
	  
	  double nodesize = this->data[ newroot*this->stride + 3 ] ;
	    
	  double possible_distance ;
	  double worst_distance ;
	  
	  possible_distance = this->distance(Pos, data + newroot*this->stride  ) -  (nodesize/2)*sqrt(3.0);
	  worst_distance = this->distance(Pos, data + newroot*this->stride  ) +  (nodesize/2)*sqrt(3.0);


	  
	  if( worst_distance< *maxdist) *maxdist = worst_distance;
	  
	  if( possible_distance< *maxdist   )  {	  
	    this->findNearestPosition(newroot ,Pos,      maxdist, bestpos); 
	  } else { 
	    // printf( " non interseca \n");
	  }
	  ibranch++;
	}
      }
    }
  }
}



int  OctTree::findPosition(int root,double *Pos ) {   


  this->checkLocality( root, Pos  );

  if( this->data[root*stride+4] == ROOT ) {
    int ramo = this-> findBranch( root, Pos  );

    int newroot  = this->data[root*this->stride+6 + ramo]; 
    return this->findPosition( newroot, Pos ); 
  } else {
    return root ; 
  }
}; 

 

void  OctTree::getcontactelements(int N, int N3, double *posin, int &Nout, int * &pids, double lato ) {

  Nout=Nout+1;
  // printf("  lato %e , Nout %d\n", lato, Nout);

  if( N3!=3) {
    throw " int OctTree::getcontactelements(int N3, double *posin, int &Nout, int * &pids   expects a list of  3D vectors ";
  }
  


  // fare una nuvoletta 
  // aggiungere solo se assenti
  // contare solo
  // allocare
  // rifare aggiungendo

  OctTree   collector (data[0] ,data[1] , data[2],  data[3] );

  collector.MAXPRESENTI=1;

  for( int ipos = 0 ; ipos < N ; ipos ++ ) {
    this->findPosition( 0  ,  posin + 3*ipos, collector, lato ) ; 
  } 
  Nout=0;
  for(int i=0; i<collector.Nnodes ; i++) {
    if(  collector.data[collector.stride*i+4 ]==FOGLIA  ) {
      if( collector.data[collector.stride*i+5 ]==1) {
	Nout+= collector.data[collector.stride*i+5 ];
      }
      if( collector.data[collector.stride*i+5 ]>1) {
	throw " the collector was not supposed to host more than one point " ;
      }
    }
  }

  // printf( " CONTATTI %d   \n", Nout);
  
  pids = new int [ Nout   ] ;
  
  Nout=0;
  for(int i=0; i<collector.Nnodes ; i++) {
    if(  collector.data[collector.stride*i+4 ]==FOGLIA ) {
      if( collector.data[collector.stride*i+5 ]==1) {
	pids[Nout ] = collector.data[collector.stride*i+5+4 ]  ; 
	// printf( " %d %e %e %e \n", pids[Nout ],
	// 	collector.data[collector.stride*i+5+1 ],
	// 	collector.data[collector.stride*i+5+2 ],
	// 	collector.data[collector.stride*i+5+3 ]
	// 	) ;


	Nout++;
      }
    }
  }
}

void  OctTree::getdatapointer(int &N, double *&data) { 
  data = this->data ; 
  N = this-> Nnodes*this->stride ; 
}



int  main () {

  srand(time(NULL));



  
  double P[4]; 
  double D ; 


  D=10 /120.  ; 

  P[0]=20 /120.  ;
  P[1]=10/120. ; 
  P[2]=5 /120. ; 

  try {
    OctTree   albero (P[0] ,P[1] , P[2],  D );
    
    int N=210000*0+10; 
    
    double NodePos [ 4*N] ; 
    
    for(int i=0; i<N*4; i++) {
      NodePos[i] =  (rand()*1.0/RAND_MAX   -1.0/2)*D  + P [ i%4  ]  ; 
      if(i%4==3) {
	NodePos[i]=  1.0/1000*   i/4; 
      }
    }
    
    
    
    // printf( "inserisco nodi \n") ; 
    albero.insertNodeS(  N, 4  ,  NodePos);  // python
    // printf( "inserisco nodi OK  \n") ; 
    


    
    int N3=4*3; 
    
    double P3 [ N3] ; 
    double Ptest [ 3] ; 
    
    
    printf( " --- Probe Points \n");
    for(int i=0; i<N3; i++) {
      P3[i] =  (rand()*1.0/RAND_MAX   -1./2)*D  + P [ i%3 ]  ; 
      printf("%e  ", P3[i] ) ;
      if(i%3==2) printf( "\n");
    }
    for(int i=0; i<3; i++) {
      Ptest[i] =  (rand()*1.0/RAND_MAX   -1./2)*D  + P [ i%3 ]  ; 
    }
    
    printf( " --- Grid Points \n");
    for(int i=0; i<N*4; i++) {
      printf("%e  ", NodePos[i] ) ;
      if(i%4==3) printf( "\n");
    }
    
    
    int Nout; 
    int *pids; 
    
    albero.getcontactelements( N3/3, 3,  P3, Nout, pids, 0.1 );  // python
    
    printf( "---------- I found %d neighbours \n  -----------", Nout);
    
    for(int i=0; i< Nout; i++ ) {
      printf( " %e %e %e    \n", NodePos[ pids[i]*4+0],  NodePos[ pids[i]*4+1],  NodePos[ pids[i]*4+2]);
    }
    
    
    albero.getcontactelements( N3/3, 3,  P3, Nout, pids, 0.1 );
    
    
    double maxdist=1.0e30;
    double bestpos[3];
    
    // albero.findNearestPosition(0 , Ptest , &maxdist, bestpos) ;
    albero.findNearestPosition(0 , Ptest, &maxdist, bestpos) ;
    
    printf("SEARCH OF NEAREST \n");
    printf(" MinDist = %e  POS = %e %e %e\n", maxdist, bestpos[0], bestpos[1], bestpos[2]);
    printf("   Ptest = %e %e %e\n",  Ptest[0], Ptest[1],Ptest [2]);
    
    printf(" SIMPLE SEARCH " );
    maxdist=1.0e30;
    
    for(int i=0; i< N ; i++) {
      double dist = albero.distance( Ptest, NodePos+4*i );
      if ( dist< maxdist) {
	maxdist = dist;
	memcpy(bestpos, NodePos+4*i, 3*sizeof(double));
      }    
    }
    printf(" MinDist = %e  POS = %e %e %e\n", maxdist, bestpos[0], bestpos[1], bestpos[2]);
  } catch (char  const * messaggio ) {
    printf(" CATCHED : %s \n", messaggio);
    
  }
}
