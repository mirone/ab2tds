
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

print( """
#/*##########################################################################
# 
# Welcome to Ab2tds!
#
# For documentation see https://forge.epn-campus.eu/html/ab2tds/
#
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is developed at
# the ESRF by the Scientific Software staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/
""")

try:
    import logs
#    print " THIS IS THE COMMIT HISTORY "
#    print logs.logs
except:
    print (" git LOG  file not available ")
    
try:
    import diffoutput
#    print " THIS IS THE DIFF FROM LAST COMMIT OF LOCAL BRANCH "
#    print diffoutput.diffoutput
except:
    print (" git diffoutput   file not available ")
    
