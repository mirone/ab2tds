
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


""" The script  make_TDS_DW  create the DW factors for a given temperature. The script does an integral sum over the whole Brillouin zone using previous knowledge of the dynamical matrix  that has been obtained, through make_TDS_Simmetrization. The output is written into the hdf5 file associated to the castep output file.


    * The usage is ::

            make_TDS_DW castep_filename input_filename  

      the file *castep_filename*  can be either the name of the original castep output
      our the associated hdf5 file. In any case the associated hdf5 file must exist already (make_TDS_Simmetrization
      must be runned beforehand )

    * The input_file must set the variables :

       * TEMPERATURE
       * APPLYTIMEREVERSAL

      using a python syntax

The input variables are documented with docstrings below
  
""" 


from . import startmessage

from . import TDS_Simmetry      
from . import TDS_Reading

import numpy

import sys
import time
import copy

Temperature=100.0
"""
   The temperature at which DW factors are calculated.
   Units are Kelvin
"""

APPLYTIMEREVERSAL=1
""" * write APPLYTIMEREVERSAL=1 in input file to duplicate eigenvectors at K to get those at -K by complex conjugation 
    * write APPLYTIMEREVERSAL=0  otherwise """

if(sys.argv[0][-12:]!="sphinx-build"):
    s=open(sys.argv[2], "r").read()
    exec(s)

def main():
    

    calculatedDatas = TDS_Reading.CalcDatas.get_CalcDatas_Object_From_File(sys.argv[1])

    md5postfix= calculatedDatas.Get_md5postfix()
    filename = calculatedDatas.Get_filename()

    cella=TDS_Simmetry.OP_cella(cellvectors=calculatedDatas.cellVects ,
                   AtomNames_long=calculatedDatas.atomNames,
                   PositionsList_flat=calculatedDatas.atomAbsolutePositions )

    simmetries_dict = TDS_Simmetry.FindSimmetries(cella,   filename= filename,
                                                  md5postfix=md5postfix, overwrite= False,
                                                  key = "simmetries_dict" )

    # print( """)
    #     *** Now print(ing all the simmetry operation of the crystal. *** )
    # """

    # TDS_Simmetry.print(Simmetries(simmetries_dict["RotList"][:] , simmetries_dict["ShiftList"][:]))

    # print( """)
    #   *** Now print(ing all the different rotations that can be applied to Qs vectors. *** )
    # """
    #TDS_Simmetry.print(Rotations(  simmetries_dict["DiffRotList"][:]  ))



    print( "GOING TO CALCULATE DW, Temperature  = ", Temperature)


    TDS_Simmetry.CalcDWatT(Temperature ,  calculatedDatas, filename= filename, md5postfix=md5postfix, overwrite= True,
                   key = "Codes_"+(str(APPLYTIMEREVERSAL)), MAKING=1 ,simmetries_dict=simmetries_dict )


