
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
import h5py
import string
import numpy as np
import scipy.ndimage.filters as filters


def main(nomefile,size,tsh):
    h=h5py.File(nomefile,'r')
    N1= ( h['/N1']).value
    N2= ( h['/N2']).value
    N3= ( h['/N3']).value
    bigresult= ( h['/bigresult'])[:]
    subN1= ( h['/subN1']).value
    subN2= ( h['/subN2']).value
    subN3= ( h['/subN3']).value
    BV= ( h['/BV'])[:]
    h.flush()
    h.close()

    filtered=filters.maximum_filter( bigresult ,(size,size,size),mode="wrap")
    positions = np.where(bigresult==filtered)
    l=[]
    for iz,iy,ix in zip(*positions) :
        l.append( ( bigresult[iz,iy,ix ],iz,iy,ix ))
    l.sort()
    for tl in l:
        if tl[0]> tsh :
            print( " %10.6f %10.6f %10.6f  %e" %( tl[1]*1.0/subN1,tl[2]*1.0/subN1,tl[3]*1.0/subN1,  tl[0]  ) )

    print( " N1,N2,N3, subN1,subN2,subN3  " ,  N1,N2,N3, subN1,subN2,subN3 )
    print(  bigresult.shape)
size = string.atoi(sys.argv[2])
tsh   = string.atof(sys.argv[3])
main(sys.argv[1],size,tsh)
