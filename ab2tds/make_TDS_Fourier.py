
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/
r"""
    This script calculates Fourier Transform of the dynamical matrix.

    * The usage is ::

            make_TDS_Fourier castep_filename input_filename  

      the file *castep_filename* can be either the name of the original castep output or the associated hdf5 file. In any case the associated hdf5 file must exist already: make_TDS_Simmetrization must have been run beforehand in order to fill by symmetry operations the missing part of the Brillouin zone. The Fourier transform is performed by summing over the whole Brillouin zone the periodic dynamical matrix 

      .. math::  FT(P(D), {\vec R_j}) = \sum_i w_i P(D{(\vec q_i}))) exp( -i {\vec q_i}\cdot {\vec R_j}) 

      where :math:`i` runs over all provided q-points (the original and the simmetry-replicated ones). The vectors
      :math:`R_j` are defined in real space on the Bravais lattice of the crystal. 

      The Dynamical matrix is defined in periodic notation
      in :math:`Q` space. It is obtained on the basis of eigenvectors  :math:`{\mathbf e_v}` and eigenvalues  :math:`\omega^2_v`:
      
      .. math::   D = \sum_v  \omega^2_v {\mathbf e_v} \otimes   {\mathbf e_v}^\ast

      provided by the input file. If the eigenvectors are periodic in :math:`Q` the matrix is already periodic, but often, as it is the case
      for castep, the natural choice is outputting Bloch-Theorem satisfying vectors. In this latter case the periodicity of the dynamical matrix
      is ensured by adding/removing the proper phases.

    * The input_file must set the variables:

       * Nfour_interp
            this variable is very important. The cubic grid counts :math:`2 Nfour_interp + 1` points along each primitive Bravais axis.
            This number must be large enough to exceed the spatial range of the forces in order to cope with the variation of the Dynamical matrix: the longer the interaction distance, the higher 
            the necessary Nfour_interp will be. A too large Nfour_interp may however cause artefacts. In this case either you
            have to lower it, or if it is really necessary to keep long distance interactions you have to increase the number of 
            calculated q-points in the input file.
       * APPLYTIMEREVERSAL

      using a python syntax

    The input variables are documented with docstrings below 

"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from . import startmessage

from . import TDS_Simmetry      
from . import TDS_Reading

import numpy

import sys
import time
import copy


OVERWRITE=False

Nfour_interp=5
"""  The number of points in each direction of the 3D reciprocal grid
"""
APPLYTIMEREVERSAL=1
"""
  this is one by default. Must be coherent with previous steps
"""

if(sys.argv[0][-12:]!="sphinx-build"):
    s=open(sys.argv[2], "r").read()
    exec(s)

def main():
    

    CALCULATEFOURIER = 1
    CALCULATECOMPLEMENT= 0


    calculatedDatas = TDS_Reading.CalcDatas.get_CalcDatas_Object_From_File(sys.argv[1])


    md5postfix= calculatedDatas.Get_md5postfix()
    filename = calculatedDatas.Get_filename()


    cella=TDS_Simmetry.OP_cella(cellvectors=calculatedDatas.cellVects ,
                   AtomNames_long=calculatedDatas.atomNames,
                   PositionsList_flat=calculatedDatas.atomAbsolutePositions )

    simmetries_dict = TDS_Simmetry.FindSimmetries(cella,   filename= filename,
                                                  md5postfix=md5postfix, overwrite= OVERWRITE,
                                                  key = "simmetries_dict" )


    print( "GOING TO GENERATE FOURIER TRANSFORM WITH Nfour_interp = ", Nfour_interp)

    Replica = TDS_Simmetry.GetAllReplicated (  calculatedDatas, simmetries_dict,   filename= filename,
                                               md5postfix=md5postfix,
                                               overwrite= OVERWRITE ,
                                               key = "Codes_"+(str(APPLYTIMEREVERSAL)) ,
                                               APPLYTIMEREVERSAL=APPLYTIMEREVERSAL,
                                               CALCULATEFOURIER =CALCULATEFOURIER ,
                                               CALCULATECOMPLEMENT=CALCULATECOMPLEMENT,
                                               Nfour_interp=Nfour_interp,
                                               MAKING=0,
                                               MAKINGFOURIER=1
                                         )

