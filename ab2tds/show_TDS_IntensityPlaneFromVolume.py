
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/
""" The script show_TDS_IntensityPlaneFromVolume shows 2D cuts from a 3D TDS intensities distribution calculation. 

      * symmetrisation
      * Fourier transform
      * Debye waller calculation for the temperature of choice
      * 3D intensity distribution of TDS      

    * The usage is ::

        show_TDS_IntensityPlaneFromVolume IntensityVolume_xxx\_.h5  

      The file IntensityVolume\_xxx\_.h5 must contain the results from a make_TDS_intensityVolume calculation.

    * The input_file must set the variables :

      * Mandatories

          * redA
          * redB
          * redC
          * Nqs 
          * DQ
          * Saturation
          * Saturation_min
      
      * Optional 

          * COLOR

The input variables are documented with docstrings below
  
""" 

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
g
from . import startmessage

import h5py
import numpy

import matplotlib
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure, show, axes, sci
from matplotlib import cm, colors
from matplotlib.font_manager import FontProperties
import pylab

import sys
import time
import string
import math

redA = [ 1.0 , 1.0  ,0.0  ]
""" 1. q-point defining plane
"""
redB = [ 0.0  ,0.0  ,0.0  ]
""" 2. q-point defining plane
"""
redC = [ 0.0 , 0.0 , 1.0 ] 
""" 3. q-point defining plane
"""
Saturation=0.1
""" To limit intensity at peaks: intensity is saturated at this value 
"""
Saturation_min=1.0e-7
""" To limit intensity at peaks : intensity is saturated at this value 
"""
Nqs   = 20
""" Number of q-points along one axis
"""
DQ    = 0.1
""" q-resolution in 1/Ang
"""
COLOR = 1
""" Intensity in color or grayscale
"""

if(sys.argv[0][-12:]!="sphinx-build"):

  s=open(sys.argv[2], "r").read()
  exec(s)
  INPUT_TIMES=[ os.stat(sys.argv[2]  ).st_mtime]



  assert("redA"  in dir())
  assert("redB"  in dir())
  assert("redC"  in dir())

  assert("Nqs"  in dir())
  assert("DQ"  in dir())
  assert("Saturation"  in dir())
  assert("Saturation_min"  in dir())


  CMGRAY=cm.gray
  if "COLOR" in globals():
    if COLOR:
      CMGRAY=None



  h5=h5py.File(sys.argv[1] , "r")
  bigresult = h5["bigresult"][:] 
  subN1     = h5["subN1"    ] .value
  subN2     = h5["subN2"    ] .value
  subN3     = h5["subN3"    ] .value
  N1        = h5["N1"       ] .value
  N2        = h5["N2"       ] .value
  N3        = h5["N3"       ] .value
  BV        = h5["BV"][:]              
  BVinv=numpy.linalg.inv(BV)


  Center =  numpy.array(redA)+numpy.array(redB)+numpy.array(redC)
  Center =  numpy.dot( Center , BV ) /3.0
  Xaxis  =  numpy.dot(  numpy.array(redB)-numpy.array(redA) ,    BV)
  Xaxis  =  numpy.array( Xaxis/math.sqrt(numpy.sum(Xaxis*Xaxis)) , "f")
  Yaxis  =  numpy.dot(  numpy.array(redC)-numpy.array(redA) ,    BV)
  Yaxis  =  Yaxis - Xaxis*(numpy.sum(Yaxis*Xaxis))
  Yaxis  =  numpy.array( Yaxis/math.sqrt(numpy.sum(Yaxis*Yaxis)) , "f")
  Zaxis  =  numpy.zeros(3, "f" )
  for i in range(3):
      Zaxis[i] = Xaxis[ (i+1)%3]* Yaxis[ (i+2)%3]-Yaxis[ (i+1)%3]* Xaxis[ (i+2)%3]

  Center = numpy.array(Center,"f")
  Xaxis  = numpy.array(Xaxis,"f")
  Yaxis  = numpy.array(Yaxis,"f")
  Zaxis  = numpy.array(Zaxis,"f")


  # N1,N2,N3,subN1,subN2,subN3, BV, BVinv, bigresult, Nqs,DQ
  def getPlane( Center,Xaxis,Yaxis,Zaxis):

      qdispls = (numpy.arange(2*Nqs+1)-Nqs)*DQ
      intensity = numpy.zeros([ (2*Nqs+1), (2*Nqs+1)         ] , "f"   )
      Qs      =  Center  + qdispls[None,:, None]* Xaxis +  qdispls[:,None,None]  * Yaxis
      qsLine_notShifted = numpy.reshape(   Qs ,  [ (2*Nqs+1)* (2*Nqs+1), 3 ] )
      intensity_line    =  numpy.reshape( intensity,  [ (2*Nqs+1)* (2*Nqs+1) ])

      redQs = numpy.tensordot(  qsLine_notShifted  , BVinv, [[ 1  ],[0  ]] )

      float_pos = numpy.array([N1*subN1,N2*subN2,N3*subN3]) + numpy.array([subN1,subN2,subN3])*redQs
      disc_pos = {}
      flor_pos = numpy.floor(float_pos  )


      res=0
      for i in [0,1]:
        for j in [0,1]:
          for k in [0,1]:
            poss = numpy.minimum(numpy.maximum( numpy.floor(float_pos + numpy.array([i,j,k])), numpy.array([0,0,0])),
                                 numpy.array([(2*N1)*subN1-1,
                                              (2*N2)*subN2-1,
                                              (2*N3)*subN3-1])  ).astype(numpy.int32)

            add = bigresult[  poss[:,0], poss[:,1], poss[:, 2] ]
            for ipos, ival in zip( (0,1,2), (i,j,k) ) :
              if(ival==1):
                add =add * (float_pos[:,ipos]-flor_pos[:,ipos])
              else:
                add =add * (flor_pos[:,ipos]+1 -float_pos[:,ipos])
            intensity_line[:]+=add

      return intensity, Qs

  intensity, Qs =  getPlane( Center,Xaxis,Yaxis,Zaxis)
  if(Saturation > 0 ):
    intensity[:]= numpy.minimum(intensity, Saturation)
    intensity[0,1]=Saturation

  if(Saturation_min > 0):
    intensity[:]= numpy.maximum(intensity, Saturation_min)
    intensity[0,0]=Saturation_min


  res=intensity[::-1]

  fig=figure()
  axes=fig.add_subplot(111)

  axes.set_title("Plane from Volume interpolation")

  # cm.clim(math.log(Saturation_min), math.log(Saturation))
  ima=axes.imshow(numpy.log(numpy.maximum(res,1.0e-14))/math.log(10.0),cmap=CMGRAY)

  cbar=fig.colorbar(ima)
  axes.set_aspect('auto')

  Xtickpos = [  0  ]
  Xticklabels = [ str( numpy.dot(Xaxis,BVinv)  )   ]
  Ytickpos = [  0  ]
  Yticklabels = [ str( numpy.dot(Yaxis,BVinv)  )   ]

  axes.xaxis.set_ticks(numpy.array(Xtickpos) )
  axes.xaxis.set_ticklabels(Xticklabels )
  axes.yaxis.set_ticks(numpy.array(Ytickpos) )
  axes.yaxis.set_ticklabels(Yticklabels, rotation='vertical'  )

  ima.set_extent([-Nqs-0.5,Nqs+0.5, -Nqs-0.5 ,  Nqs+0.5] )

  last_line=[None,None, None,None, None]


  def onclick(event ):

      if event.button in [1,2] and last_line[2] is not None:
        data=last_line[2]
        if event.button == 1:
          data[:]=(data+Nqs)/1.5-Nqs
        else:
          data[:]=(data+Nqs)*1.5-Nqs

        last_line[1].remove( )

        if last_line[4]==1:
          last_line[1] = matplotlib.lines.Line2D( numpy.arange(-Nqs,Nqs+1) ,  data  ,    color="r"   )
        else:
          last_line[1] = matplotlib.lines.Line2D( data  ,  numpy.arange(-Nqs,Nqs+1)[::-1]   ,   color="r"   )

        if last_line[3] is None :
          last_line[3]=ax=fig.add_subplot(111)
        else:
          ax = last_line[3]

        ax.add_line( last_line[1] )
        fig.show()


      if event.button == 3: #right click

        x0_=event.xdata
        y0_=event.ydata
        x0 = int(event.xdata+100000+0.4999)-100000
        y0 = int(event.ydata+100000+0.4999)-100000
        x = event.x
        y = event.y

        if(x0_>y0_):
          last_line[4]=0
          pinredLine = x0
        else:
          last_line[4]=1
          pinredLine = y0


        if pinredLine>=-Nqs and pinredLine<=Nqs:
          print( "     POINT =  "  ,  Qs[x0+Nqs,y0+Nqs   ])
          print( " RED POINT =  "  ,  numpy.dot(Qs[y0+Nqs, x0+Nqs]  , BVinv))
          print( "x,y", x,y)
          print( "x0,y0", x0,y0)
          print( "VALUE ", res[::-1][Nqs+y0, Nqs+x0])
          print( "VALUE ", intensity[Nqs+y0, Nqs+x0])


          if last_line[0] is not None:
            last_line[0].remove( )
            last_line[1].remove( )

          if last_line[3] is None :
            last_line[3]=ax=fig.add_subplot(111)
          else:
            ax = last_line[3]


          if(x0_>y0_):
            last_line[0] = matplotlib.lines.Line2D([x0, x0], [-Nqs  ,Nqs   ], color="r")
            last_line[2] = -Nqs + (2*Nqs+1)*intensity[:,pinredLine+Nqs ]/numpy.max(intensity[:, pinredLine +Nqs ])
            last_line[1] = matplotlib.lines.Line2D(  last_line[2]  ,numpy.arange(-Nqs, Nqs+1) , color="r")
            print( "NORMALIZATION TO ", numpy.max(res[:,pinredLine+Nqs ]))
          else:
            last_line[0] = matplotlib.lines.Line2D([-Nqs  ,Nqs   ],[y0, y0],  color="r")
            last_line[2] = -Nqs + (2*Nqs+1)*intensity[pinredLine+Nqs , :]/numpy.max(intensity[pinredLine+Nqs , : ])
            last_line[1] = matplotlib.lines.Line2D( numpy.arange(-Nqs, Nqs+1) ,  last_line[2]  ,color="r")
            print( "NORMALIZATION TO ", numpy.max(res[pinredLine+Nqs , :]))

          ax.add_line( last_line[0] )
          ax.add_line( last_line[1] )
          fig.show()

  cid = fig.canvas.mpl_connect('button_press_event', onclick)



  # def on_key(event):
  def on_key():
      global ima, cbar, last_line

      if  INPUT_TIMES[0]==os.stat(sys.argv[2]).st_mtime:
        return

      INPUT_TIMES[0]=os.stat(sys.argv[2]).st_mtime


      last_line=[None,None, None,None, None]



      #print( 'you pressed', event.key, event.xdata, event.ydata)

      s=open(sys.argv[2], "r").read()
      exec(s)

      Center[:] =  numpy.array(redA)+numpy.array(redB)+numpy.array(redC)
      Center[:] =  numpy.dot( Center , BV ) /3.0
      Xaxis[:]  =  numpy.dot(  numpy.array(redB)-numpy.array(redA) ,    BV)
      Xaxis[:]  =  numpy.array( Xaxis/math.sqrt(numpy.sum(Xaxis*Xaxis)) , "f")
      Yaxis[:]  =  numpy.dot(  numpy.array(redC)-numpy.array(redA) ,    BV)
      Yaxis[:]  =  Yaxis - Xaxis*(numpy.sum(Yaxis*Xaxis))
      Yaxis[:]  =  numpy.array( Yaxis/math.sqrt(numpy.sum(Yaxis*Yaxis)) , "f")
      Zaxis[:]  =  numpy.zeros(3, "f" )
      for i in range(3):
          Zaxis[i] = Xaxis[ (i+1)%3]* Yaxis[ (i+2)%3]-Yaxis[ (i+1)%3]* Xaxis[ (i+2)%3]

      intensity[:], Qs[:] =  getPlane( Center,Xaxis,Yaxis,Zaxis)
      if(Saturation > 0 ):
        intensity[:]= numpy.minimum(intensity, Saturation)
        intensity[0,1]=Saturation

      if(Saturation_min > 0):
        intensity[:]= numpy.maximum(intensity, Saturation_min)
        intensity[0,0]=Saturation_min

      # print intensity

      matplotlib.pyplot.clf()
      axes=fig.add_subplot(111)
      axes.set_title("Plane from Volume interpolation")
      ima=axes.imshow(numpy.log(numpy.maximum(res,1.0e-14))/math.log(10.0),cmap=CMGRAY)
      cbar=fig.colorbar(ima)
      axes.set_aspect('auto')

      Xticklabels[0] =  str( numpy.dot(Xaxis,BVinv)  )   
      Yticklabels[0] =  str( numpy.dot(Yaxis,BVinv)  )       
      axes.xaxis.set_ticks(numpy.array(Xtickpos) )
      axes.xaxis.set_ticklabels(Xticklabels )
      axes.yaxis.set_ticks(numpy.array(Ytickpos) )
      axes.yaxis.set_ticklabels(Yticklabels, rotation='vertical'  )
      ima.set_extent([-Nqs-0.5,Nqs+0.5, -Nqs-0.5 ,  Nqs+0.5] )

      fig.show()


  # Create a new timer object. Set the interval 500 milliseconds (1000 is default)
  # and tell the timer what function should be called.
  timer = fig.canvas.new_timer(interval=100)
  timer.add_callback(on_key)
  timer.start()

  # cidk = fig.canvas.mpl_connect('key_press_event', on_key)

  show()



