
#/*##########################################################################
# Copyright (C) 2011-2014 European Synchrotron Radiation Facility
#
#              Ab2tds  
#  European Synchrotron Radiation Facility, Grenoble,France
#
# Ab2tds is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for Ab2tds: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# Ab2tds is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Ab2tds; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# Ab2tds follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/
r""" The script make_TDS_DispersionIntensityCurves calculates inelastic scattering intensities for x-rays and neutrons. It has interactive capabilities to let you choose two modes at a given q-point and look for translations into other Brillouin zones which optimize the contrast (see below *Interactions*) It requires that you have performed the following operations beforehand:

      * symmetrisation
      * Fourier Transform
      * Debye waller calculation for the temperature of choice
    
    * The usage is  ::

            make_TDS_DispersionIntensityCurves castep_filename input_filename  

      the file *castep_filename* can be either the name of the original castep output
      the associated hdf5 file. In any case the associated hdf5 file must exist already (make_TDS_Simmetrization
      must be run beforehand)

    * The input_file must set the variables :

      * Mandatories

        *  APPLYTIMEREVERSAL
        *  redStarts
        *  redEnds
        *  Nqlines
        *  Nfour_interp
        *  Temperature
        *  resolutionfile
        *  Saturation
        *  lowerLimit
        *  bottom_meV
        *  tth_max        (degrees)

      * If NEUTRONCALC==0 ( default )

        * Lambda

      * If NEUTRONCALC==1

        * CohB
        * NeutronE

      * Optional

        * branchWeight
        * Eigscal
        * UniqueIon

    EXTRA OUTPUT

      * File summedoverQs.dat

        contains two columns : energy , intensity.   Obtained after summing over Qs

      * File  alongtheline_TDS.dat
     
        energy integrated intensity (TDS) for each q-value 

    INTERACTIONS

        * To toggle back and forth the  *Choosing* mode (selection mode) type C with the graph as active window.
        * Choosing branch A :

             when you are in selection mode left-click over the branch. The q-point also is selected according to the click position. 
        * Choosing branch B :

              right click
        
        * To change manually selected points :  type m
                this is sometimes necessary to disambiguate overlapping branches.
                This interaction opens an emacs editor with variable assignation.
                When you save and exit, the variables are updated with new values if you have modified them.
                You can, in this way, change the branch number and see if there are ambiguous neighbours.

        * To run the optimisation :  type M
                This launches a calculation of equivalent Qpoints in a wide range of cells (from -6 to 6 along all three axis )
                which is centred over the initial q-point. New points are considered if they are physical : K < 2pi/lambda.
                The calculation writes the intensity of the two branches on file  *choosen_modes.txt*.  

                The first three columns are the reduced reciprocal shift applied to the original Q point.
                The fourth is scattering angle theta in degree.
                The last three columns are Ia, Ib and Ia/Ib ( the constrast )
                The lines are ordered in descending order of contrast.

    The implemented formula is the following 

    .. math:: I(\omega, {\bf Q}) = \sum_v \delta(\omega-\omega_v)  \left \|   \sum_\alpha  S_\alpha(\lambda,Q) exp(-\sum_{ij} W^\alpha_{ij} Q_i Q_j ) exp(-i {\bf Q} \cdot {\bf R}_\alpha) \frac{{\bf Q}\cdot {\bf e}^\alpha_{v{\bf Q}}} { M_\alpha^{1/2}  }\right \|^2

    where :math:`v` is the branch index,  :math:`\alpha` the atom index,  :math:`S` is the scattering factor for a given wavelenght ang angle,
    :math:`W` are the Deby-Waller coefficients for the given temperature,  :math:`R` the atomic positions,  :math:`M` the atomic masses 
    and  :math:`{\bf e}` the eigenvectors.

The input variables are documented with docstrings below
  
""" 

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from . import startmessage

from . import TDS_Simmetry      
from . import TDS_Reading
from . import TDS_Interpolation
import h5py
import matplotlib.cm as cm

import numpy

import sys
import time
import copy

import string
from . import  dabax
import math
from . import EdfFile
import os
import tempfile
import subprocess



import numpy as np
import matplotlib
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure, show, axes, sci
from matplotlib import cm, colors
from matplotlib.font_manager import FontProperties

def string_split(s,*arg):
    return s.split(*arg)

def list_map(f,l):
    return list(map(f, l))



def check_input():

  s=open(sys.argv[2], "r").read()
  exec(s)
  
  assert("Temperature"  in dir())
  assert("Nfour_interp"  in dir())
  assert("Nqlines"  in dir())
  assert("redEnds"  in dir())
  assert("redStarts"  in dir())
  assert("resolutionfile"  in dir())
  assert("APPLYTIMEREVERSAL"  in dir())
  assert("Saturation"  in dir())
  assert("lowerLimit"  in dir())
  assert("bottom_meV"  in dir())
  assert("tth_max"  in dir())

  if "NEUTRONCALC" in dir() and NEUTRONCALC:
    assert( "CohB" in dir())
    assert( "NeutronE" in dir())
  else:
    assert("Lambda"  in dir())





def show_contrast():

  dataC=np.loadtxt("./choosen_modes_contrast.txt")
  dataI=np.loadtxt("./choosen_modes_intensity.txt")

  fig = plt.figure()
  ax = fig.add_subplot(111)
  ax.set_title('A and B Intensities ordered by contrast')

  line4, = ax.plot( dataC[:,6],   dataC[:,4], 'o', picker=5)  # 5 points tolerance
  line5, = ax.plot( dataC[:,6],   dataC[:,5], 'o', picker=5)  # 5 points tolerance


  ax.set_xscale('log')
  ax.invert_xaxis()
  extrainfo={line4:4, line5:5}

  ax.set_xlabel("Contrast")
  ax.set_ylabel("Intensity")



  def onpick(event):
      thisline = event.artist

      xdata = thisline.get_xdata()
      ydata = thisline.get_ydata()

      ind = event.ind

      if type(ind) == type(1):
          ind=[ind]

      inds=ind

      print( " ------------------------------------------")
      print( " ------------------------------------------")
      print( " ------------------------------------------")

      for ind in inds:


          # print( 'onpick points:', zip(xdata[ind], ydata[ind]))

      # pos=int(xdata[ind])
          pos = ind

          print( " Mode A , intensity ", dataC[pos][4])
          print( " Mode B , intensity ", dataC[pos][5])
          print( " Contrast           ", dataC[pos][4]/dataC[pos][5])
          print( " Qs                 ", dataC[pos][:3])

  fig.canvas.mpl_connect('pick_event', onpick)

  plt.show()






if(sys.argv[0][-12:]!="sphinx-build"):
  check_input()



APPLYTIMEREVERSAL=1
"""
  this is one by default. Must be coherent with previous steps
"""

redStarts=[[-2.0 , -2.0, 0.], [2.0 , 2.0 ,  0.]]
""" The list of Q points at which calculation is made is done of N segments. The redStarts
variable contains a list of N starting points in  reciprocal space reduced units.
Each segment i starts at  redStarts[i] and ends at redEnds[i].  Within each segment
the spacing between points is the segment lenght divided by Nqlines[i]
"""
redEnds  =[[2.0 , 2.0 , 0.],  [2.0 , 2.0 , 1.0]] 
""" see redStarts
"""
Nqlines=[200, 100]
""" see redStarts
"""

Nfour_interp=5
"""  The number of points in each direction of the 3D reciprocal grid.
     The hdf5 file must contain a previous treatement done with the same parameter.
"""
Temperature=100
""" 
The hdf5 file must include DW factors calculated ALSO at this temperature.
 The temperature at which DW factors have been calculated.
   Units are Kelvin
"""
resolutionfile=""
""" a two column file : first the energy in cm-1, second the value of resolution function
"""

Lambda=1.0
""" For X-ray scattering : the wavelenght in Angstroems.
"""

Saturation=0
""" To limit intensity at peaks : intensity is saturated at this value
"""

lowerLimit=0
""" To correct the dynamical range, when displaying, the intensity is clipped to prevent it going below this value
"""

bottom_meV=0
""" Calculated eigenvalues are clipped to this value before use.
"""

tth_max=45.0
""" When searching for maximum contrast angles, the maximum 2theta angle achievable on the beamline.
"""


NEUTRONCALC=0
""" This activate neutron scattering calculations.
"""
CohB=None
""" When neutroncalc is on, this must be a dictionary : for each atom name the Coherent lenght.
"""
NeutronE=0
""" When neutroncalc is on, the neutron energy in meV
"""
branchWeight=None
""" a list of weights : one per branch.
"""

Eigscal=0
"""
EigScal==1   Intensity are calculated WITH only eigenvector scalar products ; ==2 Intensity are calculated WITH only eigenvector scalar products PLUS Mass factor
"""

UniqueIon=-1
""" If >=0. Selects one ion. All the other will be silent.
"""
red4castep = 'q-vals.dat'
""" Write q-values to file
"""


contrast_width=0
"""
  contrast width
"""

if(sys.argv[0][-12:]!="sphinx-build"):
  s=open(sys.argv[2], "r").read()
  exec(s)


def main():


  if "EigScal" in dir():
      print( "EigScal  option has been set , in input" )
  else:
      EigScal=0
  if EigScal==1:
    print( " Intensity are calculated WITH only eigenvector scalar products ")
  elif   EigScal==2:
      print( " Intensity are calculated WITH only eigenvector scalar products PLUS Mass factor")
  else:
    print( " Intensity are calculated WITH  more than just   eigenvector scalar products" )
  if branchWeight is not None:
     print( " CONSIDERING WEIGTHED BRANCHES AS GIVEN BY  branchWeight = ", branchWeight)



  CMGRAY=cm.gray
  if "COLOR" in globals():
    if COLOR:
      CMGRAY=None



  redLine=[]
  tickpos=[]
  ticklabel=[]
  directions=[]

  count=0
  for redStart, redEnd, Nqline in zip(     redStarts, redEnds, Nqlines     ):
    count=count+1

    if Nqline<1:
      raise Exception( "Nqline<1")

    tickpos.append(len(redLine))
    ticklabel.append(str(redStart ) )

    if count==len(Nqlines):
      npts=Nqline+1
    else:
      npts=Nqline

    addLine = numpy.array(redStart)+(((numpy.array(redEnd)-numpy.array(redStart) )))*(( numpy.arange(npts)*1.0/(Nqline))[:,None])

    addDirections = [  (numpy.array(redEnd)-numpy.array(redStart))/100.0   ]*len(addLine) 
    redLine.extend( addLine )
    directions.extend(addDirections)

  tickpos.append(len(redLine))
  ticklabel.append(str(redEnd) )
  tickpos=numpy.array(tickpos, numpy.float32)/(len( redLine))


  redLine=numpy.array(redLine)
  directions=numpy.array(directions)

  resolution=[]
  file=open(resolutionfile, "r")
  for line in file:
    toks = list_map(float, string_split(line))
    if len(toks)==2:
      resolution.append(toks)

  resolution=numpy.array(resolution)

  ########################################################
  # 
  # Q=2*sin(theta)*2*Pi/lambda
  def ThetaCheck(Q, Lambda , tth_max ):
    dum = numpy.sqrt( numpy.sum(Q*Q, axis=0)) * Lambda/2.0/math.pi/2.0
    if dum<=1.0:
      res     =  numpy.arcsin(  dum )
      if( abs(res)*180/np.pi < tth_max/2 ):
        return numpy.arcsin(  dum )
      else:
        return None
    else:
      return None

  def Theta(Q, Lambda  ):
    dum = numpy.sqrt( numpy.sum(Q*Q, axis=1)) * Lambda/2.0/math.pi/2.0
    return numpy.arcsin( dum  )


  #########################################################################

  calculatedDatas = TDS_Reading.CalcDatas.get_CalcDatas_Object_From_File(sys.argv[1])


  if "UniqueIon" in dir():
      N_ions=len(calculatedDatas.atomNames)   
      UniqueFilter = numpy.zeros( N_ions  , dtype= numpy.float32 )
      UniqueFilter[UniqueIon]=1.0
  else:
      UniqueIon=-1



  md5postfix= calculatedDatas.Get_md5postfix()
  filename = calculatedDatas.Get_filename()

  cella=TDS_Simmetry.OP_cella(cellvectors=calculatedDatas.cellVects ,
                 AtomNames_long=calculatedDatas.atomNames,
                 PositionsList_flat=calculatedDatas.atomAbsolutePositions )

  simmetries_dict = TDS_Simmetry.FindSimmetries(cella,   filename= filename,
                                                md5postfix=md5postfix, overwrite= False,
                                                key = "simmetries_dict" )


  print( "GOING TO RETRIEVE FOURIER TRASFORM  Nfour_interp = ", Nfour_interp)

  Replica = TDS_Simmetry.GetAllReplicated (  calculatedDatas, simmetries_dict,   filename= filename,
                                             md5postfix=md5postfix,
                                             overwrite= False ,
                                             key = "Codes_"+(str(APPLYTIMEREVERSAL)) ,
                                             APPLYTIMEREVERSAL=APPLYTIMEREVERSAL,
                                             CALCULATEFOURIER =1 ,
                                             CALCULATECOMPLEMENT=0,
                                             Nfour_interp=Nfour_interp,
                                             MAKING=0,
                                             MAKINGFOURIER=0
                                       )

  #################################################################################3
  ## 
  ##         A DISPERSION CURVE
  ##

  BV=calculatedDatas.GetBrillVects()
  qsLine =  calculatedDatas.GetClippedQs(QsReduced=redLine)
  directions = calculatedDatas.GetClippedQs(QsReduced=directions)

  qsLine_notShifted =   numpy.dot( redLine ,  BV  )



  if "redq4castep" in dir():
    f=open(redq4castep, "w")
    for redq in  redLine:
      f.write(" %e %e %e \n"%(redq[0] ,redq[1] ,redq[2] , ))
    f=None



  if NEUTRONCALC:
      scatterers = numpy.array([  CohB[aname] for aname in  calculatedDatas.atomNames ] , "F" )   
  else:
      tf0 = dabax.Dabax_f0_Table("f0_WaasKirf.dat")
      lambdas= [Lambda]*len(qsLine_notShifted)
      scatterers = [  tf0.Element(aname  ) for aname in  calculatedDatas.atomNames ] 
      scattFactors_site_q =numpy.array( [ scatterer.f0Lambda(numpy.array(lambdas), Theta(qsLine_notShifted, Lambda  )   )
                                      for scatterer in scatterers ]  ).astype(numpy.float32)

      scattFactors_q_site= scattFactors_site_q.T




  file=open("alongthelineF.dat", "w")
  Nbranches=3*len(calculatedDatas.atomNames)

  kinematicalFactors      =  numpy.zeros( [  len(qsLine_notShifted),  Nbranches//3  ] , numpy.complex64)
  kinematicalFactors.imag = numpy.tensordot(   qsLine_notShifted,
                                            -calculatedDatas.atomAbsolutePositions  ,  # the minus sign is here
                                            [[1], [ 1 ]] ) 
  kinematicalFactors = numpy.exp( kinematicalFactors )

  massfactors  = numpy.sqrt(1.0/ calculatedDatas.atomMasses )


  h5=h5py.File(filename+"."+md5postfix, "r")
  DWs_3X3 = h5["Codes_"+(str(APPLYTIMEREVERSAL))]["DWs"][str(Temperature)]["DWf_33"][:]


  dwfacts_perq = numpy.tensordot(  qsLine_notShifted,DWs_3X3    ,  axes=[[1], [ 1]])

  dwfacts_perq  = dwfacts_perq  *qsLine_notShifted[:,None,:]
  dwfacts_perq  = numpy.exp(-numpy.sum(dwfacts_perq , axis=2 ))


  if EigScal==2 :
      dwfacts_mass_kin_scatt_perq  = massfactors*kinematicalFactors
  elif EigScal==1:
      dwfacts_mass_kin_scatt_perq  = kinematicalFactors
  else:
      if NEUTRONCALC:
          dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*  scatterers  
      else:
          dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*scattFactors_q_site

  if UniqueIon !=-1:
      dwfacts_mass_kin_scatt_perq  = dwfacts_mass_kin_scatt_perq  *UniqueFilter




  # cambia temperatura in Hartree
  Temperature_Hartree=Temperature /11604.50520/27.211396132

  # amu = 1822.8897312974866 electrons
  # cm-1 ==> Hartree  cm-1 =  4.5563723521675276e-06 Hartree
  # Bohr = 0.529177249 Angstroems
  factor_forcoth = 4.5563723521675276e-06*1.0/(2.0*Temperature_Hartree)

  resevals=[]
  resstokes=[]

  stime=time.time()

  # for q in qsLine:
  #   print( q)
  #   print( calculatedDatas.GetNonAnalytic(   q   ))

  DMs = [  TDS_Simmetry.GetDM_fromF(q,  Replica["Four_Rvects"],   Replica["Four_DMs"] ,  calculatedDatas, direction=direction ) for q, direction  in   zip(qsLine, directions)  ]

  print( " CREATED DMS in " , time.time()-stime, " seconds ")
  stime=time.time()

  for (redQ, q , qnotshifted, 
       dwfacts_mass_kin_scatt,
       dm) in zip(redLine,
                  qsLine,
                  qsLine_notShifted,
                  dwfacts_mass_kin_scatt_perq,
                  DMs
                  ):

    # dm = TDS_Simmetry.GetDM_fromF(q,  Replica["Four_Rvects"],   Replica["Four_DMs"]  )
    evals, evects = numpy.linalg.eigh(dm)

    evals=numpy.sqrt(numpy.maximum(evals, numpy.array([1.0e-16], numpy.float32) ))
    evals=(numpy.maximum(evals, numpy.array([ bottom_meV/(0.0001239852 *1000) ], numpy.float32) ))


    qvect_s = numpy.tensordot(  qnotshifted ,      evects.reshape([-1,3, Nbranches]) , [[0], [1]   ]  )

    Fjs = numpy.sum(dwfacts_mass_kin_scatt* (qvect_s.T), axis=-1)  # sommato sugli ioni


    exp_plus =  numpy.exp( factor_forcoth * evals)
    exp_minus = numpy.exp(-factor_forcoth * evals)

    intensity_array = ( Fjs*(Fjs.conjugate())).real

    deno = (exp_plus-exp_minus)*evals


    if NEUTRONCALC:
        kinstokes = numpy.sqrt(numpy.maximum( (NeutronE-evals*(0.0001239852 *1000))/NeutronE, 0.0 ))
        kinantistk= numpy.sqrt((NeutronE+evals*(0.0001239852 *1000))/NeutronE)
    else:
        kinstokes = 1.0
        kinantistk= 1.0


    if EigScal==0:
      intensity_stokes     =       intensity_array*      exp_plus/deno    *kinstokes
      intensity_antistokes =       intensity_array*      exp_minus/deno   *kinantistk
    else:
      intensity_stokes     =       intensity_array   *kinstokes
      intensity_antistokes =       intensity_array   *kinantistk

    if branchWeight is not None:
      intensity_stokes     =   intensity_stokes     *numpy.array(branchWeight) *kinstokes
      intensity_antistokes =    intensity_stokes    *numpy.array(branchWeight) *kinantistk

    freqmev = evals*0.0001239852 *1000
    mask = numpy.less(0.0, freqmev)

    towrite=  numpy.array([freqmev,  intensity_stokes*mask ,  intensity_antistokes*mask  ])


    resevals.append(numpy.concatenate( [-evals*0.0001239852 *1000,  evals*0.0001239852 *1000] ) )
    resstokes.append( numpy.concatenate( [   intensity_antistokes*mask , intensity_stokes*mask  ] )   )

    towrite=numpy.reshape(towrite.T, [-1])


    file.write("%e %e %e " % tuple(redQ.tolist())  )
    file.write((("%e "*(len(towrite)) )+"\n") % tuple( towrite.tolist()
        )  
               )
    # file.write((("%e "*len(dm) )+"\n") % tuple(dm[0].real.tolist())  )

  print( " Calculated intensities  in " , time.time()-stime, " seconds ")

  file=None

  resevals=numpy.array(resevals)

  evalsWmin=numpy.amin(resevals)
  evalsWmax=numpy.amax(resevals)
  step = (resolution[-1,0]-resolution[0,0])/len(resolution)
  Wmin = evalsWmin+resolution[0,0]
  Wmax = evalsWmax+resolution[-1,0]

  print( " MIN MAX , " , numpy.amin(resevals) ,  numpy.amax(resevals))
  Ws=numpy.arange(Wmin,Wmax+step*0.9, step )
  Wmax = Ws[-1]

  res = numpy.zeros( [len(redLine), len(Ws)], numpy.float32  )
  minres = numpy.min( resolution[:,1] )
  reso=numpy.interp( Ws ,resolution[:,0] ,resolution[:,1] , left=minres, right= minres)

  reso[Ws<resolution[0,0]] = minres*(resolution[0,0]/Ws[ Ws<resolution[0,0]])**2
  reso[Ws> resolution[-1,0]] = minres*(resolution[-1,0]/Ws[ Ws>resolution[-1,0]])**2



  print( " ")
  print( "minres est ", minres)
  file=open("reso.dat", "w")
  for om,da in zip(Ws, reso):
    file.write("%e %e \n"%(om,da ))

  print( " QUI " , reso.shape)

  resofft=numpy.fft.fft(reso)
  freqs = numpy.fft.fftfreq (len(Ws),step)*2*math.pi


  for (redQ, ws , intens , target ) in zip(redLine,resevals, resstokes, res ):
    facts = numpy.sum( numpy.exp ( -1.0j* freqs[:,None]*  ws    ) * intens , axis = 1  )

    target[:]=numpy.fft.ifft(  facts*resofft  ).real

  #
  res=res.T

  # la res, plottee ca commence par le haut  
  res=res[::-1]


  fig=figure()
  axes=fig.add_subplot(111)



  log10=math.log(10.0)

  edfname = "dispimage.edf" 
  if os.path.exists(edfname):
    os.remove(edfname)

  edf= EdfFile.EdfFile(edfname)
  edf.WriteImage({}, res)
  edf=None


  ima=axes.imshow(numpy.log(    numpy.maximum( numpy.minimum(numpy.maximum(res,1.0e-14),Saturation ), lowerLimit)    )  /log10    ,cmap=CMGRAY)


  n_pix_y, n_pix_x=  res.shape

  step_x = 1.0/n_pix_x
  step_y = (Wmax-Wmin)/(n_pix_y-1)

  LIMITS = [0-0.5*step_x,1.+0.5*step_x, Wmin -0.5*step_y,  Wmax + 0.5*step_y] 
  
  ima.set_extent(LIMITS)

  #  fig.colorbar(ima)

  axes.set_aspect('auto')


  axes.xaxis.set_ticks(numpy.array(tickpos) )


  axes.xaxis.set_ticklabels(ticklabel )

  class event_manager:
    def __init__(self, fig, log_lin):
      self.fig=fig
      self.log_lin=log_lin
      self.last_line=[None,None, None,None]
      self.inSelection=False
      self.Cqred=None
      self.CnbA = None
      self.CnbB = None
      self.Clast_line=[None,None, None,None]
      self.pinredline = None
    def onpress(self, event ):

      print( " pressed key" , event.key)
      if event.key=='C':
        self.inSelection=not  self.inSelection
      if event.key in ['M'] and   (self.Cqred is not None) and (self.CnbA is not None) and  ( (self.CnbB is not None) or (contrast_width>0)):
        print( " MASSIMIZZO CONTRASTO " )
        CalculateContrast(self.Cqred,self.CnbA,self.CnbB)

        show_contrast()
        

      if event.key in ['m'] :
        (fd, filename) = tempfile.mkstemp()
        try:
          tfile = os.fdopen(fd, "w")
          tfile.write("Cqred=numpy.%s\nCnbA=%s\nCnbB=%s\n"%(repr(self.Cqred) ,self.CnbA,self.CnbB ))
          tfile.close()
          subprocess.Popen(["emacs", filename]).wait() 
          s=open(filename,"r").read()
        finally:
          dic={}
          exec(s ,globals(),dic)
          self.Cqred ,self.CnbA,self.CnbB= dic["Cqred"], dic["CnbA"], dic["CnbB"]
          os.remove(filename)
          self.plot_lines( doA=1, doB=1)




    def plot_lines(self, doA=0, doB=0):
      if self.Clast_line[3] is None :
        self.Clast_line[3]=ax=self.fig.add_subplot(111, facecolor="none")
      else:
        ax = self.Clast_line[3]

      if doA:
        if self.Clast_line[0] is not None:
          self.Clast_line[0].remove( )
        print( " PLOTTO ")
        self.Clast_line[0] = matplotlib.lines.Line2D(numpy.linspace(0.0,1.0,len(resevals)),
                                                       resevals[:,self.CnbA] , linewidth=2.0, color="b")
        ax.add_line( self.Clast_line[0] )


        ax.set_xlim(LIMITS[:2])
        ax.set_ylim(LIMITS[2:4])
        
        ax.autoscale_view(tight=True)

        self.fig.tight_layout()
        
        plt.draw()

        
        
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

        plt.subplots_adjust(left=0.0, right=1.0, top=1.0, bottom=0.0)

        
        self.fig.show()
 

      if doB:
        if self.Clast_line[1] is not None:
          self.Clast_line[1].remove( )

        self.Clast_line[1] = matplotlib.lines.Line2D(numpy.linspace(0.0,1.0,len(resevals)),
                                                       resevals[:,self.CnbB], linewidth=2.0 , color="w")
        ax.add_line( self.Clast_line[1] )

        self.fig.tight_layout()

        
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

        self.fig.show()





    def onclick(self, event ):
      print(" in onclick")
      if self.inSelection:
        print( " inSelection " , event.xdata, event.ydata,  event.x, event.y)
        x0 = event.xdata
        y0 = event.ydata
        x = event.x
        y = event.y

        pinredLine = int( 0.5  +  (len(redLine)-1)*x0)
        if pinredLine>=0 and pinredLine<len(redLine):
          self.pinredline =pinredLine
          evals=resevals[pinredLine]
          imin=numpy.argmin(numpy.abs(evals-y0))

          self.plot_lines( doA=0, doB=0)

          if event.button == 1: 
            self.Cqred = redLine[pinredLine]
            self.CnbA  = imin

            self.plot_lines( doA=1, doB=0)


          if event.button == 3: #right click

            if contrast_width==0:
              self.CnbB  = imin
              self.plot_lines( doA=0, doB=1)
            else:
              print( " other branches are selected within contrast_width range ")

          print( " SELECTED BRANCHES A and B " , self.CnbA,self.CnbB, "  AT  " ,  self.Cqred)

        return

      if event.button in [1,2] and self.last_line[2] is not None:
        data=self.last_line[2]
        if event.button == 1:
          data[:]=data/1.5
        else:
          data[:]=data*1.5

        self.last_line[1].remove( )

        self.last_line[1] = matplotlib.lines.Line2D( data  ,Ws[::-1] , color="r")

        if self.last_line[3] is None :
          self.last_line[3]=ax=self.fig.add_subplot(111, facecolor="none")

          ax.set_xlim(LIMITS[:2])
          ax.set_ylim(LIMITS[2:4])

          
        else:
          ax = self.last_line[3]

        ax.add_line( self.last_line[1] )
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

        self.fig.show()

      if event.button == 3:         
        x0 = event.xdata
        y0 = event.ydata
        x = event.x
        y = event.y

        pinredLine = int( 0.5  +  (len(redLine)-1)*x0)
        if pinredLine>=0 and pinredLine<len(redLine):

          print( redLine[pinredLine])
          if self.last_line[0] is not None:
            self.last_line[0].remove( )
            self.last_line[1].remove( )

          if self.last_line[3] is None :
            self.last_line[3]=ax=self.fig.add_subplot(111, facecolor="none")
            ax.set_xlim(LIMITS[:2])
            ax.set_ylim(LIMITS[2:4])
          else:
            ax = self.last_line[3]

          self.last_line[0] = matplotlib.lines.Line2D([0.0+x0,0.0+x0], [Wmin  ,Wmax   ], color="r")

          if(self.log_lin=="lin"):
            self.last_line[2] = res[:,pinredLine]/numpy.max(res[:,pinredLine])

            f=open("click.dat", "w")
            for x,y in zip(Ws[::-1],res[:,pinredLine] ):
              f.write("%e %e \n"%(x,y   ))
            f=None
          else:
            fmin= numpy.log(numpy.min(res[:,pinredLine]) )
            fmax= numpy.log(numpy.max(res[:,pinredLine]))

            self.last_line[2] = (numpy.log( res[:,pinredLine]) - fmin ) /(fmax-fmin)

          print( "NORMALIZATION TO ", numpy.max(res[:,pinredLine]))
          print( "MIN   ", numpy.min(res[:,pinredLine]))


          self.last_line[1] = matplotlib.lines.Line2D(  self.last_line[2]  ,Ws[::-1] , color="r")
          #self.last_line[1] = matplotlib.lines.Line2D( [1.0,0.0] ,[ 0 ,Wmax   ], color="r")


          ax.add_line( self.last_line[0] )
          ax.add_line( self.last_line[1] )
          self.fig.canvas.draw()
          self.fig.canvas.flush_events()

          self.fig.show()


  ##########################################################################


  def CalculateContrast(CredQ,CnA,CnB):
  #################################################################################3
  ## 
  ##         A DISPERSION CURVE
  ##

    BV=calculatedDatas.GetBrillVects()
    Credline =  [ ]
    thetas=[]
    for i in range(-10,11):
      for j in range(-10,11) :
        for k in range(-10,11)   :
          dumRedQ = CredQ+numpy.array([i,j,k])
          dumQ  = numpy.dot( dumRedQ ,  BV  )
          dumTheta= ThetaCheck(dumQ, Lambda, tth_max)
          if dumTheta is not None:
            Credline.append(dumRedQ)
            thetas.append(dumTheta*180.0/math.pi )

    CqsLine =  calculatedDatas.GetClippedQs(QsReduced=Credline)
    CqsLine_notShifted =   numpy.dot( Credline ,  BV  )

    if NEUTRONCALC:
      scatterers = numpy.array([  CohB[aname] for aname in  calculatedDatas.atomNames ] , "F" )   
    else:
      tf0 = dabax.Dabax_f0_Table("f0_WaasKirf.dat")
      lambdas= [Lambda]*len(CqsLine_notShifted)
      scatterers = [  tf0.Element(aname  ) for aname in  calculatedDatas.atomNames ] 
      scattFactors_site_q =numpy.array( [ scatterer.f0Lambda(numpy.array(lambdas), Theta(CqsLine_notShifted, Lambda  )   )
                                          for scatterer in scatterers ]  ).astype(numpy.float32)
      scattFactors_q_site= scattFactors_site_q.T

    totNbranches=3*len(calculatedDatas.atomNames)

    if CnA>= totNbranches:
      CnA = CnA-totNbranches
      stokA=1
#    else:
#      CnA = (totNbranches-1)-CnA
#      stokA=0
    else:
      CnA = CnA
      stokA=0



    if contrast_width==0:
      if CnB>= totNbranches:
        CnB = CnB-totNbranches
        stokB=1
      else:
        CnB = CnB
        stokB=0
    else:
      stokB=stokA

    print( " VADO A LEGGERE HD5")


    Natoms = len(calculatedDatas.atomNames )
    kinematicalFactors      =  numpy.zeros( [  len(CqsLine_notShifted),  Natoms  ] , numpy.complex64)
    kinematicalFactors.imag = numpy.tensordot(   CqsLine_notShifted,
                                                 -calculatedDatas.atomAbsolutePositions  ,  # the minus sign is here
                                                 [[1], [ 1 ]] ) 
    kinematicalFactors = numpy.exp( kinematicalFactors )
    massfactors  = numpy.sqrt(1.0/ calculatedDatas.atomMasses )

    h5=h5py.File(filename+"."+md5postfix, "r")
    DWs_3X3 = h5["Codes_"+(str(APPLYTIMEREVERSAL))]["DWs"][str(Temperature)]["DWf_33"][:]

    dwfacts_perq = numpy.tensordot(  CqsLine_notShifted,DWs_3X3    ,  axes=[[1], [ 1]])

    dwfacts_perq  = dwfacts_perq  *CqsLine_notShifted[:,None,:]
    dwfacts_perq  = numpy.exp(-numpy.sum(dwfacts_perq , axis=2 ))


    print( "CALCOLO QUALCHE FATTORE ")
    if EigScal==2 :
      dwfacts_mass_kin_scatt_perq  = massfactors*kinematicalFactors
    elif EigScal==1:
      dwfacts_mass_kin_scatt_perq  = kinematicalFactors
    else:
      if NEUTRONCALC:
        dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*  scatterers  
      else:
        dwfacts_mass_kin_scatt_perq  = dwfacts_perq*massfactors*kinematicalFactors*scattFactors_q_site

    if UniqueIon !=-1:
      dwfacts_mass_kin_scatt_perq  = dwfacts_mass_kin_scatt_perq  *UniqueFilter

      # cambia temperatura in Hartree
    Temperature_Hartree=Temperature /11604.50520/27.211396132

    # amu = 1822.8897312974866 electrons
    # cm-1 ==> Hartree  cm-1 =  4.5563723521675276e-06 Hartree
    # Bohr = 0.529177249 Angstroems
    factor_forcoth = 4.5563723521675276e-06*1.0/(2.0*Temperature_Hartree)

    resevals=[]
    resstokes=[]

    stime=time.time()

    print( " ESTRAGGO DM ED EVALS EVECTS " )


    ## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    dm = TDS_Simmetry.GetDM_fromF(CqsLine[0],Replica["Four_Rvects"],Replica["Four_DMs"],calculatedDatas )
    evals, evects = numpy.linalg.eigh(dm)   


    SQRTevals=numpy.sqrt(numpy.maximum(evals, numpy.array([1.0e-16], numpy.float32) ))*0.0001239852 *1000


    if contrast_width==0: 
      evals=numpy.array([evals[CnA], evals[CnB]])
      evects =  numpy.array([evects[:,CnA], evects[:,CnB]])
      Nwindow=1
    else:
      mask = np.less(  abs( SQRTevals[CnA] -SQRTevals ), contrast_width/2) * np.less( 0,  abs( SQRTevals[CnA] -SQRTevals ))
      evals=numpy.array([evals[CnA]]+ [ tok for tok in evals[mask]])

      evects =  numpy.concatenate([     evects[:,CnA:CnA+1] , evects[:,mask]  ], axis=1 )
      Nwindow = (mask*1).sum()


    Nbranches=1+Nwindow

    evals=numpy.sqrt(numpy.maximum(evals, numpy.array([1.0e-16], numpy.float32) ))

    print( " CHIAMO NUOVI PUNTI " )
    for (redQ, q , qnotshifted, 
         dwfacts_mass_kin_scatt) in zip(Credline,
                    CqsLine,
                    CqsLine_notShifted,
                    dwfacts_mass_kin_scatt_perq
                    ):

      qvect_s = numpy.tensordot(  qnotshifted ,      evects.T.reshape([-1,3, Nbranches]) , [[0], [1]   ]  )

      Fjs = numpy.sum(dwfacts_mass_kin_scatt* (qvect_s.T), axis=-1)  # sommato sugli ioni

      exp_plus =  numpy.exp( factor_forcoth * evals)
      exp_minus = numpy.exp(-factor_forcoth * evals)

      intensity_array = ( Fjs*(Fjs.conjugate())).real

      deno = (exp_plus-exp_minus)*evals

      if NEUTRONCALC:
        kinstokes = numpy.sqrt(numpy.maximum( (NeutronE-evals*(0.0001239852 *1000))/NeutronE, 0.0 ))
        kinantistk= numpy.sqrt((NeutronE+evals*(0.0001239852 *1000))/NeutronE)
      else:
        kinstokes = 1.0
        kinantistk= 1.0


      if EigScal==0:
        intensity_stokes     =       intensity_array*      exp_plus/deno    *kinstokes
        intensity_antistokes =       intensity_array*      exp_minus/deno   *kinantistk
      else:
        intensity_stokes     =       intensity_array   *kinstokes
        intensity_antistokes =       intensity_array   *kinantistk

      if branchWeight is not None:
        intensity_stokes     =   intensity_stokes     *numpy.array(branchWeight) *kinstokes
        intensity_antistokes =    intensity_stokes    *numpy.array(branchWeight) *kinantistk

      freqmev = evals*0.0001239852 *1000
      mask = numpy.less(0.0, freqmev)

      towrite=  numpy.array([freqmev,  intensity_stokes*mask ,  intensity_antistokes*mask  ])

      resevals.append(numpy.concatenate( [-evals*0.0001239852 *1000,  evals*0.0001239852 *1000] ) )
      resstokes.append( numpy.concatenate( [   intensity_antistokes*mask , intensity_stokes*mask  ] )   )

    print( " Calculated intensities  in " , time.time()-stime, " seconds ")
    l=[]
    l_A=[]
    for redQ,theta,resE,resV in zip(Credline,thetas,resevals,resstokes):
      if(stokA ):
        AAe = resE[1+Nwindow]
        AAi = resV[1+Nwindow]
      else:
        AAe = resE[0]
        AAi = resV[0]
      if(stokB ):
        BBe = resE[2+Nwindow:].sum()/Nwindow
        BBi = resV[2+Nwindow:].sum()/Nwindow
      else:

        BBe = (resE[1:1+Nwindow]).sum()/Nwindow
        BBi = (resV[1:1+Nwindow]).sum()/Nwindow


      l.append( [AAi/BBi, AAi,BBi ] +  (redQ-CredQ).tolist() +[theta] )
      l_A.append( [ AAi, AAi/BBi,BBi ] +  (redQ-CredQ).tolist() +[theta] )

    l.sort()
    l.reverse()
    l_A.sort()
    l_A.reverse()
 
    ca,cb,cc=CredQ

    file=open("choosen_modes_contrast.txt","w")
    file.write("#CreQ = %s   CnA = %s stokA=%d  CnB = %s stokB=%d  ENERGIES ==>   Ea=%e   Eb=%e  \n"%(str(CredQ),CnA,stokA,CnB,stokB, AAe, BBe ))
    for contr, AAi,BBi, sa,sb,sc , theta in l:
      file.write(" %e %e %e %e %e %e %e\n"% (sa+ca  ,sb+cb ,sc+cc , 2*theta, AAi,BBi,contr ))
    
    file=open("choosen_modes_intensity.txt","w")
    file.write("#CreQ = %s  CnA = %s  stokA=%d  CnB = %s stokB=%d  ENERGIES ==>   Ea=%e   Eb=%e  \n"%(str(CredQ),CnA,stokA,CnB,stokB, AAe, BBe ))
    for AAi, contr, BBi, sa,sb,sc , theta in l_A:
      file.write(" %e %e %e %e %e %e %e\n"% (sa+ca  ,sb+cb ,sc+cc , 2*theta, AAi,BBi,contr ))

  #########################################################################

  dumlog=event_manager(fig, "log")

  cid = fig.canvas.mpl_connect('button_press_event', dumlog.onclick)
  fig.canvas.mpl_connect('key_press_event',dumlog.onpress)

  fig2=figure()
  axes=fig2.add_subplot(111)


  
  cumulated = res.sum(axis=1)/res.shape[1]
  np.savetxt('summedoverQs.dat', np.column_stack([Ws, cumulated])[::-1,:] , fmt='%20.10e', delimiter=' ')
  
  ima=axes.imshow(numpy.maximum( numpy.minimum(res,Saturation ), lowerLimit) ,cmap=CMGRAY)
  # fig2.colorbar(ima)
  ima.set_extent([0-0.5*step_x,1.+0.5*step_x, Wmin -0.5*step_y,  Wmax + 0.5*step_y] )
  axes.xaxis.set_ticks(tickpos )
  axes.xaxis.set_ticklabels(ticklabel )

  axes.set_aspect('auto')

  dum=event_manager(fig2, "lin")
  cid2 = fig2.canvas.mpl_connect('button_press_event', dum.onclick)
  fig2.canvas.mpl_connect('key_press_event', dum.onpress)

  data4plot = numpy.sum(numpy.maximum( numpy.minimum(res,Saturation ), lowerLimit),axis=0)
  fig3=figure()
  axes=fig3.add_subplot(111)

  plt.plot(numpy.arange(len(data4plot)), data4plot )

  axes.xaxis.set_ticks(numpy.array(tickpos)*(len(data4plot)-1 ) )
  axes.xaxis.set_ticklabels(ticklabel )

  np.savetxt('alongtheline_TDS.dat', np.column_stack([numpy.arange(len(data4plot)), data4plot]) , fmt='%12.5f', delimiter=' ')

  # ylabel('Tot Tds AU ')

  show()
  fig.savefig("pippo.png")




