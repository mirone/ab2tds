import sys
import numpy
import string
import time
import math

def ciclicity(a,b,c):
  if( a==b or  a==c or c==b):
    return 0
  elif(  (b-a)*(c-b)*(a-c) <0  ):
    return 1
  else:
    return -1  

def CellVolume( cellVects ):
  AntiSymm= numpy.array([ [ [ ciclicity(i,j,k) for k in range(0,3) ] for j in range (0,3) ] for i in range(0,3) ])
  cellVolume=numpy.dot( numpy.dot(AntiSymm, cellVects[2]),cellVects[1] )
  cellVolume=numpy.dot(cellVects[0],cellVolume)
  return cellVolume

def BrillVects(cellVects):
  AntiSymm= numpy.array([ [ [ ciclicity(i,j,k) for k in range(0,3) ] for j in range (0,3) ] for i in range(0,3) ])
  cellVolume= CellVolume(cellVects)
  Brillvectors=numpy.zeros([3,3], numpy.float32)
  Brillvectors[0]=2 * numpy.pi * numpy.dot( numpy.dot(AntiSymm, cellVects[2]),cellVects[1] )/ cellVolume
  Brillvectors[1]=2 * numpy.pi * numpy.dot( numpy.dot(AntiSymm, cellVects[0]),cellVects[2] )/ cellVolume
  Brillvectors[2]=2 * numpy.pi * numpy.dot( numpy.dot(AntiSymm, cellVects[1]),cellVects[0] )/ cellVolume
  return Brillvectors

def GetTensor(MassTot,Volume ):
  A11 = 156
  A33 = 87.22
  A44 = 34.57
  A66 = 47.9
  A15 = 21.57
  A13 = 57.91
  
  C = numpy.zeros([6,6],"f")
  
  C[0,0] = A11 ##
  C[1,1] = A11
  C[2,2] = A33 ##
  C[0,2] = A13 ##
  C[1,2] = A13
  C[0,1] = A11-2*A66
  C[3,3] = A44 ##
  C[4,4] = A44
  C[5,5] = A66  ## 
  
  C[0,4] =  A15 ##
  C[1,4] = -A15 
  C[3,5] = -A15 
  
  for i in range(6):
    for j in range(i):
      C[i,j] = C[j,i]
      
  ## FACTORS

  #####################################################################
  ###  castep eigenval are given in cm-1
  ###  Below is the factor to change evals**2 to omega squared in seconds-2
  F1 = (2*math.pi   * 2.99792458e+10 )**2

  ######################################################
  ### The energy of an harmonic oscillator is omega**2*amplitude**2 /2
  ### below is 1.0/2
  F2=0.5

  ##############################################3
  ## This converts the mass au to grams
  ##
  F3 = 1.66053892e-24

  #######################################################
  ## 
  ##  in the fit the variations depends on Qs. Qs are in Angstroems-1
  ##  Below is the factor A->cm squared
  F4 = 1.0e-16
  
  #############################################################################
  ##
  ##  We have calculated for a unit cell and elastic tensor is energy 
  ##  over volume.  Below is the inverse of the cell volume  in cm-3
  F5 = 1.0/ Volume *1.0e+24
  
  ##############################################
  ##
  ## factor 1e-10 to get GPa
  F6 = 1.0e-10

  C=C/(F1*F2*F3*F4*F5*F6)
  
  return C


    
def ReadCastep(filename,  tipo="Castep"):


    if tipo=="Castep":

        
        s = open(filename,"r").read()
        stime=time.time()
        print "----- splitting the file into lines ", filename
        sl=string.split(s,"\n")
        s=None
        etime = time.time()
        print "        splitting Took ",etime - stime, "seconds"

        linecount=0

        while( "Number of ions" not in sl[linecount]):
            linecount+=1
        NofIons= string.atoi(string.split(sl[linecount])[-1])


        while( "Number of branches" not in sl[linecount]):
            linecount+=1
        NofBranches= string.atoi(string.split(sl[linecount])[-1])


        while( "Number of wavevectors" not in sl[linecount]):
            linecount+=1
        NofWaves= string.atoi(string.split(sl[linecount])[-1])


        print "\tNofIons\t\tNofBranches\tNofWaves"
        print "\t", NofIons, "\t\t",NofBranches, "\t\t",NofWaves


        while( "Unit cell vectors (A)" not in sl[linecount]):
            linecount+=1

        cellVects=[]
        for icellvect in range(3):
            linecount+=1
            cellVects.append(map(string.atof, string.split(sl[linecount])))

        Volume = CellVolume( cellVects )
        Brills = BrillVects( cellVects  )

        CTransf       = [ [0,1,-1],[1,-1,0],[1,1,1]  ]
        cellVects_simple = numpy.dot(CTransf, cellVects)
        Brills_simple= BrillVects(cellVects_simple)
        maxBrill = numpy.max(numpy.sqrt(    (Brills*Brills).sum(axis=-1) ))
           


        
        MassTot = 0.0

        Tensor = GetTensor(MassTot,Volume )
    
        cellVects=numpy.array(cellVects)
        print "\tCell vectors "
        for v in  cellVects:
            print "\t%e20.7  %e20.7 %e20.7"%tuple(v.tolist() )

        while( "Fractional Co-ordinates" not in sl[linecount]):
            linecount+=1

        atomReducedPositions=[]
        atomNames    =[]
        atomMasses   =[]

        for i  in range(NofIons):
            linecount+=1
            items=string.split(sl[linecount])
            atomReducedPositions.append(map(string.atof,  items[1:4]))
            atomNames.append(items[4])
            atomMasses.append(string.atof(items[5]))
            MassTot += atomMasses[-1]
        rho = MassTot/Volume
            
            
        atomMasses=numpy.array(atomMasses)   
        atomMassesSqrt=numpy.sqrt(atomMasses)   
        qs= numpy.zeros([NofWaves,3],"f")
        weights = numpy.zeros([NofWaves],"f")
        frequencies = numpy.zeros([NofWaves,NofBranches],"f")
        eigenvectors = numpy.zeros([NofWaves,NofBranches, NofBranches],"F")

        linecount+=1

        stime = time.time()
        print "Read  waves\t ", 0, "/", NofWaves,
        oldQn=-1
        while(oldQn<NofWaves-1): # CASTEP counting starts from 1
        #for iwave in range(NofWaves):
            iwave=oldQn
            if (iwave and iwave%100 ==0):
                etime = time.time()
                print "\rRead  waves\t ", iwave, "/", NofWaves, "in ", etime-stime, "seconds. Total is approx.",NofWaves*(etime-stime)/iwave, 
                sys.stdout.flush()

            linecount+=1

            items=string.split(sl[linecount], "=")
            items=string.split(items[1])
            newQn=string.atoi(items[0])-1 # CASTEP counting starts from 1
            if(newQn==oldQn):
                print " PRESENCE  QPOINTS WITH MULTEPLICITY>1 . LINE = ", sl[linecount]

            oldQn=newQn    
            qs[newQn]=map(string.atof, items[1:4])
            q = numpy.dot(qs[newQn], Brills)

            normaq = math.sqrt( (q*q).sum() )

            
            polX  =  numpy.array([  q[0],   0      ,    0   ,   0   ,  q[2]    ,   q[1]  ] )
            polY  =  numpy.array([     0 ,   q[1]  ,    0   ,  q[2],  0        ,   q[0]  ] )
            polZ  =  numpy.array([     0 ,   0      ,  q[2] ,  q[1],  q[0]    ,   0      ] )
 
            
            if numpy.sum( qs[newQn]*qs[newQn] )==0.0 :
                if(len(items)>8):
                    direction = numpy.array(map(string.atof, items[5:8]))

                    bvs = BrillVects(cellVects)

                    directionQ = numpy.dot(direction  , bvs )

                    direction = SMALLQ * direction   / numpy.sqrt( numpy.sum( directionQ*directionQ ) )
                    print " FOUND A Gamma POINT, replacing it with direction ", direction
                    qs[newQn]= direction/10
                else:
                    qs[newQn]= [0.0,0.0,1.0e-6]

            weights[newQn]=string.atof(items[4])

            mode2line={}
            
            for imode in range(NofBranches):
                linecount+=1
                items=string.split(sl[linecount])
                frequencies[newQn,imode] = string.atof(items[1])
                mode2line[imode] = linecount
            linecount+=2


            for imode in range(NofBranches):
                for idegree in range(0,NofBranches,3):
                    linecount+=1
                    items=map(string.atof,string.split(sl[linecount])[2:8])
                    eigenvectors[newQn,imode, idegree:idegree+3].real=items[0:8:2]
                    eigenvectors[newQn,imode, idegree:idegree+3].imag=items[1:8:2]

            for imode in range(3):

              
              # if frequencies[newQn,imode]<8.0:
              if normaq<0.2*maxBrill:
                vv = eigenvectors[newQn,imode, :]
                # print vv
                # print (vv*vv.conjugate()).sum()
                vv = numpy.reshape(vv, [  -1, 3 ] )
                # print vv
                vv = (vv.T / atomMassesSqrt ).T
                # print vv
                VV = vv.sum(axis=0)/len(vv)
                # print VV
                # print VV
                VV=VV.real
                gammas = VV[0]*polX +VV[1]*polY + VV[2]*polZ

                ene = numpy.dot( gammas, numpy.dot(Tensor, gammas.conjugate())  )
                newene = math.sqrt(ene.real/2)

                nlinea = mode2line[imode]
                
                linea = sl[nlinea]
                pos = linea.find(linea.split()[1] )
                newlinea = linea[:pos]+"%e "%newene
                sl[nlinea]=newlinea
                f=open("energie.txt","a")
                f.write("%e %e %e \n"%(math.sqrt((q*q).sum()) , frequencies[newQn,imode],newene ))
                f.close()
                # raise
                
        f=open("newcastep.phonon","w")
        
        for l in sl:
          f.write("%s\n"%l)
          
        f.close()
                

  
ReadCastep(sys.argv[1])
