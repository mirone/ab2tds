import os
import re
import subprocess
import sys
import glob

from setuptools import setup

eps = [
            f"{os.path.basename(script_name[:-3])}=ab2tds.{os.path.basename(script_name[:-3])}:main"
            for script_name in glob.glob("ab2tds/make*.py")
]

print (glob.glob('ab2tds/data/*.dat'))
distrib = setup(
    name="ab2tds",
    version="1.0",
    license = "GPL - Please read LICENSE.GPL for details",
    author="Alessandro Mirone & Bjorn Wehinger",
    author_email="mirone@esrf.fr",
    description =  "From ab initio to thermal diffuse scattering",    
    packages = ["ab2tds"],
    package_dir = {"ab2tds": "ab2tds", "ab2tds.PyMca": "PyMca"},
    package_data = {
        "ab2tds": ["data/*.dat"] ,
    },    
    entry_points = {
        'console_scripts': eps,
    },
)




















