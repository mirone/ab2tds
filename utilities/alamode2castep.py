#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Esempio di script che legge:
  1) un file di output ALAMODE (con i modi fononici, frequenze, eigenvectors),
  2) un file di log ANPHON (con info su cella, posizioni atomiche, k-points e pesi, ecc.)

e produce un file in formato CASTEP-like.
"""

import sys
import math

def parse_log_anphon(filename_log):
    """
    Legge dal file di log ANPHON (esempio: anphon_log.txt) le info:
      - Lattice vectors (primitive cell)
      - Number of atoms in primitive cell
      - Fractional coordinates e specie
      - Mass per specie
      - K-points e relative pesi (irred. o totali), se disponibili

    Restituisce un dizionario con le chiavi:
       {
         'lattice_prim': [[a1x,a1y,a1z],
                          [a2x,a2y,a2z],
                          [a3x,a3y,a3z]],
         'nat_prim': 4,
         'atoms': [
            { 'species': 'O',
              'xfrac': float,
              'yfrac': float,
              'zfrac': float },
            ...
         ],
         'masses': {
            'O': 15.999,
            'Cr': 51.9961,
            'Pd': 106.42,
         },
         'kpoints': [
            { 'kindex': 1,
              'kx': 0.0,
              'ky': 0.0,
              'kz': 0.0,
              'weight': 0.000244
            },
            ...
         ],
         'nk_total': 4096,  # se trovi la riga "Number of k points : 4096"
       }
    """
    data = {
        'lattice_prim': [],
        'nat_prim': 0,
        'atoms': [],
        'masses': {},
        'kpoints': [],
        'nk_total': 0,
    }

    with open(filename_log, 'r') as f:
        lines = f.readlines()

    idx = 0
    nlines = len(lines)

    # Piccoli "flag" per riconoscere se siamo dentro a un blocco
    in_prim_cell_block = False
    in_fractional_positions = False
    in_masses_block = False
    in_kpoints_list = False

    while idx < nlines:
        line = lines[idx].strip()

        # 1) Trova "Number of atoms in the primitive cell:"
        if "Number of atoms in the primitive cell:" in line:
            # Esempio: "Number of atoms in the primitive cell: 4"
            parts = line.split(':')
            # la parte a destra => " 4"
            nat_prim = int(parts[1].strip())
            data['nat_prim'] = nat_prim
            idx += 1
            continue

        # 2) Se compare " * Primitive cell"
        if '* Primitive cell' in line:
            # salta righe finché non trovi i 3 successivi
            # La riga dopo magari ha "a1 ..."
            # Esempio di righe (dal log):
            #  * Primitive cell 
            #
            #    2.783856e+00   1.607260e+00   1.129595e+01 : a1
            #   -2.783856e+00   1.607260e+00   1.129595e+01 : a2
            #    0.000000e+00  -3.214520e+00   1.129595e+01 : a3
            in_prim_cell_block = True
            idx += 1
            continue

        if in_prim_cell_block:
            # Cerchiamo 3 righe con " : a1", " : a2", " : a3"
            # e salviamo i vettori
            if (': a1' in line) or (': a2' in line) or (': a3' in line):
                parts_line = line.split(':')[0].split()
                # es. "2.783856e+00   1.607260e+00   1.129595e+01"
                vec = [float(v) for v in parts_line]
                data['lattice_prim'].append(vec)
                # Se abbiamo raccolto 3, usciamo
                if len(data['lattice_prim']) == 3:
                    in_prim_cell_block = False
            else:
                # se c'è riga vuota o altra, ignoriamo
                pass
            idx += 1
            continue

        # 3) Atomic positions in the primitive cell (fractional):
        if "Atomic positions in the primitive cell (fractional)" in line:
            # le righe seguenti di forma:
            #  1:   1.107343e-01   1.107343e-01   1.107343e-01   O
            in_fractional_positions = True
            idx += 1
            continue

        if in_fractional_positions:
            if line.strip() == "" or "Mass of atomic species" in line:
                # Fine blocco
                in_fractional_positions = False
                # Non incrementare idx qui, cosi la prossima iter vede "Mass"
            else:
                # parse la riga
                # es:  "1:   1.107343e-01   1.107343e-01   1.107343e-01   O"
                parts_line = line.split()
                # parts_line[0] => "1:"
                # parts_line[1] => "1.107343e-01"
                # parts_line[4] => "O" (specie)
                if len(parts_line) >= 5:
                    xfr = float(parts_line[1])
                    yfr = float(parts_line[2])
                    zfr = float(parts_line[3])
                    specie = parts_line[4]
                    data['atoms'].append({
                        'species': specie,
                        'xfrac': xfr,
                        'yfrac': yfr,
                        'zfrac': zfr
                    })
            idx += 1
            continue

        # 4) "Mass of atomic species (u):"
        if "Mass of atomic species (u):" in line:
            in_masses_block = True
            idx += 1
            continue

        if in_masses_block:
            # es.  " O:   15.999000"
            # se riga vuota => fine
            if line.strip() == "" or line.startswith('Symmetry') or ('=' in line):
                in_masses_block = False
            else:
                parts_line = line.split(':')
                if len(parts_line) == 2:
                    sp = parts_line[0].strip()
                    mass_val = float(parts_line[1])
                    data['masses'][sp] = mass_val
            idx += 1
            continue

        # 5) "Number of k points :" => nk_total
        if "Number of k points" in line:
            parts_line = line.split(':')
            nk_t = int(parts_line[1].strip())
            data['nk_total'] = nk_t
            idx += 1
            continue

        # 6) Riconosci se inizia la "List of irreducible k points"
        if "List of irreducible k points" in line:
            in_kpoints_list = True
            idx += 1
            continue

        if in_kpoints_list:
            # Se la riga è vuota o non ha coordinate, o comincia a parole "Symmetry" => stop
            if line.strip() == "" or "---" in line or "Analysis" in line:
                in_kpoints_list = False
            else:
                # es:  "1:   0.00000e+00   0.00000e+00   0.00000e+00   0.000244"
                # parse
                parts_line = line.split()
                # parts_line[0] => "1:"
                # parts_line[1] => "0.00000e+00"
                # parts_line[4] => "0.000244"
                if len(parts_line) >= 5:
                    kindex_str = parts_line[0].replace(':','')
                    try:
                        kindex = int(kindex_str)
                    except:
                        kindex = -1
                    kx = float(parts_line[1])
                    ky = float(parts_line[2])
                    kz = float(parts_line[3])
                    w  = float(parts_line[4])
                    data['kpoints'].append({
                        'kindex': kindex,
                        'kx': kx, 'ky': ky, 'kz': kz,
                        'weight': w
                    })
            idx += 1
            continue

        idx += 1

    return data


def parse_alamode_output(filename_alamode):
    """
    Legge il file di output ALAMODE (simile al tuo esempio).
    Restituisce un dizionario:
      {
        'n_modes': int,
        'n_kpoints': int,
        'kpoints_data': [
            {
              'k_index': ...,
              'q_vector': (qx, qy, qz),
              'modes': [
                  {
                    'mode_index': m,
                    'frequency': freq_cm1,  # da convertire se serve
                    'eigenvector': [ (rx,ix, ry,iy, rz,iz) per atomo ],
                  },
                  ...
              ]
            },
            ...
        ]
      }
    """
    data = {
        'n_modes': 0,
        'n_kpoints': 0,
        'kpoints_data': []
    }
    with open(filename_alamode, 'r') as f:
        lines = f.readlines()

    idx = 0
    nlines = len(lines)
    kpoints_data = []

    while idx < nlines:
        line = lines[idx].strip()

        # Number of phonon modes
        if line.startswith('# Number of phonon modes'):
            parts = line.split(':')
            n_modes = int(parts[1].strip())
            data['n_modes'] = n_modes
            idx += 1
            continue

        # Number of k points
        if line.startswith('# Number of k points'):
            parts = line.split(':')
            n_kpts = int(parts[1].strip())
            data['n_kpoints'] = n_kpts
            idx += 1
            continue

        # kpoint block
        if line.startswith('## kpoint'):
            # parse
            # Esempio: "## kpoint       1 :    0.000000e+00   0.000000e+00   0.000000e+00"
            tmp = line.replace('## kpoint','').split(':')
            left = tmp[0].strip()  # es. "1"
            coords_str = tmp[1].strip()  # es. "0.000000e+00   0.000000e+00   0.000000e+00"
            k_index = int(left)
            cparts = coords_str.split()
            qx, qy, qz = [float(c) for c in cparts]

            current_kpt = {
                'k_index': k_index,
                'q_vector': (qx, qy, qz),
                'modes': []
            }
            kpoints_data.append(current_kpt)
            idx += 1
            continue

        # Mode block
        if line.startswith('### mode'):
            # Esempio: "### mode        1 :    1.000000e-30"


            print(" analizzo ", line)
            
            parts = line.split(':')
            left = parts[0].replace('### mode','').strip()
            mode_index = int(left)
            freq_val_str = parts[1].strip()
            freq_val = math.sqrt(float(freq_val_str))
            
            print(" parte per la frequenza ",   freq_val *109737 )
            
            # Se serve conversione in cm^-1, fallo adesso. Oppure ipotizziamo sia già in cm^-1
            freq_cm1 = freq_val

            # Leggiamo i vettori (simile a script di prima)
            idx_scan = idx + 1
            tmp_vectors = []
            while idx_scan < nlines:
                l2 = lines[idx_scan].strip()
                if l2.startswith('### mode') or l2.startswith('## kpoint') or l2.startswith('#'):
                    break
                parts_line = l2.split()
                if len(parts_line) != 2:
                    break
                try:
                    val1 = float(parts_line[0])
                    val2 = float(parts_line[1])
                except:
                    break
                tmp_vectors.append((val1, val2))
                idx_scan += 1

            # Raggruppiamo in triplette per atomo
            # 3 coppie per atomo => (Rx,Ix), (Ry,Iy), (Rz,Iz)
            natom = len(tmp_vectors)//3
            eigvec_per_atom = []
            for iat in range(natom):
                c1 = tmp_vectors[3*iat]
                c2 = tmp_vectors[3*iat+1]
                c3 = tmp_vectors[3*iat+2]
                eigvec_per_atom.append((c1[0], c1[1],
                                        c2[0], c2[1],
                                        c3[0], c3[1]))

            this_mode = {
                'mode_index': mode_index,
                'frequency': freq_cm1,
                'eigenvector': eigvec_per_atom
            }
            if len(kpoints_data) > 0:
                kpoints_data[-1]['modes'].append(this_mode)

            idx = idx_scan
            continue

        idx += 1

    data['kpoints_data'] = kpoints_data
    return data


def write_castep_output(filename_out, log_info, alamode_info):
    """
    Scrive il file CASTEP-like utilizzando:
      - log_info: dizionario prodotto da parse_log_anphon()
      - alamode_info: dizionario prodotto da parse_alamode_output()
    """
    # Numero atomi
    nat_prim = log_info['nat_prim']  # es. 4
    # Numero di modi
    n_modes = alamode_info['n_modes']  # es. 12
    # Numero wavevectors
    # In CASTEP: "Number of wavevectors" tipicamente = n_kpoints (oppure = dimensione della griglia)
    # Dunque potresti voler usare alamode_info['n_kpoints'] = 4096
    # Oppure se stiamo usando la irreducible list => len(log_info['kpoints'])?
    # Decidi tu. Qui useremo TUTTI i kpoints (es. 4096) se ALAMODE li ha generati.
    n_wavevectors = alamode_info['n_kpoints']  # es. 4096

    # Lattice
    latt_prim = log_info['lattice_prim']  # 3 righe
    # Atomi
    atoms = log_info['atoms']
    # Masses
    masses = log_info['masses']
    # Kpoints e pesi (dal log)
    # (Se ALAMODE fornisce tutti i 4096, col log abbiamo la IRR k, potresti dover incrociare.)

    # Apri file in scrittura
    with open(filename_out, 'w') as f:
        # Header
        f.write("BEGIN header\n")
        f.write(f" Number of ions         {nat_prim}\n")
        f.write(f" Number of branches     {n_modes}\n")
        f.write(f" Number of wavevectors  {n_wavevectors}\n")
        f.write(" Frequencies in         cm-1\n")
        f.write(" IR intensities in      (D/A)**2/amu\n")
        f.write(" Raman intensities in   A**4\n")
        f.write(" Unit cell vectors (A)\n")
        for vec in latt_prim:
            f.write(f"   {vec[0]:12.6f}  {vec[1]:12.6f}  {vec[2]:12.6f}\n")

        f.write(" Fractional Co-ordinates\n")
        # Scriviamo i 4 atomi (o quanti nat_prim) con la loro massa. 
        # CASTEP tipicamente vuole "ID x y z Species mass"
        # Oppure una forma simile:
        #   1    0.450605    0.086290    0.982658   H    1.00794
        # Adeguiamo:
        for i, atm in enumerate(atoms, start=1):
            sp = atm['species']
            mx = atm['xfrac']
            my = atm['yfrac']
            mz = atm['zfrac']
            mmass = masses.get(sp,"??")  # se non trovato, ?? 
            f.write(f"   {i:3d}  {mx:9.6f}  {my:9.6f}  {mz:9.6f}  {sp:<3s}  {mmass:9.5f}\n")

        f.write("END header\n")

        # Ora, i dati su q-pt, freq, eigenvectors. 
        # alamode_info['kpoints_data'] => lista
        # Esempio: per kpoint in kpoints_data:
        #    q-pt= kidx  qx qy qz  weight
        #    (poi elenco di freq)
        #    "Phonon Eigenvectors"
        #    ...
        kpoints_data = alamode_info['kpoints_data']

        # Creiamo un "lookup" di weight da log. L'utente ha la lista di irreducible k.
        # Se ALAMODE ha 4096 kpoint, la corrispondenza 1:1 con la lista di 417 irr deve
        # essere mediata da simmetrie.  
        # Per semplificare, se la dimensione coincide e l'ordine coincide, lo associamo. 
        # Altrimenti, potremmo mettere un weight fisso "0.000196" come esempio, o 1/nk. 
        # Esempio: costruiamo un dict: weight_dict[k_index] = weight
        weight_dict = {}
        for kinfo in log_info['kpoints']:
            weight_dict[kinfo['kindex']] = kinfo['weight']

        for kpt in kpoints_data:
            kidx = kpt['k_index']
            qvec = kpt['q_vector']
            # Cerco il weight
            w = weight_dict.get(kidx, 0.000196)  # se non c'è corrispondenza, fallback
            f.write(f"     q-pt= {kidx:6d}   {qvec[0]:9.6f}  {qvec[1]:9.6f}  {qvec[2]:9.6f}      {w:10.6f}\n")

            # Frequenze
            modes_list = kpt['modes']
            for i, md in enumerate(modes_list, start=1):
                freq_cm1 = md['frequency'] * 109737
                f.write(f"       {i:2d}   {freq_cm1:12.6f}\n")

            f.write("                        Phonon Eigenvectors\n")
            # Ora i vettori
            for i, md in enumerate(modes_list, start=1):
                freq_cm1 = md['frequency']
                eig = md['eigenvector']
                # Stampa in un formato simile all’esempio CASTEP
                # Esempio: 
                # Mode Ion    X(real) X(imag)  Y(real) Y(imag)  Z(real) Z(imag)
                if i==1 :
                    f.write(f"Mode {i} Ion   X                   Y                   Z\n")
                for ia in range(len(eig)):
                    (rx, ix, ry, iy, rz, iz) = eig[ia]
                    # Ion index => ia+1
                    f.write(f"  {i:3d}  {ia+1:3d} {rx:18.12f} {ix:18.12f}  {ry:18.12f} {iy:18.12f}  {rz:18.12f} {iz:18.12f}\n")

def main():
    if len(sys.argv) < 4:
        print("Uso: python convert_alamode_to_castep.py anphon_log.txt alamode_output.dat output_castep.dat")
        sys.exit(1)

    log_file = sys.argv[1]
    alamode_file = sys.argv[2]
    out_file = sys.argv[3]

    # 1) parse log
    log_info = parse_log_anphon(log_file)
    # 2) parse alamode
    alamode_info = parse_alamode_output(alamode_file)
    # 3) scrivi output cast
    write_castep_output(out_file, log_info, alamode_info)

    print(f"File CASTEP scritto in: {out_file}")

if __name__ == "__main__":
    main()
