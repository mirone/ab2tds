TDS intensity distribution in 3D reciprocal space:
==================================================

.. automodule:: make_TDS_IntensityVolume
    :members: APPLYTIMEREVERSAL,subN1,subN2,subN3,N1 ,N2,N3,Nfour_interp,Temperature,Lambda,NEUTRONCALC,CohB,NeutronE,do_histogram,Nbins,xvdos,DOQLIMITS,QMIN,QMAX,outputname,branchWeight,energyWindow, RemoveBose
    :noindex:


