.. ab2tds documentation master file, created by
   sphinx-quickstart on Tue Jan  8 15:25:06 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ab2tds's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

   Introduction
   installation
   preliminary
   different_spectra
   tutorial

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

