IXS and INS intensity maps without interpolation:
=================================================


.. automodule::  make_TDS_DispersionIntensityCurvesClone
    :members: APPLYTIMEREVERSAL,Temperature,resolutionfile,Lambda,Saturation,lowerLimit,bottom_meV,NEUTRONCALC,CohB,NeutronE,branchWeight,Eigscal,UniqueIon

    :noindex:
 


