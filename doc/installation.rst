Installation
============

ab2tds is distributed as open-source package via http://gitlab.esrf.fr

Using Git, sources can be retrieved with the following commands:

    * public access ::

        git clone  https://gitlab.esrf.fr/mirone/ab2tds

    * developer access : to contribute to the project you can either register to gitlab.esrf.fr and ask to be added as developer (only for long term contributors) or fork the project somewhere, even on other repositories, and then ask for a pull.

Requirements for the installation

python2.6+ including the following modules

numpy, PyMca, h5py, pyyaml, matplotlib, mpi4py (optional), pylab (optional)

Installation ::

    python setup.py install --install-lib ${PWD}/dummy --install-scripts ${PWD}/dummyscripts/
    export PATH="${PWD}/dummyscripts"/:$PATH

Auxiliary scripts can be found in the ab2tds directory.



