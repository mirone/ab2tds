Phonon density of states (partial and weighted):
================================================


   .. automodule:: make_TDS_IntensityVolumeOnlyHisto
       :members:  APPLYTIMEREVERSAL,subN1,subN2,subN3,N1 ,N2,N3,Nfour_interp,Temperature,NEUTRONCALC,Lambda,NEUTRONCALC,CohB,NeutronE,Nbins,MAX_W, xvdos,DOQLIMITS,QMIN,QMAX,outputname,branchWeight,energyWindow, RemoveBose
       :noindex:

