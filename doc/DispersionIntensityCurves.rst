Inelastic x-ray (IXS) and neutron (INS) scattering intensity maps:
==================================================================

.. automodule::  make_TDS_DispersionIntensityCurves
    :members: APPLYTIMEREVERSAL,redStarts,redEnds ,Nqlines,Nfour_interp,Temperature,resolutionfile,Lambda,Saturation,lowerLimit,bottom_meV,NEUTRONCALC,CohB,NeutronE,branchWeight,Eigscal,UniqueIon
    :noindex:

