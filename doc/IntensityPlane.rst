TDS intensity distribution in reciprocal space planes: 
======================================================

.. automodule:: make_TDS_IntensityPlane
    :members: APPLYTIMEREVERSAL,Nfour_interp,Temperature,Lambda,NEUTRONCALC,CohB,NeutronE,lowerLimit,bottom_meV,Nqs,DQ,redA, redB, redC, redCenter, redNormal,redX_idea,branchWeight,energyWindow,Eigscal,UniqueIon,RemoveBose, LINEAR, COLOR, red4castep 
    :noindex:


