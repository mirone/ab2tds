Gradients calculation and search for critical points:
=====================================================

   .. automodule:: make_TDS_Gradients
       :members: subN1,subN2,subN3,N1,N2,N3,Nfour_interp,Lambda,outputname,branchWeight,energyWindow
       :noindex:

