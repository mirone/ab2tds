Preliminary steps to be performed for computing IXS spectras and TDS intensities :
==================================================================================

* **PRELIMINARY STEPS**

The following steps are mandatory for the calculation of scattering intensities. They must be done once and their results are stored automatically in a hdf5 file. Subsequent calculations will use the saved results. The Debye-Waller factors calculation must be performed for each temperature of interest.

      .. toctree::
	 :maxdepth: 2

	    Reconstruction of first Brillouin zone <zone_reconstruction>
	    Calculation of Debye-Waller factors  <debye_waller>
	    Fourier Interpolation on Dynamical matrix <dynamicalFT>

