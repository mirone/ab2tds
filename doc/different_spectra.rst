Calculating scattering intensities, phonon densities of states and dispersion gradients:
========================================================================================

* **SECONDARY STEPS**


      .. toctree::
	 :maxdepth: 2

         Inelastic x-ray (IXS) and neutron (INS) scattering intensity maps <DispersionIntensityCurves>
         IXS and INS intensity maps without interpolation <DispersionIntensityCurvesClone>
         Direction integrated and powder spectra <DispersionIntensityCurvesIntegrated>
	 3D intensity distribution of thermal diffuse scattering (TDS) <IntensityVolume>
         3D TDS intensity distribution on sub Volume <SubVolume>
         TDS intensity plane from Volume cut <IntensityPlaneFromVolume>
         2D TDS intensity distribution (high resolution plane calculation) <IntensityPlane>
         2D TDS intensity distribution without interpolation <IntensityPlaneClone>  
	 Phonon density of states (partial and weighted) <IntensityVolumeOnlyHisto>
         Gradients calculation and search for critical points <Gradients>
	
	    


