Direction integrated and powder spectra: 
========================================

 
.. automodule::  make_TDS_DispersionIntensityCurvesIntegrated
    :members: APPLYTIMEREVERSAL,redStarts,redEnds,Nqlines,Nfour_interp,Temperature,resolutionfile,Lambda,Saturation,lowerLimit,bottom_meV,NEUTRONCALC,CohB,NeutronE,branchWeight,Eigscal,UniqueIon,axis ,Nangular ,sphereintegral ,Nangular2

    :noindex:


