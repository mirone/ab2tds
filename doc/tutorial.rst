Tutorial
========

This tutorial comes with example files for the calculation of IXS and INS intensity maps and TDS intensity distribution. The case of ice-XI

1. Download the files containing the dynamical matrix together with cell information (.phonon file) and the Born effective charges together with polarisability (.castep file). These files have been created via CASTEP though a "phonon" and an "efield" calculation. All files are located in the "tutorial" directory.

2. Preliminary steps

    * reconstruct Brillouin zone using symmetry operations

      requires the files "phon_PBE_e774_co820_k554_fine001.phonon", "efield_PBE_e774_co820_k554.castep" and the input file called "inputSimmetrization.txt" 

      the input file is a text file containing the following parameters ::
      
        APPLYTIMEREVERSAL=1

      run the script ::

        make_TDS_Simmetrization phon_PBE_e774_co820_k554_fine001.phonon inputSimmetrization.txt efield_PBE_e774_co820_k554.castep     

    * calculate Debye Waller factors with input file "inputDW.txt" containing ::

        APPLYTIMEREVERSAL=1
        Temperature=100

      run the script ::

        make_TDS_DW phon_PBE_e774_co820_k554_fine001.phonon inputDW.txt

      inspect calculated values stored in the md5 file ::

        h5dump -d /Codes_1/DWs/100/DWf_33 phon_PBE_e774_co820_k554_fine001.phonon.* 

      here '100' stands for the temperature, Debye-Waller factors calculated at other temperatures will be stored in an subdirectory named accordingly. Explore the contents of the md5 file with h5ls -r 


    * perform Fourier Interpolation (can be executed in parallel) with input file "inputFourier.txt" containing ::

        APPLYTIMEREVERSAL=1
        Nfour_interp=4

      run the script ::

        mpirun -n 4 make_TDS_Fourier phon_PBE_e774_co820_k554_fine001.phonon inputFourier.txt

3. Calculate scattering intensities for x-rays and neutron

    * calculate IXS intensity map for given direction with input file "inputDispersionIntensityCurve.txt" containing ::

        APPLYTIMEREVERSAL=1
        Nfour_interp=4
        Temperature=100
        redStarts=[[1.000 , 1.000 , 0.000]]
        redEnds  =[[2.000 , 2.000 , 0.000]]
        Nqlines=[200]
        resolutionfile="a3res9_pm.fit"
        Lambda=0.6968
        Saturation=1e-2
        lowerLimit=1e-12
        bottom_meV=1.0e-1
        COLOR=1


     
      download the experimental resolution file "a3res9_pm.fit" and run the script ::

        make_TDS_DispersionIntensityCurves phon_PBE_e774_co820_k554_fine001.phonon inputDispersionIntensityCurve.txt

      right-click on the graph to get a constant Q-cut

      try different values of Nfour_interp to see how the Fourier interpolation effects the intensity map. Remember that make_TDS_Fourier needs to be run for each values of Nfour_interp. 

    * calculate INS intensity map
     
      replace "Lambda=0.6968" by "NeutronE=25.0" in the input file and add the scattering lengths ::

        NeutronE=25.0
        CohB={"H": 6.671+0.0j , "O": 5.803+0.0j }

      activate the neutron calculation by adding ::

        NEUTRONCALC=1

      run the script as for IXS intensity calculation

    * calculate 3D TDS intensity distribution (can be parallel) with input file "inputIntensityVolume.txt" containing ::

        APPLYTIMEREVERSAL=1
        subN1=50
        subN2=50
        subN3=50
        N1 =  4
        N2 =  4
        N3 =  4
        Nfour_interp=4
        Temperature=100
        Lambda=0.6968
        Saturation=1.0e6
        do_histogram=0
        DOQLIMITS=0
      
      run the script ::

        mpirun -n 6 make_TDS_IntensityVolume phon_PBE_e774_co820_k554_fine001.phonon inputIntensityVolume.txt

      ATTENTION: This operation requires 6 GB RAM per core! Run on less cores or decrease subNx if your machines does not have enough memory.

      open the VolumeIntensity\_100\_.ccp4 file with Chimera or VolumeIntensity\_100\_.h5 with pymca to display 3D isosurfaces


    * calculate TDS intensity map (can be parallel) with input file "inputIntensityPlane.txt" containing ::

        APPLYTIMEREVERSAL=1
        Nfour_interp=4
        Temperature=100
        Lambda=0.6968
        redCenter = [0.0 , 0.0, 0.0]
        Nqs   = 80
        DQ    = 0.12
        Saturation=2
        lowerLimit=0.000001
        bottom_meV=1e-3
        LINEAR=1
        COLOR=1
        redA = [ 1.0 , 0.0  ,0.0  ] 
        redB = [ 0.0  ,1.0  ,0.0  ] 
        redC = [ 0.0 , 0.0 , 0.0 ] 

      run the script ::

        mpirun -n 4 make_TDS_IntensityPlane phon_PBE_e774_co820_k554_fine001.phonon inputIntensityPlane.txt


