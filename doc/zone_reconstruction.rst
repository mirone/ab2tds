Zone Reconstruction
===================

The reconstruction of the whole Brillouin zone is done by replicating the ab-initio results, which cover a sub-region of the Brillouin zone, with symmetry operations. These symmetry operations are calculated automatically:

  * The atoms names and positions are read from the ab-initio output file.
  * A tetraedron whose vertexes are the first atom of the unit cell, in four adjacient cells, is considered
  * All the possible combinations of putting the tetraedron vertexes over the atomic positions of the lattice
    are tested, retaining the iso-metrics and iso-atomic ones.
  * For each match, the roto-translational transformation is extracted in the form of an unitary matrix plus a translational vector.
  * This transformation is considered as a candidate for the position of good simmetry operation.
  * The candidates are checked by applying it to the whole grid.
  * The Fourier transform of the transformated grid is taken for each atomic specie. (by associating a Dirac to each atom) 
  * The symmetry operation is finally validated if all the Fourier transforms matches. 

.. automodule:: make_TDS_Simmetrization
    :members: APPLYTIMEREVERSAL, CALCULATECOMPLEMENT, energy_scaling 
    :noindex:

*The script produces an output file as described automatically in module make_TDS_Simmetrization. The following information has been automatically extracted from  make_TDS_Simmetrization.py*

.. automodule:: TDS_Simmetry

.. autofunction:: GetAllReplicated 

