2D TDS intensity distribution without interpolation:
====================================================


   .. automodule:: make_TDS_IntensityPlaneClone
       :members: APPLYTIMEREVERSAL,Nfour_interp,Temperature,Lambda,NEUTRONCALC,CohB,NeutronE,lowerLimit,bottom_meV,Nqs,DQ,redA, redB, redC, redCenter, redNormal,redX_idea,branchWeight,energyWindow,Eigscal,UniqueIon,RemoveBose, LINEAR, COLOR
       :noindex:
