Introduction
============

ab2tds is an open source package for calculating lattice dynamics properties. It is a post-processing tool which takes the dynamical matrix computed by an external program (CASTEP and phonopy which interfaces VASP, Wien2K, abinit, SIESTA)  as input. A wide range of properties can be calculated:


    * Debye-Waller factor for given temperatures
    * Dynamical structure factor
    * Inelastic and thermal diffuse scattering intensities for x-rays and neutrons
    * Inelastic x-ray and neutron spectra for single crystals, integrated directions and powders
    * Partial phonon density of states
    * X-ray and neutron weighted density of states
    * Scattering contrast between selected branches
    * Search for critical points in phonon dispersion


ab2tds uses the formalism of Xu et al [1,2]. A detailed description and a compact introduction to the calculation of dynamical matrices from first principles can be found in the thesis of Bjoern Wehinger [3]. The dynamical matrix must be computed on a Monkhorst–Pack [4] grid of q-vectors containing enough points to compute a reasonable phonon density of states. The phonon eigenvectors must be normalized and in periodic description, written to an ascii file together with the cell information. ab2tds reads dynamical matrices in the CASTEP file format [5] using the C-matrix convention [6]. The program uses Fourier interpolation according the Parlinski method [7] for the computation of the dynamical matrix at arbitrary q-points and allows thus to obtain well converged phonon densities of states and fine sampling of scattering intensities along any direction in reciprocal space.
Publications [8-15] involved the use of this program.


[1] R. Xu, H. Hong, and T. C. Chiang. Probing phonons and phase transitions in solids with x-ray thermal diffuse scattering. In R. Barabash, G. Ice, and P. E. A.Turchi, editors, Diffuse Scattering and the Fundamental Properties of Materials. Momentum Press, New York, 2009.

[2] R. Q. Xu and T. C. Chiang. Determination of phonon dispersion relations by x-ray thermal diffuse scattering. Z. Kristallogr., 220(12):1009-1016 (2005), http://dx.doi.org/10.1524/zkri.2005.220.12.1009.

[3] B. Wehinger, On the combination of thermal diffuse scattering, inelastic x-ray scattering and ab initio lattice dynamics calculations PhD thesis, Universite de Grenoble (2013), http://tel.archives-ouvertes.fr/tel-00961602

[4] H. J. Monkhorst and J. D. Pack. Special points for brillouin-zone integrations. Phys. Rev. B, 13:5188-5192 (1976), http://dx.doi.org/10.1103/PhysRevB.13.5188.

[5] CASTEP phonon file format http://www.tcm.phy.cam.ac.uk/castep/documentation/WebHelp/html/expcastepfilephonon.htm

[6] Convention of dynamical matrix http://www.tcm.phy.cam.ac.uk/castep/Phonons_Guide/Castep_Phononsch1.html#x4-40001.1

[7] K. Parlinski, Z. Q. Li, and Y. Kawazoe. First-principles determination of the soft mode in cubic ZrO2. Phys. Rev. Lett., 78:4063-4066 (1997) http://dx.doi.org/10.1103/PhysRevLett.78.4063.

[8] G. Baldi et al., Emergence of Crystal-like Atomic Dynamics in Glasses at the Nanometer Scale. Phys. Rev. Lett. 110 (2013), http://dx.doi.org/185503 10.1103/PhysRevLett.110.185503

[9] B. Wehinger et al., Lattice dynamics of coesite. J. Phys.: Condens. Matter 25 275401 (2013), http://dx.doi.org/10.1088/0953-8984/25/27/275401 http://arxiv.org/abs/1304.3046

[10]  A.I. Chumakov et al., Role of Disorder in the Thermodynamics and Atomic Dynamics of Glasses. Phys. Rev. Lett. 112 (2014), http://dx.doi.org/025502 10.1103/PhysRevLett.112.025502

[11] B. Wehinger et al., Diffuse scattering in metallic tin polymorphs, J. Phys.: Condens. Matter 26 115401 (2014), http://dx.doi.org/10.1088/0953-8984/26/11/115401 http://arxiv.org/abs/1310.3080

[12] B.Wehinger et al., Diffuse scattering in ice Ih, Preprint (2014), http://arxiv.org/abs/1402.2159 Journal reference: 2014 J. Phys.: Condens. Matter 26 265401  http://dx.doi.org/10.1088/0953-8984/26/26/265401

[13] Björn Wehinger et al., Lattice dynamics of α-cristobalite and the Boson peak in silica glass, J. Phys.: Condens. Matter 27 305401 (2015)

[14] B. Wehinger et al.,  Lattice dynamics of MgSiO3 perovskite (bridgmanite) studied by inelastic x-ray scattering and ab initio calculations, Preprint (2015) http://arxiv.org/abs/1509.06164

[15] B. Wehinger et al., Soft phonon modes in rutile TiO2 Phys. Rev. B 93, 014303 (2016)


